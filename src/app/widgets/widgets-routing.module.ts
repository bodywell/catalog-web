import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookingComponent } from '../pages/booking/booking.component';
import { BookingFinishComponent } from '../pages/booking/components/booking-finish/booking-finish.component';
import { ChoiceEmployeeComponent } from '../pages/booking/components/choice-employee/choice-employee.component';
import { ChoiceServiceComponent } from '../pages/booking/components/choice-service/choice-service.component';
import { ChoiceTimeComponent } from '../pages/booking/components/choice-time/choice-time.component';
import { ChoiceTypeComponent } from '../pages/booking/components/choice-type/choice-type.component';
import { ConfirmBookingComponent } from '../pages/booking/components/confirm-booking/confirm-booking.component';
import { BookingWidgetComponent } from './booking-widget/booking-widget.component';
import { TableWidgetComponent } from './table-widget/table-widget.component';
import { WidgetsComponent } from './widgets.component';
import {BookingPaymentMethodComponent} from '../pages/booking/components/payment-method/payment-method.component';
import {PurchaseComponent} from '../pages/purchase/purchase.component';
import {PurchaseProductsComponent} from '../pages/purchase/components/purchase-product/purchase-products.component';
import {PurchaseConfirmComponent} from '../pages/purchase/components/purchase-confirm/purchase-confirm.component';

const routes: Routes = [
  {
    path: '',
    component: WidgetsComponent,
    children: [
      {
        path: 'booking',
        component: BookingWidgetComponent,
        children: [
          {
            path: 'service',
            component: ChoiceServiceComponent,
          },
          {
            path: 'employee',
            component: ChoiceEmployeeComponent,
          },
          {
            path: 'type',
            component: ChoiceTypeComponent,
          },
          {
            path: 'time',
            component: ChoiceTimeComponent,
          },
          {
            path: 'confirm',
            component: ConfirmBookingComponent,
          },
          {
            path: 'payment',
            component: BookingPaymentMethodComponent,
          },
          {
            path: 'finish',
            component: BookingFinishComponent,
          },
          {
            path: 'purchase',
            component: PurchaseComponent,
            children: [
              {
                path: 'products',
                component: PurchaseProductsComponent,
              },
              {
                path: 'confirm',
                component: PurchaseConfirmComponent,
              }
            ],
          }
        ],
      },
      { path: 'table/:company_id/:branch_id', component: TableWidgetComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WidgetRoutingModule {}
