import { AfterContentInit, AfterViewInit, Component, OnInit } from '@angular/core';
import { take, takeUntil } from 'rxjs/internal/operators';
import { ApiService } from '../../core/service/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import { TranslateService } from '@ngx-translate/core';
import {BookingService} from '../../core/service/booking.service';

@Component({
  selector: 'app-table-widget',
  templateUrl: './table-widget.component.html',
  styles: ['.accordion { padding: 8px }'],
})
export class TableWidgetComponent implements OnInit {
  data: any;
  companyId = 0;
  branchId = 0;
  companyInfo: object = {};
  branchInfo: object = {};
  private unsubscribe: Subject<any> = new Subject();

  constructor(
    public apiService: ApiService,
    private route: ActivatedRoute,
    public translate: TranslateService,
    public bookingService: BookingService
  ) {
    this.route.params.pipe(takeUntil(this.unsubscribe)).subscribe((params) => {
      this.companyId = parseInt(params['company_id'], 10);
      this.branchId = parseInt(params['branch_id'], 10);
    });
  }

  ngOnInit() {
    this.apiService
      .getCompanyById(this.companyId)
      .pipe(take(1))
      .subscribe((response) => {
        if (response['status'] === 'success') {
          this.companyInfo = {
            ...response['data'],
            industriesTitle: response['data']['industries']
              .slice(0, 3)
              .map((title) => this.translate.instant(title))
              .join(', '),
          };

          for (let i = 0; i < this.companyInfo['branches'].length; i++) {
            if (this.branchId === this.companyInfo['branches'][i]['id']) {
              this.data = this.bookingService.parseByCategory(this.companyInfo['branches'][i]['services'], this.companyInfo);
              this.data.sort((a, b) => a.category_title.localeCompare(b.category_title));
              this.data.forEach(category => {
                category.items.sort((a, b) => a.title.localeCompare(b.title));
              });
            }
          }

          document.body.style.overflow = 'hidden';
          let contentHeight = document.body.offsetHeight;
          const HeightUpdateInverval = 100;

          setInterval(() => {
            if (contentHeight !== document.body.offsetHeight) {
              contentHeight = document.body.offsetHeight;
              window.parent.postMessage({ type: 'bodywellTableWidgetDynamicHeight', height: contentHeight }, '*');
            }
          }, HeightUpdateInverval);
        }
      });
  }
}
