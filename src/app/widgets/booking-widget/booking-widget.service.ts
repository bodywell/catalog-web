import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { WidgetModule } from '../widgets.module';

@Injectable({providedIn: WidgetModule})
export class ServiceNameService {
  public login: Subject<any> = new Subject();

  constructor() { }
  
}