import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AppAuthService } from 'src/app/core/service/auth.service';

import { BookingService } from '../../core/service/booking.service';

import { Company } from '../../shared/interfaces/company.model';

@Component({
  selector: 'app-booking-widget',
  templateUrl: 'booking-widget.component.html',
  styleUrls: ['booking-widget.component.scss'],
})
export class BookingWidgetComponent implements OnInit {
  public isLoaded = false;
  public isAuth = false;

  public readonly companyId: number;
  public readonly branchId: number;

  public company: Company;

  public step = 0;

  private readonly destroy: Subject<any> = new Subject();

  constructor(
    private route: ActivatedRoute,
    private translate: TranslateService,
    private bookingService: BookingService,
    private authService: AppAuthService
  ) {
    this.companyId = this.route.snapshot.queryParams['company_id'];
    this.branchId = this.route.snapshot.queryParams['branch_id'];
  }

  ngOnInit() {
    this.initBookingService(this.route.snapshot.queryParams);
    this.authService.startAuthEvent.subscribe(() => {
      this.isAuth = true;
    });
    
    this.bookingService.isLoaded.pipe(takeUntil(this.destroy)).subscribe((status: boolean) => {
      this.isLoaded = status;
      
      if (status) {
        this.company = this.bookingService.companyInfo;
      }
    });

    this.route.queryParams.pipe(takeUntil(this.destroy)).subscribe((params) => {
      this.bookingService.setQueryParams(params);
    });
  }

  private initBookingService(queryParams: Params): void {
    this.bookingService.init({
      companyId: Number(queryParams['company_id']),
      branchId: Number(queryParams['branch_id']),
      productId: Number(queryParams['product_id']),
      employeeId: Number(queryParams['employee_id']),
      bookDate: queryParams['book_date'],
      type: Number(queryParams['type']),
      paymentSessionId: queryParams['session_id'],
    });
  }

  public prevStep(): void {
    if (this.step <= 0) {
      return;
    }

    this.step -= 1;
  }

  public nextStep(): void {
    this.step += 1;
  }

  public onLogin(): void {
    this.isAuth = false;
  }

  public closeWidget(): void {
    window.parent.postMessage({ type: 'bodywellBookingWidgetClose' }, '*');
  }
}
