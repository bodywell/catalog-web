import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-widgets',
  templateUrl: 'widgets.component.html',
})
export class WidgetsComponent implements OnInit {
  constructor(private route: ActivatedRoute, private translate: TranslateService) {}

  ngOnInit() {
    this.initLanguage();
  }

  private initLanguage() {
    const lang = this.route.snapshot.queryParams['lang'];

    this.translate.use(lang);
    this.translate.setDefaultLang(lang);
  }
}
