import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../app.module';
import { BookingModule } from '../pages/booking/booking.module';
import { AccordionModule } from '../shared/accordion/accordion.module';
import { AuthLoginModule } from '../shared/modules/auth/auth.module';
import { SharedModule } from '../shared/shared.module';
import { BookingWidgetComponent } from './booking-widget/booking-widget.component';
import { TableWidgetComponent } from './table-widget/table-widget.component';
import { WidgetRoutingModule } from './widgets-routing.module';
import { WidgetsComponent } from './widgets.component';
import {PurchaseModule} from '../pages/purchase/purchase.module';

@NgModule({
  imports: [
    WidgetRoutingModule,
    AccordionModule,
    CommonModule,
    SharedModule,
    BookingModule,
    PurchaseModule,
    AuthLoginModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  exports: [],
  declarations: [WidgetsComponent, BookingWidgetComponent, TableWidgetComponent],
  providers: [],
})
export class WidgetModule {}
