import { Component, DoCheck, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from './core/service/api.service';
import { Toast } from 'primeng/toast';
import { LocationService } from './core/service/location.service';
import { updateLocale } from 'moment';
import * as moment from 'moment';
import {CookiesPrefService} from './core/service/cookiesPref.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements DoCheck {
  title = 'Bodywell';
  isInit = false;

  @ViewChild('requestsMessages', { static: false } as any) requestsMessages: Toast;

  constructor(
      translate: TranslateService,
      public apiService: ApiService,
      public locationService: LocationService,
      public cookiesPrefService: CookiesPrefService) {
    this.locationService.getCurrentPosition().then((res) => {});

    translate.addLangs(['EN', 'UA', 'FR', 'DE']);

    let storageLang = localStorage.getItem('lang');
    storageLang =
      storageLang && ~storageLang.indexOf('-') ? storageLang.substring(0, storageLang.indexOf('-')) : storageLang;
    storageLang = storageLang === 'UK' || storageLang === 'RU' ? 'UA' : storageLang;

    if (storageLang) {
      translate.use(storageLang.match(/UA|EN|FR|DE/) ? storageLang : 'EN');
      translate.setDefaultLang(storageLang.match(/UA|EN|FR|DE/) ? storageLang : 'EN');
    }

    if (!storageLang || !storageLang.match(/UA|EN|FR|DE/)) {
      let language = window.navigator.language;
      language = language && ~language.indexOf('-') ? language.substring(0, language.indexOf('-')) : language;
      language = language === 'uk' || language === 'ru' ? 'UA' : language.toUpperCase();

      const languages = window.navigator.languages
        .map((lang) => {
          lang = lang && ~lang.indexOf('-') ? lang.substring(0, lang.indexOf('-')) : lang;
          return lang === 'uk' ? 'ua' : lang;
        })
        .filter((l) => l !== 'ru');

      if (language.match(/UA|EN|FR|DE/)) {
        localStorage.setItem('lang', language);
        translate.use(language);
        translate.setDefaultLang(language);
      } else if (languages) {
        languages
          .map((language) => language.toUpperCase())
          .some((language, idx) => {
            if (language.match(/UA|EN|FR|DE/)) {
              localStorage.setItem('lang', language);
              translate.use(language);
              translate.setDefaultLang(language);

              return true;
            } else if (languages.length === idx + 1) {
              localStorage.setItem('lang', 'EN');
              translate.use('EN');
              translate.setDefaultLang('EN');
            }
          });
      } else {
        localStorage.setItem('lang', 'EN');
        translate.use('EN');
        translate.setDefaultLang('EN');
      }
    }

    if (!('path' in Event.prototype)) {
      Object.defineProperty(Event.prototype, 'path', {
        get: function () {
          const path = [];
          let currentElem = this.target;
          while (currentElem) {
            path.push(currentElem);
            currentElem = currentElem.parentElement;
          }
          if (path.indexOf(window) === -1 && path.indexOf(document) === -1) path.push(document);
          if (path.indexOf(window) === -1) path.push(window);
          return path;
        },
      });
    }

    translate.onLangChange.subscribe((value) => {
      translate.use(value.lang);

      if (String(value.lang).toLowerCase() === 'en') {
        updateLocale('en', {
          week: {
            dow: 1,
            doy: 7,
          },
        });
      } else if (String(value.lang).toLowerCase() === 'uk') {
        updateLocale('uk', {
          week: {
            dow: 1,
            doy: 7,
          },
          months: [
            'Січень',
            'Лютий',
            'Березень',
            'Квітень',
            'Травень',
            'Червень',
            'Липень',
            'Серпень',
            'Вересень',
            'Жовтень',
            'Листопад',
            'Грудень',
          ],
          monthsShort: ['Січ', 'Лют', 'Бер', 'Квіт', 'Трав', 'Черв', 'Лип', 'Серп', 'Вер', 'Жовт', 'Лист', 'Груд'],
        });
      } else if (String(value.lang).toLowerCase() === 'fr') {
        updateLocale('fr', {
          week: {
            dow: 1,
            doy: 7,
          },
          months: [
            'Janvier',
            'Février',
            'Mars',
            'Avril',
            'May',
            'Juin',
            'Juillet',
            'Août',
            'Septembre',
            'Octobre',
            'Novembre',
            'Décembre',
          ],
          weekdaysShort: ['Dim', 'Lu', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
          weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        });
      } else if (String(value.lang).toLowerCase() === 'de') {
        updateLocale('de', {
          week: {
            dow: 1,
            doy: 7,
          },
          months: [
            'Januar',
            'Februar',
            'März',
            'April',
            'Mai',
            'Juni',
            'Juli',
            'August',
            'September',
            'Oktober',
            'November',
            'Deyember',
          ],
          weekdaysShort: ['So', 'Mo', 'Die', 'Mi', 'Do', 'Fr', 'Sa'],
          weekdays: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
        });
      }

      moment.locale((String(value.lang).toLowerCase() === 'ua' ? 'uk' : String(value.lang).toLowerCase()) || 'en');

      this.isInit = true;
    });
  }

  onActivate(event) {
    window.scroll(0, 0);
  }

  ngDoCheck() {
    if (this.requestsMessages && this.requestsMessages.messages && this.requestsMessages.messages.length > 3) {
      this.requestsMessages.messages.shift();
    }
  }
}
