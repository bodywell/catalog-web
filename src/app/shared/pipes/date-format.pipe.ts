import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'amDateFormat',
})
export class DateFormatPipe implements PipeTransform {
  transform(value: Date | moment.Moment | string | number, ...args: any[]): string {
    if (!value) {
      return '';
    }

    if (typeof value === 'number' && args[1] === 'unix') {
      return moment.unix(value).format(args[0]);
    }

    if (typeof value === 'string' || Date) {
      return moment(value).format(args[0]);
    }

    return moment(value).format(args[0]);
  }
}
