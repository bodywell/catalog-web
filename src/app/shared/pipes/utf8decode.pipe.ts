import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'utf8decode'
})
export class Utf8DecodePipe implements PipeTransform {
    transform(value: string): string {
        const decoder = new TextDecoder('utf-8');
        const bytes = new Uint8Array(value.length);
        for (let i = 0; i < value.length; i++) {
            bytes[i] = value.charCodeAt(i);
        }
        return decoder.decode(bytes);
    }
}
