import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-btn-switch',
    template: `<div class="button-switch-container">
        <button (click)="selectItem(item.key)" [ngClass]="{'active': item.key == selectedValue}" *ngFor="let item of array | keyvalue">{{ item.value | translate }}</button>
    </div>`,
    styleUrls: ['./btn-switch.component.scss'],
})
export class BtnSwitchComponent implements OnInit, OnDestroy {
    @Input() array: any = [];
    @Input() selectedValue: number;
    @Output() selected = new EventEmitter<number>();

    constructor() {}

    ngOnInit() {
        this.selected.subscribe((value: number) => {
            this.selectedValue = value;
        });
    }

    selectItem(value: number) {
        if (this.selectedValue == value) {
            return;
        }

        this.selected.emit(value);
    }

    ngOnDestroy() {
        this.selected.unsubscribe();
    }
}
