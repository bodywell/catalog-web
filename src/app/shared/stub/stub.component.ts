import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-stub',
  templateUrl: './stub.component.html',
  styleUrls: ['./stub.component.scss']
})
export class StubComponent implements OnInit {

  @Input() heading = '';
  @Input() text = '';
  @Input() img = '';
  @Input() mode = '';
  constructor() { }

  ngOnInit() {
  }

}
