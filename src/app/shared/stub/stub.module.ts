import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StubComponent } from './stub.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../shared.module';

@NgModule({
  declarations: [StubComponent],
  imports: [
    CommonModule,
    TranslateModule
  ],
  exports: [StubComponent]
})
export class StubModule { }
