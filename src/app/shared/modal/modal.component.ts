import { DOCUMENT } from '@angular/common';
import {
  Component,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild,
  EventEmitter,
  ElementRef,
  OnChanges,
  SimpleChanges,
  Renderer2,
  Inject,
} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit, OnChanges {
  @Input() isOpen = false;
  @Input() overlayClass = null;
  @Input() overlayStyle = null;
  @Input() contentClass = null;
  @Input() contentStyle = null;

  @Output() hide: EventEmitter<any> = new EventEmitter();

  @ViewChild('content', { static: false }) content: ElementRef;

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes['isOpen'] !== undefined) {
      changes['isOpen'].currentValue
        ? this.renderer2.addClass(this.document.body, 'overflow-hidden')
        : this.renderer2.removeClass(this.document.body, 'overflow-hidden');
    }
  }

  constructor(private renderer2: Renderer2, @Inject(DOCUMENT) private document: Document) {}

  ngOnInit() {}

  @HostListener('click', ['$event'])
  _hideModal(e) {
    if (this.content) {
      const targetElementPath = (e.composedPath && e.composedPath()) || e.path;
      const elementRefInPath = targetElementPath.find((path) => path === this.content.nativeElement);
      
      if (this.isOpen && !elementRefInPath) {
        this.hide.emit();
      }
    }
  }

  public toggle() {
    this.isOpen = !this.isOpen;
  }

  public hideModal() {
    this.isOpen = false;
  }

  public showModal() {
    this.isOpen = true;
  }
}
