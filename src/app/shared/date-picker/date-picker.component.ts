import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import moment from 'moment';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
})
export class DatePickerComponent implements OnInit {
  public weekDays = [];
  firstDay: Date;
  counter = 0;
  today: Date = new Date();
  public selectedDay: Date;
  public minDate: Date = new Date();
  public maxDate: Date;
  @Input() selectStartDay: Date;
  @Output() selectedDate = new EventEmitter<Date>();

  constructor(public translate: TranslateService) {}

  ngOnInit() {
    this.maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 3));
    this.firstDay = moment().startOf('week').toDate();
    this.getWeek();

    this.selectedDay = this.selectStartDay;
  }

  public selectDay(date: Date): void {
    if (this.isToday(date)) {
      this.selectedDay = this.today;
    } else {
      this.selectedDay = new Date(date);
    }
    this.selectedDate.emit(this.selectedDay);
  }

  public nextWeek(): void {
    this.counter++;
    this.getWeek();
  }

  public previousWeek(): void {
    if (this.counter > 0) {
      this.counter--;
      this.getWeek();
    }
  }

  private getWeek(): void {
    this.weekDays = new Array();

    for (let i = 0; i < 7; i++) {
      this.weekDays.push(
        moment()
          .startOf('week')
          .add(i + this.counter * 7, 'days')
          .toDate()
          .toISOString()
      );
    }
  }

  public goCalendarDate(date: moment.Moment): void {
    let firstWeekDay = moment(date).startOf('week').toDate();

    let diff = Math.floor((firstWeekDay.getTime() - this.firstDay.getTime()) / 1000 / 60 / 60 / 24) / 7;

    if (diff >= 0) {
      this.counter = Math.floor(diff);

      this.getWeek();
      this.selectDay(moment(date).toDate());
    }
  }

  public goToday() {
    this.counter = 0;
    this.getWeek();
    this.selectDay(this.today);
  }

  public getDate(date): number {
    return new Date(date).getDate();
  }

  public getWeekRangeShortString(): string {
    let startDate = new Date(this.weekDays[0]).toLocaleString(this.translate.currentLang.toLowerCase(), {
      month: 'short',
    });
    let endDate = new Date(this.weekDays[this.weekDays.length - 1]).toLocaleString(
      this.translate.currentLang.toLowerCase(),
      {
        month: 'short',
      }
    );

    startDate = startDate[0].toUpperCase() + startDate.slice(1);
    endDate = endDate[0].toUpperCase() + endDate.slice(1);

    return `${new Date(this.weekDays[0]).getDate()} ${startDate} - ${new Date(
      this.weekDays[this.weekDays.length - 1]
    ).getDate()} ${endDate}`;
  }

  public getLocateDateShortString(date: Date): string {
    return new Date(date).toLocaleString(this.translate.currentLang.toLowerCase(), { weekday: 'short' });
  }

  public getLocateDateLongString(date: Date): string {
    if (date !== undefined) {
      let tmpDate = new Date(date).toLocaleString(this.translate.currentLang.toLowerCase(), { weekday: 'long' });
      tmpDate = tmpDate[0].toUpperCase() + tmpDate.slice(1);
      return tmpDate + ',';
    }
    return '';
  }

  public getLocateMonthLongString(date: Date): string {
    if (date !== undefined) {
      let tmpDate = new Date(date).toLocaleString(this.translate.currentLang.toLowerCase(), { month: 'long' });
      tmpDate = tmpDate[0].toUpperCase() + tmpDate.slice(1);
      return tmpDate;
    }
    return '';
  }

  public isToday(date: Date): boolean {
    return (
      this.today.getDate() === new Date(date).getDate() &&
      this.today.getMonth() === new Date(date).getMonth() &&
      this.today.getFullYear() === new Date(date).getFullYear()
    );
  }

  public isSelected(date: Date): boolean {
    if (this.selectedDay !== undefined) {
      return (
        this.selectedDay.getDate() === new Date(date).getDate() &&
        this.selectedDay.getMonth() === new Date(date).getMonth() &&
        this.selectedDay.getFullYear() === new Date(date).getFullYear()
      );
    }
    return false;
  }

  public isPastDay(date: Date): boolean {
    const tmpToday = new Date(this.today);
    tmpToday.setHours(0, 0, 0);
    return tmpToday > new Date(date);
  }
}
