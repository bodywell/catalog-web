import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatePickerComponent } from './date-picker.component';
import {TranslateModule} from "@ngx-translate/core";
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { SharedModule } from '../shared.module';

@NgModule({
  declarations: [DatePickerComponent],
    imports: [
        CommonModule,
        TranslateModule,
        OverlayPanelModule,
        SharedModule
    ],
  exports: [
      DatePickerComponent
  ]
})
export class DatePickerModule { }
