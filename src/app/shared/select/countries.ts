export const countries = [
    { 'key': '41', 'value': 'Switzerland', 'img': 'assets/flags/205-switzerland.svg', 'country_code': 'ch' },
    { 'key': '49', 'value': 'Germany', 'img': 'assets/flags/162-germany.svg', 'country_code': 'de' },
    { 'key': '43', 'value': 'Austria', 'img': 'assets/flags/234-australia.svg', 'country_code': 'at' },
    { 'key': '33', 'value': 'France', 'img': 'assets/flags/195-france.svg', 'country_code': 'fr' },
    { 'key': '1', 'value': 'United States', 'img': 'assets/flags/226-united-states.svg', 'country_code': 'us' },
    { 'key': '44', 'value': 'Great Britain', 'img': 'assets/flags/260-united-kingdom.svg', 'country_code': 'gb' },
    { 'key': '372', 'value': 'Estonia', 'img': 'assets/flags/008-estonia.svg', 'country_code': 'ee' },
    { 'key': '370', 'value': 'Lithuania', 'img': 'assets/flags/064-lithuania.svg', 'country_code': 'lt' },
    { 'key': '380', 'value': 'Ukraine', 'img': 'assets/flags/145-ukraine.svg', 'country_code': 'ua' },
    { 'key': '7', 'value': 'Russia', 'img': 'assets/flags/248-russia.svg', 'country_code': 'ru' }
];
