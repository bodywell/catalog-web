import {Component, forwardRef, HostListener, Input, DoCheck, ViewEncapsulation, Output, EventEmitter} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SelectComponent),
    multi: true
  }],
  host: {
    '(document:click)': 'onClick($event)',
   }
})

export class SelectComponent implements ControlValueAccessor, DoCheck {

  apiUrl = environment.ApiUrl;
  isOpen = false;
  selectedValue: string;
  selectedKey: any;
  selectedImg: string;
  @Input() data;
  isInit = false;
  @Input() mode;
  @Input() selected;
  @Output() doSelect = new EventEmitter<any>();
  id = 'select' + (Math.random() * (1000000 - 1));

  // вызовем когда значение изменится
  onChange: Function;

  // вызовем при любом дествии пользователя с контроллом
  onTouched: Function;

  private _value: string;

  ngDoCheck() {
    if (!this.isInit && this.onChange) {
      if (this.selected != undefined) {
        const tmp = this.data.find(x => x.key == this.selected);
        if (!tmp) {
          return;
        }
        this.selectedValue = tmp['value'];
        this.selectedKey = tmp['key'];
        this.value = tmp['key'];
        if (this.data[0]['img']) {
          for (let i = 0; i < this.data.length; i++) {
            if (this.data[i]['value'] ==  this.selectedValue ) {
              this.selectedImg = this.data[i]['img'];
            }
          }
        }
      } else {
        if (this.data[0] != undefined) {
          this.selectedValue = this.data[0]['value'];
          this.value = this.data[0]['key'];
          if (this.data[0]['img']) {
            this.selectedImg = this.data[0]['img'];
          }
          this.isInit = true;
        }
      }
    }
  }
  get value(): string {
    return this._value;
  }

  // устанавливаем новое значение и сообщаем об этом форме
  set value(value: string) {
    this._value = value;
    if (this.onChange) {
      this.onChange(value);
    }
  }

  // реакция на клик хост-элемента, говорим форме что контрол "потрогали"
  @HostListener('click') click() {
    if (this.onTouched) {
      this.onTouched();
    }
  }

  selectElement(element) {
    this.value = element['key'];
    this.selectedKey = element['key'];
    this.selectedValue = element['value'];

    if (element['img']) {
      this.selectedImg = element['img'];
    }

    this.isOpen = false;
    this.doSelect.emit(element);
  }

  // вызовет форма если значение изменилось извне
  writeValue(obj: any): void {
    this.value = obj;
  }

  // сохраняем обратный вызов для изменений
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  // сохраняем обратный вызов для "касаний"
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  toggleSelect(event) {
    this.isOpen = !this.isOpen;
  }

  onClick(event) {
    // if (!('path' in Event.prototype)) {
    //   Object.defineProperty(Event.prototype, 'path', {
    //     get: function () {
    //       const path = [];
    //       let currentElem = this.target;
    //       while (currentElem) {
    //         path.push(currentElem);
    //         currentElem = currentElem.parentElement;
    //       }
    //       if (path.indexOf(window) === -1 && path.indexOf(document) === -1)
    //         path.push(document);
    //       if (path.indexOf(window) === -1)
    //         path.push(window);
    //       return path;
    //     }
    //   });
    // }
    for (let i = 0; i < event.path.length; i++) {
      if (event.path[i].id === this.id) {
        return;
      }
    }
    this.isOpen = false;
  }
}
