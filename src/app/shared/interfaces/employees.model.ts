export interface Employee {
  bookings: EmployeeBooking[];
  first_name: string | number;
  id: number;
  is_online_booking: number;
  is_personal_time: number;
  last_name: string | number;
  products: any[];
  role: number;
}

export interface EmployeeBooking {
  from_time: string;
  to_time: string;
  employees: [];
  product: EmployeeBookingProduct[];
}

export interface EmployeeBookingEmployees {
  id: number;
  employee_first_name: string | number;
  employee_last_name: string | number;
}

export interface EmployeeBookingProduct {
  branch_id: number;
  category_id: number;
  cost: number;
  description: string;
  duration: number;
  id: number;
  is_online_booking: number;
  price: number;
  price_max: number;
  time_start: string;
  time_step: number;
  title: string | number;
  type: number;
  vendor: string;
}
