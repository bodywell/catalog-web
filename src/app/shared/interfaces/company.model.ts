export interface CompanyBranch {
  city: string;
  country: string;
  description: string;
  groups: BranchGroup[];
  groups_personal: BranchGroup[];
  id: number;
  latitude: number;
  longitude: number;
  phones: number[];
  services: BranchService[];
  state: string;
  street: string;
  times: BranchTime[];
  title: string;
  zip: number;
}

export interface BranchTime {
  day: number;
  is_active: number;
  time: string;
}

export interface BranchService {
  category: string;
  description: string;
  duration: number;
  id: number;
  price: number;
  price_max: number;
  title: string;
  vendor: string;
}

export interface BranchGroup {
  description: string;
  duration: number;
  id: number;
  limit: number;
  subscription_price?: number;
  subscriptions: BranchGroupSubscription[];
  title: string;
  vendor: string;
}

export interface BranchGroupSubscription {
  activate: string | number;
  description: string;
  id: number;
  limit: number;
  price: number;
  price_max: number;
  time: string;
  title: string;
  vendor: string;
}

export interface CompanyPhoto {
  path: string;
  is_main: number;
}

export interface Company {
  branches: CompanyBranch[];
  currentBranch?: CompanyBranch;
  currency: string;
  description: string;
  email: string;
  fb_url: string;
  id: number;
  ig_url: string;
  industries: string[];
  industryTitle: string;
  industries_ids: number;
  firstThreeIndustries?: string;
  industry: string;
  online_payment: any;
  photos: CompanyPhoto[];
  rating: number;
  required_client_address: number;
  reviews: number;
  session: number;
  site_url: string;
  stripe_account: any;
  title: string;
  type: number;
}
