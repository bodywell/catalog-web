export interface Error {
  code: number;
  message: string;
}

export interface ApiResponseError {
  status: 'error';
  errors: Error[];
}

export interface ApiResponseSuccess<T> {
  status: 'success';
  data: T;
}
