export interface BookingTime {
  employees: BookingServiceTimeEmployees[] | BookingClassTimeEmployees[];
  product: BookingServiceTimeProduct | BookingClassTimeProduct;
  from_time: string;
  to_time: string;
}

export interface BookingEmptyClassTime {
  label: string;
  next_date: string;
}

export interface BookingEmptyServiceTime {
  label: string;
  next_date: string;
  times: BookingTime[];
}

export interface BookingServiceTimeEmployees {
  employee_first_name: string;
  employee_last_name: string;
  id: number;
}

export interface BookingClassTimeEmployees {
  first_name: string;
  last_name: string;
  id: number;
}

export interface BookingServiceTimeProduct {
  branch_id: number;
  category_id: number;
  cost: number;
  count: number;
  date: string;
  description: string;
  duration: number;
  id: number;
  is_archive: number;
  is_online_booking: number;
  price: number;
  price_max: number;
  time_start: string;
  time_step: number;
  title: string;
  type: number;
  vendor: string;
}

export interface BookingClassTimeProduct {
  branch_id: number;
  category_id: number;
  category_title: string;
  clients: number;
  cost: number;
  count: number;
  count_in_write_off_unit: number;
  date: string;
  description: string;
  duration: number;
  id: number;
  is_archive: number;
  is_online_booking: number;
  limit: number;
  price: number;
  price_max: number;
  time_start: string;
  time_step: number;
  title: string;
  type: number;
  unit_id_product: number;
  unit_id_write_off: number;
  unit_write_off_price: number;
  unit_write_off_value: number;
  vendor: string;
}
