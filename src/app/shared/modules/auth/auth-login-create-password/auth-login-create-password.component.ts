import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { take } from 'rxjs/operators';
import { ApiService } from 'src/app/core/service/api.service';

import { ErrorsService } from 'src/app/core/service/errors.service';
import { NotificationMessageService } from 'src/app/core/service/notification.service';
import { Steps } from '../auth.component';

@Component({
  selector: 'app-auth-login-create-password',
  templateUrl: 'auth-login-create-password.component.html',
  styleUrls: ['auth-login-create-password.component.scss'],
})
export class AuthLoginCreatePasswordComponent implements OnInit {
  public steps: typeof Steps = Steps;
  public isSubmitted = false;

  private timerInterval;
  public resendTimer = 0;
  public isResend = false;

  @Input()
  public loginForm: FormGroup;
  @Input()
  public phoneCodes = [];

  @Output()
  public changeStep: EventEmitter<any> = new EventEmitter();
  @Output()
  public submit: EventEmitter<any> = new EventEmitter();

  constructor(
    private notificationMessageService: NotificationMessageService,
    private apiService: ApiService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.startResendTimer();
  }

  public createPasswordAndLogin() {
    this.isSubmitted = true;
    if (!this.loginForm.value.login.password || !this.loginForm.value.login.smsCode) {
      return;
    }

    if (this.loginForm.value.login.password !== this.loginForm.value.login.password2) {
      return;
    }

    this.apiService
      .addPasswordToGoogleAcc(
        this.loginForm.value.login.phoneCode['phone_code'],
        this.loginForm.value.login.phoneNumber,
        this.loginForm.value.login.password,
        this.loginForm.value.login.smsCode
      )
      .pipe(take(1))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.isSubmitted = false;
          this.submit.emit();
          this.notificationMessageService.pushAlert({
            detail: this.translate.instant('notification-password-created'),
          });
        }
      });
  }

  public resendConfirmCode(): void {
    if (this.resendTimer > 0) {
      return;
    }

    this.apiService
      .resendPasswordVerifyCode(this.loginForm.value.phoneCode['phone_code'], this.loginForm.value.phoneNumber)
      .pipe(take(1))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.startResendTimer()
          this.notificationMessageService.pushAlert({ detail: this.translate.instant('notification-code-resended') });
        } else {
          res['errors'].forEach((error) => {
            if (error.code === 6) {
              this.notificationMessageService.pushAlert({
                detail: this.translate.instant('notification-activate-code-already-used'),
              });
            }
          });
        }
      });
  }

  private startResendTimer(): void {
    let eventTime = Number(window.localStorage.getItem('resendEmailTimer'));

    if (!eventTime) {
      eventTime = moment().add(3, 'minutes').unix();
      window.localStorage.setItem('resendEmailTimer', String(eventTime));
    }

    if (!isNaN(eventTime)) {
      const currentTime = moment().unix();
      const diffTime = eventTime - currentTime;
      let duration = moment.duration(diffTime, 'milliseconds');
      this.resendTimer = +duration;

      if (+duration > 0) {
        this.timerInterval = setInterval(() => {
          duration = moment.duration(+duration - 1, 'milliseconds');

          this.resendTimer = +duration;

          if (+duration <= 0) {
            window.localStorage.removeItem('resendEmailTimer');
            clearInterval(this.timerInterval);
          }
        }, 1000);
      } else {
        window.localStorage.removeItem('resendEmailTimer');
      }
    }
  }
}
