import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../../shared.module';

import { AuthComponent } from './auth.component';
import { AuthRegisterComponent } from './auth-register/auth-register.component';
import { AuthMainComponent } from './auth-main/auth-main.component';
import { AuthLoginComponent } from './auth-login/auth-login.component';
import { AuthHeaderComponent } from './auth-haeder/auth-header.component';
import { AuthPhoneConfirmComponent } from './auth-phone-confirm/auth-phone-confirm.component';
import { AuthAddressComponent } from './auth-address/auth-address.component';
import { AuthLoginPasswordComponent } from './auth-login-password/auth-login-password.component';
import { AuthLoginCreatePasswordComponent } from './auth-login-create-password/auth-login-create-password.component';
import { AuthLoginRestorePasswordComponent } from './auth-login-restore-password/auth-login-restore-password.component';

@NgModule({
  imports: [CommonModule, SharedModule, TranslateModule.forChild()],
  exports: [
    AuthComponent,
    AuthHeaderComponent,
    AuthMainComponent,
    AuthLoginComponent,
    AuthLoginPasswordComponent,
    AuthLoginCreatePasswordComponent,
    AuthLoginRestorePasswordComponent,
    AuthRegisterComponent,
    AuthPhoneConfirmComponent,
    AuthAddressComponent,
  ],
  declarations: [
    AuthComponent,
    AuthHeaderComponent,
    AuthMainComponent,
    AuthLoginComponent,
    AuthLoginPasswordComponent,
    AuthLoginCreatePasswordComponent,
    AuthLoginRestorePasswordComponent,
    AuthRegisterComponent,
    AuthPhoneConfirmComponent,
    AuthAddressComponent,
  ],
  providers: [],
})
export class AuthLoginModule {}
