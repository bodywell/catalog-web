import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Steps } from '../auth.component';

@Component({
  selector: 'app-auth-main',
  templateUrl: 'auth-main.component.html',
  styleUrls: ['auth-main.component.scss'],
})
export class AuthMainComponent implements OnInit {
  public steps: typeof Steps = Steps;

  @Output()
  public changeStep: EventEmitter<number> = new EventEmitter();

  constructor() {}

  ngOnInit() {}
}
