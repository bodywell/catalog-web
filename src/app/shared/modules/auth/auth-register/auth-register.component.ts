import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { ErrorsService } from 'src/app/core/service/errors.service';

@Component({
  selector: 'app-auth-register',
  templateUrl: 'auth-register.component.html',
  styleUrls: ['auth-register.component.scss'],
})
export class AuthRegisterComponent implements OnInit {
  public isSubmitted = false;

  @Input()
  public registerForm: FormGroup;
  @Input()
  public phoneCodes = [];

  @Output()
  public goToLogin: EventEmitter<any> = new EventEmitter();
  @Output()
  public submit: EventEmitter<any> = new EventEmitter();

  constructor(private errorsService: ErrorsService) {}

  ngOnInit() {
    /*this.controls.name.reset();
    this.controls.surname.reset();
    this.controls.email.reset();
    this.controls.phoneNumber.reset();
    this.controls.password.reset();
    this.controls.iAgree.reset();*/
  }

  private validateForm() {
    this.errorsService.checkFirstName(this.controls.name.value, this.controls.name);
    this.errorsService.checkLastName(this.controls.surname.value, this.controls.surname);
    this.errorsService.checkEmail(this.controls.email.value, this.controls.email);
    this.errorsService.checkPhone(this.controls.phoneNumber.value, this.controls.phoneNumber);
    this.errorsService.checkPassword(this.controls.password.value, this.controls.password);
    this.errorsService.checkIsEmpty(this.controls.iAgree.value, this.controls.iAgree, 'apply-terms');
  }

  public onSubmit(): void {
    this.isSubmitted = true;

    this.validateForm();

    const isErrors = Object.values(this.controls).some((control) => {
      return control.errors && Object.keys(control.errors).some((key) => control.errors[key]);
    });

    if (!isErrors) {
      this.submit.emit(true);
    }
  }

  private get controls(): { [key: string]: AbstractControl } {
    return this.registerForm.controls;
  }
}
