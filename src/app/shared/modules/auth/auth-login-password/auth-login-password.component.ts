import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { ErrorsService } from 'src/app/core/service/errors.service';
import { Steps } from '../auth.component';

@Component({
  selector: 'app-auth-login-password',
  templateUrl: 'auth-login-password.component.html',
  styleUrls: ['auth-login-password.component.scss'],
})
export class AuthLoginPasswordComponent implements OnInit {
  public steps: typeof Steps = Steps;
  public isSubmitted = false;

  @Input()
  public loginForm: FormGroup;
  @Input()
  public phoneCodes = [];

  @Output()
  public changeStep: EventEmitter<any> = new EventEmitter();
  @Output()
  public submit: EventEmitter<any> = new EventEmitter();

  constructor(private errorsService: ErrorsService) {}

  ngOnInit() {
    if (typeof this.loginForm.controls['password2'] !== 'undefined') {
      const password2Control = this.loginForm.get('password2');

      if (typeof password2Control.errors !== 'undefined' && typeof password2Control.errors.required !== 'undefined') {
        password2Control.clearValidators();
        password2Control.updateValueAndValidity();
      }
    }
    if (typeof this.loginForm.controls['smsCode'] !== 'undefined') {
      const smsCodeControl = this.loginForm.get('smsCode');

      if (typeof smsCodeControl.errors !== 'undefined' && typeof smsCodeControl.errors.required !== 'undefined') {
        smsCodeControl.clearValidators();
        smsCodeControl.updateValueAndValidity();
      }
    }
  }

  public goToRestorePassword(): void {
    this.loginForm.controls.phoneNumber.reset();
    this.changeStep.emit(this.steps.restorePasswordStepOne);
  }

  public onSubmit(): void {
    this.isSubmitted = true;

    this.errorsService.checkPassword(this.controls.password.value, this.controls.password);
    const isErrors = Object.values(this.controls).some((control) => {
      return control.errors && Object.keys(control.errors).some((key) => control.errors[key]);
    });

    if (!isErrors) {
      this.submit.emit({
        phone_code: this.controls.phoneCode.value.phone_code,
        phone_number: this.controls.phoneNumber.value,
        password: this.controls.password.value,
      });
    }
  }

  private get controls(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }
}
