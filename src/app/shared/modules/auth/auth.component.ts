import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { of, Subject } from 'rxjs';
import moment from 'moment';

import { ApiService } from 'src/app/core/service/api.service';
import { commonDataService } from 'src/app/core/service/commonData.service';
import { LocationService } from 'src/app/core/service/location.service';
import { NotificationMessageService } from 'src/app/core/service/notification.service';
import { GapiCalendarService } from 'src/app/core/service/gapi.calendar.service';
import { AppAuthService } from 'src/app/core/service/auth.service';

export enum Steps {
  main = 1,
  login = 2,
  register = 3,
  phoneConfirm = 4,
  address = 5,
  loginPassword = 6,
  restorePasswordStepOne = 7,
  restorePasswordStepTwo = 8,
  createPassword = 9,
}

@Component({
  selector: 'app-auth',
  templateUrl: 'auth.component.html',
  styles: [
    `
      .auth-wrapper {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 2;
        background-color: #fff;
      }
    `,
  ],
})
export class AuthComponent implements OnInit {
  public authForm: FormGroup;
  public steps: typeof Steps = Steps;
  public step = Steps.main;

  public phoneCodes = [];

  @Output() close: EventEmitter<boolean> = new EventEmitter();

  private destroy: Subject<any> = new Subject();

  constructor(
    private fb: FormBuilder,
    private locationService: LocationService,
    private apiService: ApiService,
    private commonDataService: commonDataService,
    private translate: TranslateService,
    private notificationMessageService: NotificationMessageService,
    public calendar: GapiCalendarService,
    private appAuthService: AppAuthService
  ) {}

  ngOnInit() {
    this.authForm = this.fb.group({
      login: this.fb.group({
        phoneCode: [
          {
            code: null,
            phone_code: null,
          },
        ],
        phoneNumber: [null, [Validators.minLength(8), Validators.maxLength(22), Validators.required]],
        password: [null, [Validators.required]],
        password2: [null, [Validators.required]],
        smsCode: [null, [Validators.required]],
      }),
      register: this.fb.group({
        name: ['', [Validators.required]],
        surname: ['', [Validators.required]],
        phoneCode: [
          {
            code: null,
            phone_code: null,
          },
        ],
        phoneNumber: [null, [Validators.minLength(8), Validators.maxLength(22), Validators.required]],
        email: [''],
        password: [null, [Validators.required]],
        iAgree: [null, [Validators.required]],
      }),
    });

    this.locationService.userIpDefined
      .pipe(
        mergeMap((status) => {
          if (status !== null) {
            return this.commonDataService.getPhoneCodes().pipe(
              tap((phoneCodes) => {
                this.setPhoneData(phoneCodes);
              })
            );
          }

          return of(status);
        }),
        takeUntil(this.destroy)
      )
      .subscribe();
  }

  changeStep(step) {
    this.step = step;
  }

  private setPhoneData(phoneCodes): void {
    this.phoneCodes = [].concat(phoneCodes);

    const phoneCode = phoneCodes.find(
      (i) => i.value.code === String(this.locationService.userIpData.code).toLowerCase()
    );

    const formPhoneCode = {
      code: this.apiService.user.phone_country
        ? String(this.apiService.user.phone_country).toLowerCase()
        : String(phoneCode.value.code).toLowerCase(),
      phone_code: this.apiService.user.phone_code || phoneCode.value.phone_code,
    };

    this.authForm.controls.login.patchValue({
      phoneCode: formPhoneCode,
    });

    this.authForm.controls.register.patchValue({
      phoneCode: formPhoneCode,
    });
  }

  public checkPhonePassword(): void {
    if (!this.authForm.value.login.phoneCode['phone_code'] && !this.authForm.value.login.phoneNumber) {
      return;
    }

    this.apiService
      .checkPhoneNumber(this.authForm.value.login.phoneCode['phone_code'], this.authForm.value.login.phoneNumber)
      .pipe(take(1))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          if (res['data']['hasPassword']) {
            this.step = Steps.loginPassword;
          }
          if (!res['data']['hasPassword']) {
            this.step = Steps.createPassword;
          }
        } else if (res['status'] === 'error') {
          for (let i = 0; i < res['errors'].length; i++) {
            if (res['errors'][i]['description'] === 'Phone does not exist!') {
              this.loginControls.phoneNumber.setErrors({ 'phone-does-not-exist': true });
            }
          }
        }
      });
  }

  public createPasswordAndLogin() {
    this.onLogin({
      phone_code: this.authForm.value.login.phoneCode['phone_code'],
      phone_number: this.authForm.value.login.phoneNumber,
      password: this.authForm.value.login.password,
    });
  }

  public confirmedPhoneAndLogin() {
    if (this.apiService.token) {
      this.close.emit(true);
    } else {
      this.onLogin({
        phone_code: this.apiService['user']['phone_code'],
        phone_number: this.apiService['user']['phone_number'],
        password: this.apiService['user']['password'],
      });
    }
  }

  public register(): void {
    const registerInfo = {
      first_name: this.registerControls.name.value,
      last_name: this.registerControls.surname.value,
      email: this.registerControls.email.value,
      phone_country: this.registerControls.phoneCode.value.code,
      phone_code: this.registerControls.phoneCode.value.phone_code,
      phone_number: this.registerControls.phoneNumber.value,
      password: this.registerControls.password.value,
      language: this.translate.currentLang.toLowerCase(),
    };

    const refRecords = window.localStorage.getItem('ref');
    if (refRecords) {
      registerInfo['invite_code'] = this.appAuthService.getInvites(refRecords);
    }

    this.apiService
      .register(registerInfo)
      .pipe(
        switchMap((res) => {
          if (res['status'] === 'success') {
            const eventTime = moment().add(3, 'minutes').unix();
            this.apiService['user'] = registerInfo;
            window.localStorage.setItem('resendEmailTimer', String(eventTime));
            this.changeStep(this.steps.phoneConfirm);
          } else if (res['status'] === 'error') {
            for (let i = 0; i < res['errors'].length; i++) {
              if (res['errors'][i]['code'] === 4) {
                if (res['errors'][i]['description'] === 'Phone already exist!') {
                  this.registerControls.phoneNumber.setErrors({ exist: true });
                } else {
                  this.registerControls.email.setErrors({ exist: true });
                }
              } else if (res['errors'][i]['code'] === 5) {
                this.registerControls.password.setErrors({ passwordError: true });
              } else if (res['errors'][i]['code'] === 12) {
                this.notificationMessageService.pushAlert({
                  detail: this.translate.instant('notification-error-reg-email-social-exists'),
                });
              }
            }
          }

          return of(res);
        }),
        take(1)
      )
      .subscribe();
  }

  public onLogin(data) {
    this.login(data).pipe(take(1)).subscribe();
  }

  public login({ phone_code, phone_number, password }) {
    return this.apiService.login({ phoneCode: phone_code, phoneNumber: phone_number, password }).pipe(
      mergeMap((res) => {
        if (res['status'] === 'success') {
          return this.apiService.getSelfUser(res['data']['token']).pipe(
            tap((selfUserResponse) => {
              if (selfUserResponse['status'] === 'success') {
                this.apiService.user = selfUserResponse['data'];

                let syncEmail = selfUserResponse['data']['calendar_sync_email']
                  ? selfUserResponse['data']['calendar_sync_email']
                  : '';

                this.calendar.syncEmail.next(syncEmail);

                window.localStorage.setItem('token', res['data']['token']);
                this.apiService.token = res['data']['token'];

                if (!selfUserResponse['data']['is_phone']) {
                  this.appAuthService.isEmailConfirm.next(false);
                  this.apiService.resendConfirmCode().pipe(take(1)).subscribe();
                  this.changeStep(this.steps.phoneConfirm);
                } else {
                  this.close.emit(true);
                  this.appAuthService.finishRegEvent.emit(true);
                }
              }
            })
          );
        } else if (res['status'] === 'error') {
          for (let i = 0; i < res['errors'].length; i++) {
            if (res['errors'][i]['code'] === 5) {
              this.loginControls.password.setErrors({ 'invalid-login-password-or-phone': true });
            }
          }
        }

        return of(res);
      }),
      take(1)
    );
  }

  back() {
    switch (this.step) {
      case this.steps.login:
      case this.steps.register:
        this.step = this.steps.main;
        break;

      case this.steps.loginPassword:
      case this.steps.createPassword:
        this.step = this.steps.login;
        break;

      case this.steps.restorePasswordStepTwo:
        this.step = this.steps.restorePasswordStepOne;
        break;

      case this.steps.phoneConfirm:
        this.step = this.steps.register;
        break;

      default:
        break;
    }
  }

  get registerControls(): { [key: string]: AbstractControl } {
    return (this.authForm.controls.register as FormGroup).controls;
  }
  get loginControls(): { [key: string]: AbstractControl } {
    return (this.authForm.controls.login as FormGroup).controls;
  }
}
