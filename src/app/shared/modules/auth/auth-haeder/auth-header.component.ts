import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Steps } from '../auth.component';

@Component({
  selector: 'app-auth-header',
  templateUrl: 'auth-header.component.html',
  styleUrls: ['auth-header.component.scss'],
})
export class AuthHeaderComponent implements OnInit {
  public steps: typeof Steps = Steps;

  private titles = [
    'modal-confirmation',
    'auth-modal-login-title',
    'register-modal-heading-step-one',
    'register-confirm-code-title',
    'booking-confirm-set-position-title',
    'auth-modal-login-title',
    'auth-modal-forgot-password',
    'forgot-forgot-your-password',
    'auth-modal-login-password-create-title',
  ];

  @Input()
  public readonly step;
  @Input()
  public loginForm: FormGroup;

  @Output()
  public back: EventEmitter<null> = new EventEmitter();
  @Output()
  public close: EventEmitter<null> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  public get title(): string {
    return this.titles[this.step - 1] || '';
  }
}
