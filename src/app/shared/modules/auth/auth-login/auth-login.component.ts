import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { ErrorsService } from 'src/app/core/service/errors.service';
import { Steps } from '../auth.component';

@Component({
  selector: 'app-auth-login',
  templateUrl: 'auth-login.component.html',
  styleUrls: ['auth-login.component.scss'],
})
export class AuthLoginComponent implements OnInit {
  public steps: typeof Steps = Steps;
  public isSubmitted = false;

  @Input()
  public loginForm: FormGroup;
  @Input()
  public phoneCodes = [];

  @Output()
  public changeStep: EventEmitter<any> = new EventEmitter();
  @Output()
  public submit: EventEmitter<any> = new EventEmitter();

  constructor(private errorsService: ErrorsService) {}

  ngOnInit() {}

  public onSubmit(): void {
    this.isSubmitted = true;

    this.errorsService.checkPhone(this.controls.phoneNumber.value, this.controls.phoneNumber);

    this.submit.emit();
  }

  private get controls(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }
}
