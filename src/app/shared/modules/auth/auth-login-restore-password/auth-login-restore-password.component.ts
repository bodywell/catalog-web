import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { ApiService } from 'src/app/core/service/api.service';
import { NotificationMessageService } from 'src/app/core/service/notification.service';
import { Steps } from '../auth.component';

@Component({
  selector: 'app-auth-login-restore-password',
  templateUrl: 'auth-login-restore-password.component.html',
  styleUrls: ['auth-login-restore-password.component.scss'],
})
export class AuthLoginRestorePasswordComponent implements OnInit {
  public steps: typeof Steps = Steps;
  public isSubmitted = false;

  private timerInterval;
  public resendTimer = 0;
  public isResend = false;

  @Input()
  public loginForm: FormGroup;
  @Input()
  public phoneCodes = [];
  @Input()
  public step = this.steps.restorePasswordStepOne;

  @Output()
  public changeStep: EventEmitter<any> = new EventEmitter();
  @Output()
  public submit: EventEmitter<any> = new EventEmitter();

  constructor(
    private apiService: ApiService,
    private notificationMessageService: NotificationMessageService,
    private translate: TranslateService
  ) {}

  ngOnInit() {}

  public onPhoneSubmit(): void {
    this.isSubmitted = true;
    
    if (!this.loginForm.value.phoneNumber) {
      return;
    }

    this.sendRestorePasswordCode()
      .pipe(take(1))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.isSubmitted = false;
          this.changeStep.emit(this.steps.restorePasswordStepTwo);
          this.startResendTimer();
        }
      });
  }

  public onResendRestoreCode(): void {
    if (this.resendTimer > 0) {
      return;
    }

    this.sendRestorePasswordCode().pipe(take(1)).subscribe();
  }

  public onSubmitPassword(): void {
    this.isSubmitted = true;

    if (!this.loginForm.value.smsCode || !this.loginForm.value.password || !this.loginForm.value.password2) {
      return;
    }

    if (this.loginForm.value.password !== this.loginForm.value.password2) {
      return;
    }

    this.apiService.forgotFinish(this.loginForm.value.smsCode, this.loginForm.value.password).subscribe((response) => {
      if (response['status'] === 'success') {
        this.isSubmitted = false;
        this.submit.emit();
        this.loginForm.controls.phoneNumber.reset();
        this.loginForm.controls.password.reset();
        this.loginForm.controls.password2.reset();
        this.notificationMessageService.pushAlert({
          detail: this.translate.instant('notification-success-password-change'),
        });
      } else {
        response['errors'].forEach((error) => {
          this.notificationMessageService.pushAlert({
            detail: this.translate.instant(
              `notification-restore-password-error-${error['code'] ? error['code'] : 'unknown'}`
            ),
          });
        });
      }
    });
  }

  public sendRestorePasswordCode(): Observable<any> {
    return this.apiService.forgot(this.loginForm.value.phoneCode.phone_code, this.loginForm.value.phoneNumber).pipe(
      tap((res) => {
        if (res['status'] === 'success') {
          this.startResendTimer();
        } else if (res['status'] === 'error') {
          for (let i = 0; i < res['errors'].length; i++) {
            this.loginForm.controls.phoneNumber.setErrors({ 'phone-does-not-exist': true });
          }
        }
      })
    );
  }

  private startResendTimer(): void {
    let eventTime = Number(window.localStorage.getItem('resendEmailTimer'));

    if (!eventTime) {
      eventTime = moment().add(3, 'minutes').unix();
      window.localStorage.setItem('resendEmailTimer', String(eventTime));
    }

    if (!isNaN(eventTime)) {
      const currentTime = moment().unix();
      const diffTime = eventTime - currentTime;
      let duration = moment.duration(diffTime, 'milliseconds');
      this.resendTimer = +duration;

      if (+duration > 0) {
        this.timerInterval = setInterval(() => {
          duration = moment.duration(+duration - 1, 'milliseconds');

          this.resendTimer = +duration;

          if (+duration <= 0) {
            window.localStorage.removeItem('resendEmailTimer');
            clearInterval(this.timerInterval);
          }
        }, 1000);
      } else {
        window.localStorage.removeItem('resendEmailTimer');
      }
    }
  }
}
