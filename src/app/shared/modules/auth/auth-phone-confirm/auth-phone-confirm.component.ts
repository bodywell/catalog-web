import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import moment from 'moment';
import { take } from 'rxjs/operators';
import { ApiService } from 'src/app/core/service/api.service';
import { NotificationMessageService } from 'src/app/core/service/notification.service';
import { Steps } from '../auth.component';

@Component({
  selector: 'app-auth-phone-confirm',
  templateUrl: 'auth-phone-confirm.component.html',
  styleUrls: ['auth-phone-confirm.component.scss'],
})
export class AuthPhoneConfirmComponent implements OnInit {
  public steps: typeof Steps = Steps;
  public isSubmitted = false;

  private timerInterval;
  public resendTimer = 0;
  public isResend = false;

  public approveCode = new FormGroup({
    code: new FormControl('', Validators.required)
  });

  @Output() public changeStep: EventEmitter<any> = new EventEmitter();
  @Output() public submit: EventEmitter<any> = new EventEmitter();

  constructor(
    public apiService: ApiService,
    private notificationMessageService: NotificationMessageService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    const eventTime = moment().add(3, 'minutes').unix();
    window.localStorage.setItem('resendEmailTimer', String(eventTime));
    this.initTimer();
  }

  private initTimer(): void {
    const eventTime = Number(window.localStorage.getItem('resendEmailTimer'));

    if (!isNaN(eventTime)) {
      const currentTime = moment().unix();
      const diffTime = eventTime - currentTime;
      let duration = moment.duration(diffTime, 'milliseconds');
      this.resendTimer = +duration;

      if (+duration > 0) {
        this.timerInterval = setInterval(() => {
          duration = moment.duration(+duration - 1, 'milliseconds');

          this.resendTimer = +duration;

          if (+duration <= 0) {
            window.localStorage.removeItem('resendEmailTimer');
            clearInterval(this.timerInterval);
          }
        }, 1000);
      } else {
        window.localStorage.removeItem('resendEmailTimer');
      }
    }
  }

  sendCode() {
    this.isSubmitted = true;
    this.approveCode.get('code').setErrors({ invalid: false });

    if (this.approveCode.get('code').getError('required')) {
      return;
    }
    this.apiService
      .approvePhone(this.apiService['user']['phone_code'], this.apiService['user']['phone_number'], this.approveCode.get('code').value)
      .pipe(take(1))
      .subscribe((data) => {
        if (data['status'] === 'success') {
          this.apiService.user['is_phone'] = 1;
          this.submit.emit(true);
        } else if (data['status'] === 'error') {
          this.approveCode.get('code').setErrors({ invalid: true });
        }
      });
  }

  public resendConfirmCode(): void {
    if (this.resendTimer > 0) {
      return;
    }

    this.apiService
      .resendConfirmCode()
      .pipe(take(1))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          const eventTime = moment().add(3, 'minutes').unix();
          const currentTime = moment().unix();
          const diffTime = eventTime - currentTime;
          let duration = moment.duration(diffTime, 'milliseconds');

          this.resendTimer = +duration;
          this.isResend = true;

          window.localStorage.setItem('resendEmailTimer', String(eventTime));

          this.timerInterval = setInterval(() => {
            duration = moment.duration(+duration - 1, 'milliseconds');

            this.resendTimer = +duration;

            if (+duration <= 0) {
              window.localStorage.removeItem('resendEmailTimer');
              clearInterval(this.timerInterval);
            }
          }, 1000);

          this.notificationMessageService.pushAlert({ detail: this.translate.instant('notification-code-resended') });
        } else {
          res['errors'].forEach((error) => {
            if (error.code === 6) {
              this.notificationMessageService.pushAlert({
                detail: this.translate.instant('notification-activate-code-already-used'),
              });
            }
          });
        }
      });
  }
}
