import {
  Component,
  Input,
  OnInit,
  forwardRef,
  ViewEncapsulation,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

export interface ISelectedPhoneCode {
  name: string;
  code: string;
  phone_code: number;
}

export interface IPhoneCodes {
  label: string;
  value: {
    name: string;
    code: string;
    phone_code: string | number;
    country?: string;
  };
}

@Component({
  selector: 'app-phone-code-select',
  templateUrl: './phone-code-select.component.html',
  styleUrls: ['./phone-code-select.component.scss'],
  // tslint:disable-next-line
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PhoneCodeSelectComponent),
      multi: true,
    },
  ],
})
export class PhoneCodeSelectComponent implements ControlValueAccessor, OnInit, OnChanges {
  public phoneCodes: Array<IPhoneCodes> = [];
  public selectedPhoneCode: ISelectedPhoneCode;
  public isOpen = false;
  public disabled: boolean;

  onChange: (value: ISelectedPhoneCode) => void;
  onTouched: () => void;
  
  @Input() defaultValue: ISelectedPhoneCode;
  @Input() options: Array<IPhoneCodes> = [];
  @Input() appendTo = null;

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.options &&
      (!changes.options.previousValue || !changes.options.previousValue.length) &&
      changes.options.currentValue.length
    ) {
      this.phoneCodes = [].concat(this.options);
    }
  }

  onChangeHandler(e) {
    this.selectedPhoneCode = e;
    this.onChange(e);
  }

  ngOnInit() {}

  rotateIcon() {
    this.isOpen = !this.isOpen;
  }

  public searchPhoneCode(term: string): void {
    if (!term) {
      this.phoneCodes = [].concat(this.options);
    }

    const phoneCodes = this.options.filter((phoneCode: IPhoneCodes) => {
      const _term = term.replace(/\+/, '');
      const phone_code = String(phoneCode.value.phone_code);
      const name = phoneCode.value.name.split(' ');
      const isNameExists = name.some(
        (i: string) =>
          String(i)
            .toLowerCase()
            .indexOf(_term) !== -1
      );

      return phone_code.indexOf(_term) !== -1 || isNameExists;
    });

    this.phoneCodes = [].concat(phoneCodes);
  }

  // вызовет форма если значение изменилось извне
  writeValue(obj: any): void {
    if (!obj) {
      return;
    }
    this.selectedPhoneCode = obj;
  }

  // сохраняем обратный вызов для изменений
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  // сохраняем обратный вызов для "касаний"
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
