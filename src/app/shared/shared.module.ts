import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectModule } from './select/select.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { NumericDirective } from './directives/numeric.directive';
import { AgmCoreModule } from '@agm/core';
import { BarRatingModule } from 'ngx-bar-rating';
import { AccordionModule } from './accordion/accordion.module';
import { DatePickerModule } from './date-picker/date-picker.module';
import { TabModule } from './tab/tab.module';
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';
import { DpDatePickerModule } from 'ng2-date-picker';
import { PhotoSliderComponent } from './photo-slider/photo-slider.component';
import { RecommendedContentComponent } from './recommended-content/recommended-content.component';
import { PopularContentComponent } from './popular-content/popular-content.component';
import { DaterangePickerComponent } from './daterange-picker/daterange-picker.component';
import { NgFooterDirective } from './daterange-picker/ng-templates.directive';
import { DateFormatPipe } from './pipes/date-format.pipe';
import { NgSelectModule } from '@ng-select/ng-select';
import { ModalComponent } from './modal/modal.component';
import { TooltipDirective } from './directives/tooltip.directive';
import { PhoneCodeSelectComponent } from './phone-code-select/phone-code-select.component';
import { InputComponent } from './components/input/input.component';
import { SubscriptionsModalComponent } from '../pages/card/components/subscriptions-modal/subscriptions-modal.component';
import { Utf8DecodePipe } from './pipes/utf8decode.pipe';
import { BtnSwitchComponent } from './btn-switch/btn-switch.component';
import { ProductCardComponent } from './components/product-card/product-card.component';
import {CompanyHeaderComponent} from './components/company-header/company-header.component';
import {SubscriptionsDetailedModalComponent} from './components/subscriptions-detailed-modal/subscriptions-detailed-modal.component';
import {WidgetTabsComponent} from './components/widget-tabs/widget-tabs.component';

@NgModule({
  declarations: [
    TooltipDirective,
    NumericDirective,
    PopularContentComponent,
    RecommendedContentComponent,
    DaterangePickerComponent,
    NgFooterDirective,
    DateFormatPipe,
    ModalComponent,
    BtnSwitchComponent,
    PhoneCodeSelectComponent,
    InputComponent,
    ProductCardComponent,
    SubscriptionsModalComponent,
    CompanyHeaderComponent,
    SubscriptionsDetailedModalComponent,
    WidgetTabsComponent,
    Utf8DecodePipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SelectModule,
    // AccordionModule,
    TranslateModule,
    RouterModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCokv39DsotQK9pui7dzIpbuQXjGHItq-k',
    }),
    AgmSnazzyInfoWindowModule,
    BarRatingModule,
    TabModule,
    DpDatePickerModule,
    NgSelectModule,
  ],
  exports: [
    SelectModule,
    // AccordionModule,
    FormsModule,
    TranslateModule,
    RouterModule,
    ReactiveFormsModule,
    NumericDirective,
    TooltipDirective,
    AgmCoreModule,
    BarRatingModule,
    TabModule,
    PopularContentComponent,
    RecommendedContentComponent,
    DaterangePickerComponent,
    ModalComponent,
    BtnSwitchComponent,
    DateFormatPipe,
    PhoneCodeSelectComponent,
    InputComponent,
    ProductCardComponent,
    SubscriptionsModalComponent,
    CompanyHeaderComponent,
    SubscriptionsDetailedModalComponent,
    WidgetTabsComponent,
    Utf8DecodePipe,
  ],
})
export class SharedModule {}
