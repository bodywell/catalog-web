import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {

  isOpen = false;
  @Input() type = 'BLOCK';
  @Input() data: object[];
  @Input() category: string;
  @Input() currency: string;
  @Input() isPersonalClass: any = false;
  @Input() enableSubscriptionModal = false;
  @Output() selectItem = new EventEmitter<object>();
  @Output() showPassInfo = new EventEmitter<object>();

  constructor() { }

  ngOnInit() {
  }

  toggle() {
    this.isOpen = !this.isOpen;
  }

  select(item) {
    this.selectItem.emit(item);
  }

  viewPassInfo(item) {
    this.showPassInfo.emit(item);
  }

}
