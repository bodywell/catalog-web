import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccordionComponent } from './accordion.component';
import { SharedModule } from '../shared.module';
import {Accordion2Component} from '../accordion2/accordion2.component';

@NgModule({
  declarations: [
      AccordionComponent,
      Accordion2Component
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
      AccordionComponent,
      Accordion2Component
  ]
})
export class AccordionModule { }
