import { Component, forwardRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import {
  ControlContainer,
  ControlValueAccessor,
  FormControl,
  FormControlDirective,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';
import { isEqual } from 'lodash';

@Component({
  selector: 'app-input',
  templateUrl: 'input.component.html',
  styleUrls: ['input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true,
    },
  ],
})
export class InputComponent implements ControlValueAccessor, OnInit, OnChanges {
  public parsedErrors: string[] = [];

  @Input() type = 'text';
  @Input() placeholder: string | number;
  @Input() errors = {};
  @Input() disabled = false;
  @Input() icon: string;
  @Input() iconPosition: string;
  @Input()
  private formControl: FormControl;

  @ViewChild(FormControlDirective, { static: true })
  private formControlDirective: FormControlDirective;

  @Input()
  private formControlName: string;

  get control() {
    return this.formControl || this.controlContainer.control.get(this.formControlName);
  }

  onChange: (value: string | number) => void;
  onTouched: () => void;

  constructor(private controlContainer: ControlContainer) {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.errors) {
      const { previousValue, currentValue } = changes.errors;

      if (!currentValue) {
        this.parsedErrors = [];
      } else if (!isEqual(previousValue || {}, currentValue || {})) {
        this.parsedErrors = Object.keys(this.errors).reduce((acc, key) => {
          if (this.errors['required']) {
            acc.push('empty-field')
          } else if (this.errors[key]) {
            acc.push(key)
          }
          return acc;
        }, []);
      }
    }
  }

  change(value) {
    this.onChange(value);
  }

  registerOnTouched(fn: any): void {
    this.formControlDirective.valueAccessor.registerOnTouched(fn);
  }

  registerOnChange(fn: any): void {
    this.formControlDirective.valueAccessor.registerOnChange(fn);
  }

  writeValue(obj: any): void {
    this.formControlDirective.valueAccessor.writeValue(obj);
  }

  setDisabledState(isDisabled: boolean): void {
    this.formControlDirective.valueAccessor.setDisabledState(isDisabled);
  }
}
