import {Component, DoCheck, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {productType} from '../../constants/product';
import {ApiService} from '../../../core/service/api.service';

@Component({
    selector: 'app-product-card',
    templateUrl: 'product-card.component.html',
    styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent implements OnInit, DoCheck {
    productType = productType;
    clientProducts = [];
    Object = Object;

    @Input() data: any = [];
    @Input() type: productType;
    @Input() enablePurchase: any = false;
    @Input() enablePaymentMode: any = false;

    @Output() showDetails = new EventEmitter<object>();
    @Output() selectItem = new EventEmitter<object>();

    constructor(private apiService: ApiService) {
        this.clientProducts = (apiService.user['client_products']) ? apiService.user['client_products'] : [];
    }

    ngOnInit() {
    }

    ngDoCheck() {
        this.clientProducts = (this.apiService.user['client_products']) ? this.apiService.user['client_products'] : [];
    }

    emitDetails(data) {
        this.showDetails.emit(data);
    }

    emitSelect(data) {
        this.selectItem.emit(data);
    }
}
