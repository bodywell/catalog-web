import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';


@Component({
    selector: 'app-company-header',
    templateUrl: './company-header.component.html',
    styleUrls: ['./company-header.component.scss']
})
export class CompanyHeaderComponent implements OnInit {

    @Input() title;
    @Input() industry;
    @Input() countReview;
    @Input() rating;
    @Input() companyId;
    @Input() branchId;
    apiUrl: string = environment.ApiUrl;

    constructor() { }

    ngOnInit() {
    }

}
