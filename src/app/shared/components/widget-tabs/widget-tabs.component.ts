import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BookingService} from '../../../core/service/booking.service';
import {productType} from '../../constants/product';

@Component({
    selector: 'app-widget-tabs',
    templateUrl: './widget-tabs.component.html',
    styleUrls: ['widget-tabs.component.scss']
})
export class WidgetTabsComponent implements OnInit {

    companyId;
    branchId;
    session;
    hasPurchaseSegment: any = false;
    productType = productType;

    constructor(private route: ActivatedRoute, private router: Router, private bookingService: BookingService) { }

    ngOnInit() {
        this.route.url.subscribe(segments => {
            this.hasPurchaseSegment = segments.some(segment => segment.path === 'purchase');
        });

        this.bookingService.isLoaded.subscribe((status) => {
            if (status) {
                const queryParams = this.bookingService.queryParams;
                const companyInfo = this.bookingService.companyInfo;

                this.companyId = queryParams['companyId'];
                this.branchId = queryParams['branchId'];
                this.session = companyInfo['session'];
            }
        });
    }

    booking() {
        const route = (this.session) ? '/booking/type' : '/booking/service';
        this.router.navigate([`/widgets${route}`], {
            queryParams: {
                company_id: this.companyId,
                branch_id: this.branchId,
                type: (this.session) ? productType.SUBSCRIPTIONS : productType.COURSE
            },
        });
    }

    purchase() {
        this.router.navigate([`widgets/booking/purchase/products`], {
            queryParams: {
                company_id: this.companyId,
                branch_id: this.branchId,
                type: (this.session) ? productType.SUBSCRIPTIONS : productType.COURSE
            },
        });
    }
}
