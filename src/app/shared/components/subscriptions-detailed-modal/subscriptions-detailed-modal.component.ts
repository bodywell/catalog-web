import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { ApiService } from 'src/app/core/service/api.service';
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-subscriptions-detailed-modal',
    templateUrl: './subscriptions-detailed-modal.component.html',
    styleUrls: ['./subscriptions-detailed-modal.component.scss']
})

export class SubscriptionsDetailedModalComponent implements OnInit {

    @Input() data: object = {};
    @Input() currency;

    // Событие закрытия модального окна для родительского компонента
    @Output() closeModalEvent = new EventEmitter<Boolean>();

    Object = Object;

    constructor(public translate: TranslateService) {}

    ngOnInit() {
        let time = this.data['time'].slice(0,-1);
        let translate = this.data['time'].slice(-1);
        translate = this.translate.instant(translate);
        this.data['time'] = time + ' ' + translate.toLowerCase();
    }

    closeModal() {
        this.closeModalEvent.emit(true);
    }
}
