import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {ApiService} from '../../core/service/api.service';
import {take} from 'rxjs/internal/operators';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-accordion2',
    templateUrl: './accordion2.component.html',
    styleUrls: ['./accordion2.component.scss'],
})
export class Accordion2Component implements OnInit, OnDestroy {
    @Input() widgetMode = false;
    @Input() currency = '';
    @Input() data: object[];
    @Input() isClasses: any = false;
    @Input() isPersonal: any = false;
    @Input() isOpen: boolean = false;
    @Input() isCompanyClient: boolean = false;

    @Output() selectItem = new EventEmitter<object>();
    @Output() showPassInfo = new EventEmitter<object>();
    @Output() lazyLoad = new EventEmitter<object>();

    apiUrl: string = environment.ApiUrl;

    private unsubscribe: Subject<any> = new Subject<any>();
    constructor(public apiService: ApiService) {}

    ngOnInit() {
    }

    toggle(e) {
        let element = e.target.parentElement.parentElement;
        if (!element.classList.contains('category-content')) {
            element = element.parentElement;
        }
        if (element.classList.contains('category-content')) {
            element.classList.toggle('open');
        }
    }

    select(item) {
        this.selectItem.emit(item);
    }

    viewPassInfo(item) {
        this.showPassInfo.emit(item);
    }

    onLazyLoad(item) {
        this.lazyLoad.emit(item);
    }

    ngOnDestroy() {
        this.unsubscribe.next(null);
        this.unsubscribe.complete();
    }
}
