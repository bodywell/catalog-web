import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { LocationService } from "../../core/service/location.service";
import {ApiService} from "../../core/service/api.service";
import {Router} from "@angular/router";
import {environment} from "../../../environments/environment";
import {ReplaySubject, Subject} from "rxjs/index";
import {take, takeUntil} from "rxjs/internal/operators";

@Component({
    selector: 'app-popular-content',
    templateUrl: './popular-content.component.html',
    styleUrls: ['./popular-content.component.scss'],
})
export class PopularContentComponent implements OnInit, OnDestroy {
    data = [];
    apiUrl = environment.ApiUrl;
    @Input() enableBackground = false;
    private unsubscribe: Subject<any> = new Subject<any>();

    constructor(public locationService: LocationService, public apiService: ApiService, private router: Router) {}

    ngOnInit() {
        this.locationService.isChanged.pipe(takeUntil(this.unsubscribe)).subscribe((val) => {
            this.getContent();
        });
    }

    search() {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([`/search-result`], {
            queryParams: {
                type: 0,
                search: '',
                industries: '',
                offset: 0,
                city: this.locationService.selectedLocation['city'],
                country: this.locationService.selectedLocation['country']
            }
        });
    }

    getContent() {
        this.apiService.getRecommendedContent(
            this.locationService.selectedLocation.city,
            this.locationService.selectedLocation.country
        ).pipe(takeUntil(this.unsubscribe)).subscribe((data) => {
            if (data['status'] === 'success') {
                this.data = data['data'];
            }
        });
    }

    ngOnDestroy() {
        this.unsubscribe.next(null);
        this.unsubscribe.complete();
    }
}
