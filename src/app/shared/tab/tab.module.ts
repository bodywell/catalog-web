import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabComponent } from './tab.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [TabComponent],
  imports: [
    CommonModule,
    TranslateModule
  ],
  exports: [
    TabComponent
  ]
})
export class TabModule { }
