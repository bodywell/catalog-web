import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  @Input() tabs;
  @Input() activeTabId;
  @Output() clickTabEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  clickTab(tab) {
    this.clickTabEvent.emit(tab['id']);
    this.activeTabId = tab['id'];
  }

}
