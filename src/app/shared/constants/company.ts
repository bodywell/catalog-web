export enum companyType {
  SERVICE = 0,
  GROUP = 1
}

export enum businessType {
  INDIVIDUAL = 1,
  SMALL_BUSINESS = 2,
  NETWORK = 3
}