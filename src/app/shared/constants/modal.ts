export enum ModalType {
  Auth = 'AUTH_MODAL',
  Registration = 'REGISTRATION_MODAL',
  Forgot = 'FORGOT_MODAL',
  ForgotFinish = 'FORGOT_FINISH_MODAL',
  SelectCity = 'SELECT_CITY_MODAL'
}