export enum classesType {
  Group = 0,
  Personal = 1,
}

export enum bookingStep {
  Type = 0,
  Service = 1,
  Group = 1,
  Employee = 2,
  Time = 3,
  
  Complete = 4
}