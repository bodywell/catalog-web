export enum productType {
    GOODS = 0,
    SERVICES = 1,
    SUBSCRIPTIONS = 2,
    CLASSES = 3,
    COURSE = 4
}
