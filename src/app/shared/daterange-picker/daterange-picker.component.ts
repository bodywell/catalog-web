import {
  Component,
  OnInit,
  Input,
  forwardRef,
  EventEmitter,
  Output,
  ContentChild,
  TemplateRef,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { NgFooterDirective } from './ng-templates.directive';
import * as moment from 'moment';

@Component({
  selector: 'app-daterange-picker',
  templateUrl: './daterange-picker.component.html',
  styleUrls: ['./daterange-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DaterangePickerComponent),
      multi: true,
    },
  ],
})
export class DaterangePickerComponent implements ControlValueAccessor, OnInit, OnChanges {
  public moment = moment;
  months = [];
  select_month = [];
  select_year = [];
  selected_month: number;
  selected_year: number;
  private current_range_selecting = 'date_from';

  currentMonth = moment().month();
  currentYear = moment().year();

  @Input() disabledDays: (day: Date) => boolean;
  @Input() disabled;
  @Input() defaultDate;
  @Input() numberOfMonths = 1;
  @Input() min_date;
  @Input() max_date: Date;
  @Input() date;
  @Input() range: 'day' | 'week' | 'custom' = 'day';
  @Input() range_type: string;
  @Input() date_range: { date_from?: moment.Moment; date_to?: moment.Moment } = {
    date_from: null,
    date_to: null,
  };

  @Input() header_type: 'date-left' | 'date-center' | 'select-controls';

  @Output() toggle: EventEmitter<any> = new EventEmitter();
  @Output() daySelect: EventEmitter<any> = new EventEmitter();
  @Output() modelChange: EventEmitter<any> = new EventEmitter();

  onChange: (value: moment.Moment) => void;
  onTouched: () => void;

  @ContentChild(NgFooterDirective, { read: TemplateRef, static: false }) footerTemplate: TemplateRef<any>;

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.date) {
      const { currentValue } = changes.date;

      if (currentValue instanceof Date) {
        this.date = moment(currentValue);
      }

      this.initComponent();
    }
    if (changes.range) {
      const { currentValue } = changes.range;

      switch (currentValue) {
        case 'day':
          this.date = moment();
          break;

        case 'week':
          this.date = {
            from_date: moment().startOf('week'),
            to_date: moment().endOf('week'),
          };
          break;

        case 'custom':
          this.date = {
            from_date: moment().startOf('week'),
            to_date: moment().endOf('week'),
          };
          break;

        case 'all':
          this.date = {
            from_date: moment(this.min_date).startOf('day') || null,
            to_date: moment(),
          };
          break;

        default:
          return;
      }
    }
  }

  ngOnInit() {
    this.initComponent();
  }

  private initComponent(): void {
    const date = new Date((this.date && this.date['from_date'] ? this.date['from_date'] : this.date) || this.defaultDate) || new Date();
    this.currentMonth = date.getMonth();
    this.currentYear = date.getFullYear();

    this.createMonths(this.currentMonth, this.currentYear);

    if (this.header_type === 'select-controls') {
      this.initMonthSelect();
      this.initYearSelect();

      this.selected_month = Number(moment(this.date || this.defaultDate).format('M')) - 1;
      this.selected_year = Number(moment(this.date || this.defaultDate).format('YYYY'));
    }
  }

  private initMonthSelect() {
    const months = [];
    for (let i = 0; i < 12; i++) {
      const month = {
        value: i,
        label: moment().month(i).format('MMMM'),
      };

      months.push(month);
    }

    this.select_month = months;
  }

  private initYearSelect() {
    const years = [];
    const current_year = this.max_date ? this.max_date.getFullYear() : new Date().getFullYear();
    
    const minYear = this.min_date ? this.min_date.getFullYear() - 1 : 1990;
    for (let i = current_year; i > minYear; i--) {
      const year = {
        value: i,
        label: i,
      };

      years.push(year);
    }

    this.select_year = years;
  }

  public monthSelected(value) {
    this.selected_month = value.value;

    this.createMonths(this.selected_month, this.selected_year);
  }

  public yearSelected(value) {
    this.selected_year = value.value;

    this.createMonths(this.selected_month, this.selected_year);
  }

  createMonths(month: number, year: number) {
    this.months = this.months = [];
    for (let i = 0; i < this.numberOfMonths; i++) {
      let m = month + i;
      let y = year;

      if (m > 11) {
        m = (m % 11) - 1;
        y = year + 1;
      }
      this.months.push(this.createMonth(m, y));
    }
  }

  createMonth(month: number, year: number) {
    const dates = [];
    const firstDay = this.getFirstDayOfMonthIndex(month, year);
    const daysLength = this.getDaysCountInMonth(month, year);
    const prevMonthDaysLength = this.getDaysCountInPrevMonth(month, year);
    let dayNo = 1;
    const today = new Date();
    const weekNumbers = [];
    const monthRows = Math.ceil((daysLength + firstDay) / 7);

    for (let i = 0; i < monthRows; i++) {
      const week = [];

      if (i === 0) {
        for (let j = prevMonthDaysLength - firstDay + 1; j <= prevMonthDaysLength; j++) {
          const prev = this.getPreviousMonthAndYear(month, year);

          week.push({
            day: j,
            month: prev.month,
            year: prev.year,
            otherMonth: true,
            today: this.isToday(today, j, prev.month, prev.year),
            selectable: this.disabledDays ? this.disabledDays(new Date(prev.year, prev.month, j)) : true,
          });
        }

        const remainingDaysLength = 7 - week.length;
        for (let j = 0; j < remainingDaysLength; j++) {
          week.push({
            day: dayNo,
            month: month,
            year: year,
            today: this.isToday(today, dayNo, month, year),
            selectable: this.disabledDays ? this.disabledDays(new Date(year, month, dayNo)) : true,
          });
          dayNo++;
        }
      } else {
        for (let j = 0; j < 7; j++) {
          if (dayNo > daysLength) {
            const next = this.getNextMonthAndYear(month, year);

            week.push({
              day: dayNo - daysLength,
              month: next.month,
              year: next.year,
              otherMonth: true,
              today: this.isToday(today, dayNo - daysLength, next.month, next.year),
              selectable: this.disabledDays
                ? this.disabledDays(new Date(next.year, next.month, dayNo - daysLength))
                : true,
            });
          } else {
            week.push({
              day: dayNo,
              month: month,
              year: year,
              today: this.isToday(today, dayNo, month, year),
              selectable: this.disabledDays ? this.disabledDays(new Date(year, month, dayNo)) : true,
            });
          }

          dayNo++;
        }
      }

      dates.push(week);
    }
    return {
      month: month,
      year: year,
      dates: dates,
      weekNumbers: weekNumbers,
    };
  }

  public prev(): void {
    if (this.currentMonth === 0) {
      this.currentMonth = 11;
      this.currentYear--;
    } else {
      this.currentMonth--;
    }

    this.createMonths(this.currentMonth, this.currentYear);
  }

  public next(): void {
    if (this.currentMonth === 11) {
      this.currentMonth = 0;
      this.currentYear++;
    } else {
      this.currentMonth++;
    }

    this.createMonths(this.currentMonth, this.currentYear);
  }

  public select(date): void {
    if (!Array.isArray(date) && !date.selectable) {
      return;
    }
    if (this.disabled) {
      return;
    }
    if (this.range === 'week' && !Array.isArray(date)) {
      return;
    } else if (this.range === 'day' && Array.isArray(date)) {
      return;
    } else if (this.range === 'custom' && Array.isArray(date)) {
      return;
    }

    let _date;
    switch (this.range) {
      case 'day':
        _date = moment({ year: date.year, month: date.month, day: date.day });

        this.date = _date;
        this.modelChange.emit(this.date);
        this.toggle.emit();
        break;
      case 'week':
        this.date = {
          from_date: moment({ year: date[0].year, month: date[0].month, day: date[0].day }),
          to_date: moment({ year: date[6].year, month: date[6].month, day: date[6].day }),
        };

        this.modelChange.emit(this.date);
        break;
      case 'custom':
        _date = moment({ year: date.year, month: date.month, day: date.day });

        // Если нет начальной даты
        if (!moment.isMoment(this.date)) {
          if (!this.date.from_date) {
            this.date.from_date = _date;
            this.current_range_selecting = 'date_to';
            // Если нет конечной даты
          } else if (!this.date.to_date) {
            this.date.to_date = _date;
            this.current_range_selecting = 'from_date';
            // Если есть обе даты и выбранная дата меньше начальной даты
          } else if (_date.diff(moment(this.date.from_date)) < 0) {
            this.date.from_date = _date;
            this.current_range_selecting = 'to_date';
            // Если есть обе даты и выбранная дата больше конечной даты
          } else if (_date.diff(moment(this.date.to_date)) > 0) {
            this.date.to_date = _date;
            this.current_range_selecting = 'from_date';
          } else if (this.current_range_selecting === 'from_date') {
            this.date.from_date = _date;
            this.current_range_selecting = 'to_date';
          } else if (this.current_range_selecting === 'to_date') {
            this.date.to_date = _date;
            this.current_range_selecting = 'from_date';
          }

          if (this.date.from_date) {
            this.modelChange.emit(this.date);
          }
        }
        break;
      default:
        return;
    }

    if (this.onChange) {
      this.onChange(_date);
      this.createMonths(this.currentMonth, this.currentYear);
    }
  }

  public isSelected(day: moment.Moment): boolean {
    if (this.range === 'day') {
      return false;
    }
    if (!day) {
      return false;
    }

    if (!moment.isMoment(this.date)) {
      const from_date = this.date.from_date;
      const to_date = this.date.to_date;

      if (from_date && to_date) {
        return from_date.isBefore(day) && to_date.isAfter(day);
      }
    }
  }

  public isStartSelected(day: moment.Moment): boolean {
    if (this.range === 'day') {
      return false;
    }
    if (!day) {
      return false;
    }

    if (!moment.isMoment(this.date)) {
      const from_date = moment(this.date.from_date);

      if (from_date) {
        return from_date.isSame(day);
      }
    }
  }

  public isEndSelected(day: moment.Moment): boolean {
    if (this.range === 'day') {
      return false;
    }
    if (!day) {
      return false;
    }

    if (!moment.isMoment(this.date)) {
      const to_date = moment(this.date.to_date);

      if (to_date) {
        return to_date.isSame(day);
      }
    }
  }

  public dateCheck(date): boolean {
    const { year, month, day } = date;
    const _date = moment({ year, month, day }).startOf('days');
    if (!_date) {
      return false;
    }

    if (this.range !== 'day' && !moment.isMoment(this.date)) {
      return this.date.from_date.isSame(_date) || this.date.to_date.startOf('days').isSame(_date);
    } else if (this.range === 'day' && moment.isMoment(this.date)) {
      return _date.isSame(moment(this.date).startOf('days'));
    }
  }

  getFirstDayOfMonthIndex(month: number, year: number): number {
    const day = new Date();
    day.setDate(1);
    day.setMonth(month);
    day.setFullYear(year);

    const dayIndex = day.getDay() + 7 - 1;

    return dayIndex >= 7 ? dayIndex - 7 : dayIndex;
  }

  private getDaysCountInMonth(month: number, year: number) {
    return 32 - this.daylightSavingAdjust(new Date(year, month, 32)).getDate();
  }

  private getDaysCountInPrevMonth(month: number, year: number) {
    const prev = this.getPreviousMonthAndYear(month, year);
    return this.getDaysCountInMonth(prev.month, prev.year);
  }

  getPreviousMonthAndYear(month: number, year: number): { [key: string]: number } {
    let m, y;

    if (month === 0) {
      m = 11;
      y = year - 1;
    } else {
      m = month - 1;
      y = year;
    }

    return { month: m, year: y };
  }

  getNextMonthAndYear(month: number, year: number): { [key: string]: number } {
    let m, y;

    if (month === 11) {
      m = 0;
      y = year + 1;
    } else {
      m = month + 1;
      y = year;
    }

    return { month: m, year: y };
  }

  daylightSavingAdjust(date) {
    if (!date) {
      return null;
    }

    date.setHours(date.getHours() > 12 ? date.getHours() + 2 : 0);

    return date;
  }

  isToday(today, day, month, year): boolean {
    return today.getDate() === day && today.getMonth() === month && today.getFullYear() === year;
  }

  // вызовет форма если значение изменилось извне
  writeValue(date): void {
    if (!date) {
      return;
    }

    if (date instanceof Date) {
      this.date = moment(date);
    } else {
      this.date = date;
    }
  }

  // сохраняем обратный вызов для изменений
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  // сохраняем обратный вызов для "касаний"
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
