import { Directive, TemplateRef } from '@angular/core';

@Directive({ selector: '[appNgFooterTmp]' })
export class NgFooterDirective {
  constructor(public template: TemplateRef<any>) {}
}
