import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotoSliderComponent } from './photo-slider.component';

@NgModule({
  declarations: [PhotoSliderComponent],
  imports: [
    CommonModule
  ],
  exports: [
    PhotoSliderComponent
  ]
})
export class PhotoSliderModule { }
