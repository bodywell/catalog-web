import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-photo-slider',
  templateUrl: './photo-slider.component.html',
  styleUrls: ['./photo-slider.component.scss']
})
export class PhotoSliderComponent implements OnInit {
  @Input() photos = [];
  @Input() smallMode;
  activePhoto = "";
  activeIndex = 0;
  width = 0;
  constructor() {
  }

  ngOnInit() {
    let constSize = 0;

    if (!this.photos || this.photos.length === 0) {
      this.photos = [{
        'path': '/assets/img/no-pic.png'
      }];
    }
    if (this.smallMode == true) {
      constSize = 472;
    } else {
      constSize = 722;
    }
    let h = setTimeout(() => {
      if (this.photos && this.photos.length > 0) {
        this.activePhoto = this.photos[0]['path'];
        this.width = (constSize - (24 * (this.photos.length - 1))) / this.photos.length;
        clearTimeout(h);
      }
    }, 100);
  }

  mouseenter(photo, activeIndex) {
    this.activePhoto = photo['path'];
    this.activeIndex = activeIndex;
  }

  next() {
    if (this.activeIndex < this.photos.length - 1) {
      this.activeIndex++;
      this.activePhoto = this.photos[this.activeIndex]['path'];
    }
  }

  back() {
    if (this.activeIndex > 0) {
      this.activeIndex--;
      this.activePhoto = this.photos[this.activeIndex]['path'];
    }
  }

}
