import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Params, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BookingService } from '../../core/service/booking.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss'],
})
export class BookingComponent implements OnInit, OnDestroy {
  public isLoaded = false;
  public companyId: number;
  public branchId: number;
  public company;

  private unsubscribe: Subject<any> = new Subject();

  constructor(private route: ActivatedRoute, private bookingService: BookingService) {}

  ngOnInit() {
    this.saveQueryParams(this.route.snapshot.queryParams);
    this.initBookingService(this.route.snapshot.queryParams);

    localStorage.removeItem('AFTER_PURCHASING_PRODUCT');

    this.bookingService.isLoaded.pipe(takeUntil(this.unsubscribe)).subscribe((status) => {
      this.isLoaded = status;

      if (status) {
        this.company = this.bookingService.companyInfo;
      }
    });

    this.route.queryParams.pipe(takeUntil(this.unsubscribe)).subscribe((params) => {
      this.bookingService.setQueryParams(params);
    });
  }

  private saveQueryParams(queryParams: Params): void {
    this.companyId = Number(queryParams['company_id']);
    this.branchId = Number(queryParams['branch_id']);
  }

  private initBookingService(queryParams: Params): void {
    this.bookingService.init({
      companyId: Number(queryParams['company_id']),
      branchId: Number(queryParams['branch_id']),
      productId: Number(queryParams['product_id']),
      employeeId: Number(queryParams['employee_id']),
      bookDate: queryParams['book_date'],
      type: Number(queryParams['type']),
      paymentSessionId: queryParams['session_id'],
    });
  }

  ngOnDestroy() {
    this.bookingService.isLoaded.next(false);

    this.unsubscribe.next(true);
    this.unsubscribe.complete();
  }
}
