import {Component, OnInit, Input, Output, EventEmitter, OnDestroy} from '@angular/core';
import {BookingService} from '../../../../core/service/booking.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-booking-filters',
    templateUrl: './booking-filters.component.html',
    styleUrls: ['./booking-filters.component.scss']
})
export class BookingFiltersComponent implements OnInit, OnDestroy {
    @Input() data: any;
    @Input() filters: any = ['search', 'employee', 'product'];
    @Output() result = new EventEmitter<any>();
    selectedEmployee;
    selectedProduct;

    private unsubscribe: Subject<any> = new Subject();
    constructor(public bookingService: BookingService, public route: ActivatedRoute) { }

    ngOnInit() {
        const { employee_id } = this.route.snapshot.queryParams;
        if (employee_id && Object.keys(this.data).includes('employees')) {
            const employee = this.data['employees'].find(x => x.key == Number(employee_id));
            this.data['selectedEmployee'] = employee['key'];
        }
        this.bookingService.isLoaded.pipe(takeUntil(this.unsubscribe)).subscribe((status) => {
            if (status) {
                this.doFilter();
            }
        });
    }

    doFilter() {
        const resultObject = {
            title: (Object.keys(this.data).includes('search')) ? this.data['search'] : '',
            employee: (Object.keys(this.data).includes('employees')) ? this.data['selectedEmployee'] : 0,
            product: (Object.keys(this.data).includes('products')) ? this.data['selectedProduct'] : 0,
        };

        this.selectedEmployee = resultObject.employee;
        this.selectedProduct = resultObject.product;
        this.result.emit(resultObject);
    }

    emitSearch() {
        this.doFilter();
    }

    emitEmployee(val) {
        if (val && val.hasOwnProperty('key') && val.hasOwnProperty('value')) {
            if (this.selectedEmployee != val['key']) {
                this.doFilter();
            }
        }
    }

    emitProduct(val) {
        if (val && val.hasOwnProperty('key') && val.hasOwnProperty('value')) {
            if (this.selectedProduct != val['key']) {
                this.doFilter();
            }
        }
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
