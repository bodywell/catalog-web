import {Component, OnDestroy, OnInit} from '@angular/core';
import {BookingService} from '../../../../core/service/booking.service';
import {takeUntil} from 'rxjs/internal/operators';
import {Subject} from 'rxjs';
import * as moment from 'moment';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../../../core/service/api.service';
import {productType} from '../../../../shared/constants/product';
import {take} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-booking-payment-method',
    templateUrl: './payment-method.component.html',
    styleUrls: ['./payment-method.component.scss'],
})
export class BookingPaymentMethodComponent implements OnInit, OnDestroy {
    productType = productType;
    public type;
    public isLoaded: boolean;
    public time;
    public product;
    public branch;
    public company;
    public employee;
    public activeSubscriptions: any = [];
    public ONETIME_PAYMENT = -1;
    public showPurchaseLink = false;
    userInfo: any = {};
    isSubmitted = false;
    Object = Object;
    private unsubscribe: Subject<any> = new Subject();
    constructor(
        private bookingService: BookingService,
        private route: ActivatedRoute,
        private router: Router,
        private apiService: ApiService
    ) {}

    ngOnInit() {
        this.bookingService.isLoaded.pipe(takeUntil(this.unsubscribe)).subscribe((status) => {
            if (status) {
                this.product = this.bookingService.selectedOptions.product;
                this.userInfo = this.apiService.user;
                this.branch = this.bookingService.companyInfo.currentBranch;
                this.company = this.bookingService.companyInfo;
                this.employee = this.bookingService.selectedOptions.employee;
                this.time = this.bookingService.queryParams.bookDate;



                this.type = this.determineType();

                const foundedActiveSub = this.findActiveSubForPrepayment();
                if (foundedActiveSub) {
                    this.getActiveSubscriptions();
                } else {
                    if (this.type == productType.COURSE) {
                        const oneTimePaymentItem = this.generateOneTimePayment();
                        this.activeSubscriptions.unshift(oneTimePaymentItem);
                        this.showPurchaseLink = true;
                    } else {
                        const queryParams = { ...this.route.snapshot.queryParams };
                        const redirectLink = '/booking/confirm';
                        const afterPurchasingProduct = {
                            action: 'redirect',
                            pathname: redirectLink,
                            queryParams: queryParams,
                        };
                        window.localStorage.setItem('AFTER_PURCHASING_PRODUCT', JSON.stringify(afterPurchasingProduct));
                        this.router.navigate(['/purchase/products'], {
                            queryParams: {
                                company_id: this.company['id'],
                                branch_id: this.branch['id'],
                                target_id: this.product['id'],
                                type: this.productType.SUBSCRIPTIONS,
                            },
                            relativeTo: this.route
                        });
                    }
                }

                this.isLoaded = status;
            }
        });
    }

    determineType() {
        return 'subscriptions' in this.product ? productType.SUBSCRIPTIONS :
               'relations' in this.product ? productType.COURSE :
                null;
    }

    getActiveSubscriptions() {
        const where = {
            is_active: 1,
            branch_id: this.branch['id'],
        };
        this.apiService.getSubscription(where).pipe(take(1)).subscribe((response) => {
           if (response['status'] == 'success') {
               if (response['data'].length) {
                   this.activeSubscriptions = response['data'].filter(sub => {
                       let found = null;
                       if (this.type == productType.SUBSCRIPTIONS) {
                           found = this.product['subscriptions'].find(x => sub.product_id == x.id);
                       } else if (this.type == productType.COURSE) {
                           found =  this.product['relations'].find(x => sub.product_id == x);
                       }

                       if (found) {
                           sub.description = found.description;
                           return true;
                       }

                       return false;
                   });
               }

               if (this.type == productType.COURSE) {
                   const oneTimePaymentItem = this.generateOneTimePayment();
                   this.activeSubscriptions.unshift(oneTimePaymentItem);
               }
           }
        });
    }
    generateOneTimePayment() {
        return {
            title: 'prepayment-one-time-payment-title',
            description: 'prepayment-one-time-payment-desc',
            currency: this.company['currency'],
            price: this.product['price'],
            sub_id: this.ONETIME_PAYMENT,
            is_onetime_payment: true
        };
    }

    findActiveSubForPrepayment() {
        if (this.type == productType.SUBSCRIPTIONS) {
            console.log(this.userInfo['client_products']);
            return this.product['subscriptions'].find(subscription => this.userInfo['client_products'].includes(subscription.id));
        } else if (this.type == productType.COURSE) {
            return this.product['relations'].find(relation => this.userInfo['client_products'].includes(relation));
        }
    }

    changeStep(type) {
        this.router.navigate([`../${type}`], {
            queryParamsHandling: 'merge',
            relativeTo: this.route,
        });
    }

    selectItem(item) {
        if (!('sub_id' in item) || this.isSubmitted) {
            return;
        }

        this.isSubmitted = true;
        const sub_id = item['sub_id'];

        if (sub_id != this.ONETIME_PAYMENT) {

            this.router.navigate(['../confirm'], {
                queryParams: {
                    sub: sub_id
                },
                queryParamsHandling: 'merge',
                relativeTo: this.route
            });
        } else {
            const amount = !this.product['price'] ? this.product['subscription_price'] : this.product['price'];
            this.bookingService.createInvoiceAndRedirectToCheckout(
                this.getCallbackUrl('booking/confirm'),
                this.company['id'],
                this.branch['id'],
                this.product['id'],
                this.employee['id'],
                moment(this.time).format('YYYY-MM-DD HH:mm'),
                amount,
                this.company['currency'],
                this.company['stripe_account']
                );
        }
    }

    getCallbackUrl(route) {
        let callbackUrl = '';
        this.route.queryParams.subscribe(params => {
            const queryParams = { ...params };
            const domain = window.location.origin;
            callbackUrl = `${domain}/${route}?${this.serializeQueryParams(queryParams)}`;
        });

        return callbackUrl;
    }

    serializeQueryParams(params) {
        return Object.keys(params)
            .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
            .join('&');
    }

    ngOnDestroy() {
        this.unsubscribe.next(true);
        this.unsubscribe.complete();
    }
}
