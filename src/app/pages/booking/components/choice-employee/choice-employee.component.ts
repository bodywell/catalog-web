import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BookingService } from '../../../../core/service/booking.service';
import { environment } from '../../../../../environments/environment';
import moment from 'moment';
import { companyType } from 'src/app/shared/constants/company';

@Component({
  selector: 'app-choice-employee',
  templateUrl: './choice-employee.component.html',
  styleUrls: ['./choice-employee.component.scss'],
})
export class ChoiceEmployeeComponent implements OnInit, OnDestroy {
  public apiUrl = environment.ApiUrl;
  public employees = [];
  public selectedEmployee = null;
  public allAvailableTimes = [];
  public generalBookings = [];
  public generalNearestDate: string;
  public isService: boolean;
  public isPersonalProduct: boolean = false;

  private unsubscribe: Subject<any> = new Subject();

  constructor(private bookingService: BookingService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.bookingService.isLoaded.pipe(takeUntil(this.unsubscribe)).subscribe((status) => {
      if (status) {
        this.isService = this.bookingService.companyInfo.session === companyType.SERVICE;
        this.employees = this.bookingService.employees;

        if (this.bookingService.queryParams.productId) {
          this.employees = this.bookingService.employees
            .filter((employee) =>
              employee.products.some((product) => product.id === this.bookingService.queryParams.productId)
            )
            .map((employee) => {
              return {
                ...employee,
                bookings: employee.bookings.slice(0, window.innerWidth > 768 ? 5 : 3),
                timeLabel:
                  employee.bookings.length &&
                  moment(employee.bookings[0].from_time).startOf('day').diff(moment().startOf('day'), 'days') === 0
                    ? 'today'
                    : moment(employee.bookings[0].from_time).startOf('day').diff(moment().startOf('day'), 'days') === 1
                    ? 'tomorrow'
                    : null,
              };
            });

          this.generalBookings = this.employees
            .reduce((acc, employee) => {
              return acc.concat(employee.bookings);
            }, [])
            .reduce((acc, book) => {
              if (!acc.some((b) => book.from_time === b.from_time)) {
                acc.push(book);
              }

              return acc;
            }, [])
            .sort((bookA, bookB) => {
              return moment(bookA.from_time).diff(moment(bookB.from_time));
            })
            .slice(0, window.innerWidth > 768 ? 5 : 3)
            .filter((booking, idx, arr) => {
              const nearestDate = moment(arr[0].from_time).format('YYYY-MM-DD');

              return moment(booking.from_time).format('YYYY-MM-DD') === nearestDate;
            })
            .map((book) => {
              const employees = [];

              this.employees.forEach((employee) => {
                if (employee.bookings.some((b) => b.from_time === book.from_time)) {
                  employees.push({
                    first_name: employee.first_name,
                    last_name: employee.last_name,
                    id: employee.id,
                  });
                }
              });

              return {
                ...book,
                employees,
              };
            });

          this.generalNearestDate =
            this.generalBookings.length &&
            moment(this.generalBookings[0].from_time).startOf('day').diff(moment().startOf('day'), 'days') === 0
              ? 'today'
              : moment(this.generalBookings[0].from_time).startOf('day').diff(moment().startOf('day'), 'days') === 1
              ? 'tomorrow'
              : null;
        }

        this.selectedEmployee = this.bookingService.selectedOptions['employee'];

        if (!this.isService && this.employees.length) {
          this.employees.find(employee => {
            return employee.products.some(product => {
              if (product.id === this.bookingService.queryParams.productId) {
                if (product.is_personal) {
                  this.isPersonalProduct = true;
                  return true;
                }
              }
            });
          });
        }
      }
    });
    window.scrollTo(0,0);
  }

  selectedTime(time, employees = [], event) {
    if (event) {
      event.stopPropagation();
    }

    const randomEmployeeIdx = Math.floor(Math.random() * time['employees'].length);

    this.bookingService.setEmployee(employees[randomEmployeeIdx]);
    this.bookingService
      .setTime({
        time,
        employee: employees[randomEmployeeIdx],
        product:
          this.bookingService.companyInfo['session'] === 0 ||
          (this.bookingService.queryParams.productId && this.bookingService.queryParams.type)
            ? time.product.id
            : time.productId,
      })
      .subscribe({
        next: () => {
          this.router.navigate(['../confirm'], {
            queryParams: {
              book_date: moment(time['from_time']).format('YYYY-MM-DD HH:mm:ss'),
              employee_id: employees[randomEmployeeIdx]['id'],
            },
            relativeTo: this.route,
            queryParamsHandling: 'merge',
          });
        },
      });
  }

  public nextStep(employee = null): void {
    this.bookingService.setEmployee(employee);

    this.router.navigate(['../time'], {
      queryParams: {
        employee_id: employee ? employee.id : null,
      },
      relativeTo: this.route,
      queryParamsHandling: 'merge',
    });
  }

  public prevStep(): void {
    this.router.navigate(['../service'], {
      relativeTo: this.route,
      queryParamsHandling: 'merge',
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
