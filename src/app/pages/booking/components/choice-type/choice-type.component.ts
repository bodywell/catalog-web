import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BookingService } from '../../../../core/service/booking.service';
import {isEmpty} from 'lodash';

@Component({
  selector: 'app-choice-type',
  templateUrl: './choice-type.component.html',
  styleUrls: ['./choice-type.component.scss'],
})
export class ChoiceTypeComponent implements OnInit, OnDestroy {
  hasPersonalGroups: boolean;
  companyTitle;
  isWidget: boolean = false;

  private unsubscribe: Subject<any> = new Subject();

  constructor(private bookingService: BookingService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.bookingService.isLoaded.pipe(takeUntil(this.unsubscribe)).subscribe(status => {
      if (status) {
        this.isWidget = this.bookingService.isWidget;
        this.companyTitle = this.bookingService.companyInfo['title'];
        this.hasPersonalGroups = !isEmpty(this.bookingService.personalGroups);
      }
    })
  }

  public nextStep(type: 0 | 1): void {
    this.bookingService.setClassType(type);

    this.router.navigate([`../${type === 0 ? 'time' : 'service'}`], {
      relativeTo: this.route,
      queryParams: {
        type: type,
        product_id: null,
        employee_id: null
      },
      queryParamsHandling: 'merge'
    })
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
