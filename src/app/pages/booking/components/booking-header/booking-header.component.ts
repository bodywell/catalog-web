import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';


@Component({
  selector: 'app-booking-header',
  templateUrl: './booking-header.component.html',
  styleUrls: ['./booking-header.component.scss']
})
export class BookingHeaderComponent implements OnInit {

  @Input() title;
  @Input() industry;
  @Input() countReview;
  @Input() rating;
  @Input() companyId;
  @Input() branchId;
  @Input() companyLogo;
  apiUrl: string = environment.ApiUrl;

  constructor() { }

  ngOnInit() {
  }

}
