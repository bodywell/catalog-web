import { Component, OnInit, EventEmitter, Output, Input, OnDestroy } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { ApiService } from 'src/app/core/service/api.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { ActivatedRoute, Router } from '@angular/router';
import { take, takeUntil } from 'rxjs/operators';
import { BookingService } from '../../../../core/service/booking.service';
import { Subject } from 'rxjs';
import { classesType } from 'src/app/shared/constants/booking';
import { ApiResponseError, ApiResponseSuccess } from 'src/app/shared/interfaces/api-response.model';
import {
  BookingEmptyServiceTime,
  BookingServiceTimeEmployees,
  BookingTime,
} from 'src/app/shared/interfaces/booking.model';
import { businessType, companyType } from 'src/app/shared/constants/company';
import {TranslateService} from '@ngx-translate/core';
import {CompanyService} from '../../../../core/service/company.service';

@Component({
  selector: 'app-choice-time',
  templateUrl: './choice-time.component.html',
  styleUrls: ['./choice-time.component.scss'],
})
export class ChoiceTimeComponent implements OnInit, OnDestroy {
  apiUrl: string = environment.ApiUrl;
  public productId: number;
  public branchId: number;
  public companyInfo = {};
  public session: number;
  public currency: string;
  public isIndividualBusiness: boolean;
  public isPersonal: boolean;
  public isService: boolean;
  public isGroup: boolean;
  public subscriptions = [];

  public employes: object[];
  public selectedEmployee: object = null;
  public selectedDate: Date = new Date();

  public isTimesLoading = true;
  public times: object[] = [];
  public morningTime = [];
  public dinnerTime = [];
  public eveningTime = [];

  public isShowSubModal = false;
  public subModalProductData;
  public bookingFilterData = {
    search: '',
    employees: [],
    products: [],
    selectedEmployee: 0,
    selectedProduct: 0,
  };

  currentFiltersValue;
  isFiltered: boolean = false;
  isWidget: boolean = false;
  companyTitle;
  allGroups = [];

  groups = [];
  personalGroups = [];
  eventsNextDate = null;
  selectDateInterval;

  @Output() selectProduct = new EventEmitter<object>();

  private unsubscribe: Subject<any> = new Subject();

  constructor(
    private apiService: ApiService,
    private bookingService: BookingService,
    private companyService: CompanyService,
    private router: Router,
    private route: ActivatedRoute,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.bookingService.isLoaded.pipe(takeUntil(this.unsubscribe)).subscribe((status) => {
      if (status) {
        this.productId = this.route.snapshot.queryParams.product_id;
        this.session = this.bookingService.companyInfo['session'];
        this.isIndividualBusiness = this.bookingService.companyInfo.type === businessType.INDIVIDUAL;
        this.isPersonal = this.bookingService.queryParams.type === classesType.Personal;
        this.isService = this.bookingService.companyInfo['session'] === companyType.SERVICE;
        this.isGroup = this.bookingService.companyInfo['session'] === companyType.GROUP;
        this.companyInfo = this.bookingService.companyInfo;
        this.isWidget = this.bookingService.isWidget;
        this.companyTitle = this.bookingService.companyInfo['title'];

        if ('subscriptions' in this.bookingService.companyInfo.currentBranch) {
          this.subscriptions = Object.values(this.bookingService.companyInfo.currentBranch['subscriptions']);
        }

        if (this.isGroup && !this.isPersonal) {
          this.bookingService.setEmployee(null);
        }
        this.selectedEmployee = this.bookingService.employees.find(
          (x) => x['id'] == Number(this.bookingService.queryParams.employeeId)
        );

        this.initBookingFilteredData();
        this.setSelectDateInterval(new Date());
      }
    });
  }

  filterResult(filter) {
    this.currentFiltersValue = filter;
    this.isFiltered = this.bookingService.isFiltered(filter);
    this.groups = this.bookingService.doFilterBookings(filter, this.allGroups, this.isGroup);
  }

  initBookingFilteredData() {
    this.bookingFilterData.employees.push({ key: 0, value: 'any-employee' });
    this.bookingService.employees.forEach((employee) => {
      this.bookingFilterData.employees.push({ key: employee.id, value: employee.first_name + ' ' + employee.last_name });
    });

    let products = this.companyService.getSubscriptions(this.bookingService.queryParams.branchId);
    products = products.map(item => ({ key: item['id'], value: item['title'] }));
    products.unshift({
      key: 0,
      value: (this.isGroup) ? ( this.translate.instant('any-sub')) : this.translate.instant('any-course')
    });
    this.bookingFilterData.products = products;
  }

  next() {}

  goToEmployeeSelect() {
    this.router.navigate(['../employee'], {
      queryParams: this.route.snapshot.queryParams,
      relativeTo: this.route,
    });
  }

  setSelectDateInterval(date) {
    if (this.selectDateInterval) {
      clearInterval(this.selectDateInterval);
    }

    this.selectDate(date);

    this.selectDateInterval = setInterval(() => {
      this.selectDate(date);
    }, 60000);
  }

  selectDate(date) {
    this.selectedDate = date;
    this.isTimesLoading = true;

    const startDate = moment(date).format('YYYY-MM-DD HH:mm:ss'),
      endDate = moment(date).endOf('day').format('YYYY-MM-DD HH:mm:ss'),
      isService = this.bookingService.companyInfo['session'] === 0,
      isGroup = this.bookingService.companyInfo['session'] === 1,
      isPersonalGroup = this.productId && this.bookingService.queryParams.type === 1;

    this.apiService
      .getBookingTime(
        isGroup && !isPersonalGroup ? 0 : this.bookingService.queryParams.productId || 0,
        this.bookingService.queryParams.branchId,
        isGroup && !isPersonalGroup ? 0 : this.bookingService.queryParams.employeeId || 0,
        startDate,
        endDate
      )
      .pipe(take(1))
      .subscribe((res: ApiResponseSuccess<BookingTime[]> | ApiResponseError) => {
        if (res['status'] === 'success') {
          if (isService || isPersonalGroup) {
            this.parseServiceBookingTimes(res.data);
          } else if (isGroup) {
            if (!Array.isArray(res.data)) {
              this.eventsNextDate = res.data;
              this.allGroups = [];
              this.groups = this.allGroups;
            } else {
              this.allGroups = this.bookingService.parseGroupsForPrepaymentValue(
                  res.data,
                  this.subscriptions,
                  this.companyInfo['prepayment_value']
              );
              this.filterResult(this.currentFiltersValue);
            }
          }
        } else {
          this.times = [];
        }

        this.isTimesLoading = false;
      });
  }

  public getTime(date): string {
    return moment(date).format('HH:mm');
  }

  public selectedTime(time, isClass = false) {
    const productId = (isClass) ? time['product_id'] : time['product']['id'];
    let employee = {};

    if (isClass) {
      employee = {
        id: time['employee_id'],
        employee_first_name: time['employee_first_name'],
        employee_last_name: time['employee_last_name']
      };
    } else {
      employee = time['employees'][0];
    }

    this.bookingService.setProduct(productId);
    this.bookingService.setEmployee({ id: employee['id'] });

    //employee: this.selectedEmployee || (time['employees'] && time['employees'][0]) || time,
    this.bookingService
      .setTime({
        time,
        employee: employee,
        product: productId,
      })
      .subscribe({
        next: () => {
          this.router.navigate(['../confirm'], {
            queryParams: {
              product_id: productId,
              book_date: moment(time['from_date'] || time['from_time']).format('YYYY-MM-DD HH:mm:ss'),
              employee_id: employee['id'],
            },
            relativeTo: this.route,
            queryParamsHandling: 'merge',
          });
        },
      });
  }

  private parseGroups(groups) {
    if (!Array.isArray(groups)) {
      return [];
    }

    return groups.reduce((acc, group) => {
      if (group['product']['type'] === 3) {
        const hours = moment.duration(group['product']['duration'], 'minutes').hours();
        const minutes = moment.duration(group['product']['duration'] - hours * 60, 'minutes').minutes();

        const products = group.employees.map((employee) => {
          let desc;

          try {
            desc = group['product']['description']
              .split('\n')
              .map((str) => {
                // @ts-ignore
                return utf8.decode(str);
              })
              .join('\n');
          } catch (error) {}

          if (desc) {
            group['product']['description'] = desc;
          }

          return {
            ...employee,
            category_title: group['product']['category_title'],
            productId: group['product']['id'],
            employeeId: employee['id'],
            employeeName: employee['first_name'] + ' ' + employee['last_name'],
            fromDate: group['from_time'],
            toDate: group['to_time'],
            price: group['product']['price'],
            price_max: group['product']['price_max'],
            title: group['product']['title'],
            duration: group['product']['duration'],
            description: group['product']['description'],
            h: hours,
            m: minutes,
            clients: group['product']['clients'],
            limit: group['product']['limit'],
          };
        });

        const categoryIndex = acc.findIndex((i) => i.category === group['product']['category_title']);
        if (categoryIndex === -1) {
          acc.push({
            category: group['product']['category_title'],
            items: products,
            open: false,
          });
        } else {
          acc[categoryIndex].items = acc[categoryIndex].items.concat(products);
        }
      }

      return acc;
    }, []);
  }

  public prevStep(): void {
    const isIndividual = this.bookingService['companyInfo']['type'] === 1;

    if (this.isGroup && !this.isPersonal) {
      this.router.navigate([`../type`], {
        queryParamsHandling: 'merge',
        relativeTo: this.route,
      });
    } else {
      this.router.navigate([`../${isIndividual ? 'service' : 'employee'}`], {
        queryParamsHandling: 'merge',
        relativeTo: this.route,
      });
    }
  }

  private parseServiceBookingTimes(times: BookingTime[] | BookingEmptyServiceTime): void {
    this.eventsNextDate = null;
    this.morningTime = [];
    this.dinnerTime = [];
    this.eveningTime = [];

    if (!Array.isArray(times)) {
      this.eventsNextDate = {
        ...times,
        times: times.times.slice(0, window.innerWidth > 768 ? 5 : 3),
      };
    } else {
      times
        .filter((element) => {
          return moment(element.from_time).diff(moment(), 'hours') >= 2;
        })
        .forEach((element) => {
          if (element['product']['id'] != this.productId) {
            return;
          }

          if (moment(element['from_time']).hour() < 12) {
            this.morningTime.push(element);
          } else if (moment(element['from_time']).hour() >= 12 && moment(element['from_time']).hour() < 17) {
            this.dinnerTime.push(element);
          } else {
            this.eveningTime.push(element);
          }
        });
    }
  }

  private parseGroupBookingTimes(times): void {
    this.times = times;
    this.groups = [];
    this.personalGroups = [];

    if (!Array.isArray(times)) {
      this.eventsNextDate = times;
    }

    for (let i = 0; i < this.times.length; i++) {
      if (this.times[i]['product']['type'] === 3) {
        for (let j = 0; j < this.times[i]['employees'].length; j++) {
          const hours = moment.duration(this.times[i]['product']['duration'], 'minutes').hours();
          const minutes = moment.duration(this.times[i]['product']['duration'] - hours * 60, 'minutes').minutes();

          if (this.times[i]['product']['limit'] == 1) {
            this.personalGroups.push({
              productId: this.times[i]['product']['id'],
              employeeId: this.times[i]['employees'][j]['id'],
              employeeName:
                this.times[i]['employees'][j]['first_name'] + ' ' + this.times[i]['employees'][j]['last_name'],
              fromDate: this.times[i]['from_time'],
              toDate: this.times[i]['to_time'],
              price: this.times[i]['product']['price'],
              price_max: this.times[i]['product']['price_max'],
              title: this.times[i]['product']['title'],
              category_title: this.times[i]['product']['category_title'],
              h: hours,
              m: minutes,
              clients: this.times[i]['product']['clients'],
              limit: this.times[i]['product']['limit'],
            });
          } else {
            this.groups.push({
              productId: this.times[i]['product']['id'],
              employeeId: this.times[i]['employees'][j]['id'],
              employeeName:
                this.times[i]['employees'][j]['first_name'] + ' ' + this.times[i]['employees'][j]['last_name'],
              fromDate: this.times[i]['from_time'],
              toDate: this.times[i]['to_time'],
              price: this.times[i]['product']['price'],
              price_max: this.times[i]['product']['price_max'],
              title: this.times[i]['product']['title'],
              category_title: this.times[i]['product']['category_title'],
              h: hours,
              m: minutes,
              clients: this.times[i]['product']['clients'],
              limit: this.times[i]['product']['limit'],
            });
          }
        }
      }
    }
  }

  public get nextDate(): string {
    return moment(this.eventsNextDate.next_date).format('MMMM DD, YYYY');
  }

  ngOnDestroy() {
    if (this.selectDateInterval) {
      clearInterval(this.selectDateInterval);
    }

    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
