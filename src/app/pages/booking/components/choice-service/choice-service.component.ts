import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { BookingService } from '../../../../core/service/booking.service';
import {ApiService} from '../../../../core/service/api.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-choice-service',
  templateUrl: './choice-service.component.html',
  styleUrls: ['./choice-service.component.scss'],
})
export class ChoiceServiceComponent implements OnInit, OnDestroy {
  public currency: string;
  public services: object[];
  public allServices: any = [];
  public companyType: number;
  public branchId: number;
  isFiltered: boolean = false;
  isWidget: boolean = false;
  companyTitle;
  Object = Object;
  public bookingFilterData = {
    search: '',
    employees: [],
    products: [],
    selectedEmployee: 0,
    selectedProduct: 0,
  };

  private unsubscribe: Subject<any> = new Subject();

  constructor(
      private bookingService: BookingService,
      private router: Router,
      private route: ActivatedRoute,
      public apiService: ApiService,
      public translate: TranslateService) {}

  ngOnInit() {
    this.bookingService.isLoaded.pipe(takeUntil(this.unsubscribe)).subscribe((status) => {
      if (status) {
        this.isWidget = this.bookingService.isWidget;
        this.services =
          this.bookingService.queryParams.type === 1
            ? this.bookingService.personalGroups
            : this.bookingService.services;
        this.branchId = this.bookingService.queryParams.branchId;
        this.currency = this.bookingService.companyInfo['currency'];
        this.companyType = this.bookingService.companyInfo['session'];
        this.allServices = this.services;
        this.companyTitle = this.bookingService.companyInfo['title'];

        this.initBookingFilteredData();
      }
    });
  }

  next(item) {
    const isIndividual = this.bookingService.companyInfo['type'] === 1;
    this.bookingService.setProduct(item.product_id);

    if (isIndividual) {
      this.bookingService.setEmployee();
      this.router.navigate(['../time'], {
        queryParams: {
          product_id: item.id,
          employee_id: this.bookingService.companyInfo['employee_id'],
        },
        relativeTo: this.route,
        queryParamsHandling: 'merge',
      });
    } else {
      this.bookingService
        .getEmployees({ productId: item.id })
        .pipe(take(1))
        .subscribe(() => {
          this.router.navigate(['../employee'], {
            queryParams: {
              product_id: item.id,
              employee_id: null,
            },
            relativeTo: this.route,
            queryParamsHandling: 'merge',
          });
        });
    }
  }

  initBookingFilteredData() {
    const emptyString = this.translate.instant('any-employee');
    this.bookingFilterData.employees.push({ key: 0, value: emptyString });
    this.bookingService.employees.forEach((employee) => {
      this.bookingFilterData.employees.push({ key: employee.id, value: employee.first_name + ' ' + employee.last_name });
    });
  }

  filterResult(filter) {
    this.isFiltered = this.bookingService.isFiltered(filter);
    this.services =  this.bookingService.doFilterBookings(filter, this.allServices, false);
  }

  public prevStep(): void {
    this.router.navigate(['../type'], {
      relativeTo: this.route,
      queryParamsHandling: 'merge',
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
