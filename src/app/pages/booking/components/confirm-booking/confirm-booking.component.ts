import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy,
  ViewChild,
  NgZone,
  ElementRef,
} from '@angular/core';
import { environment } from '../../../../../environments/environment';
import * as moment from 'moment';
import { ApiService } from 'src/app/core/service/api.service';
import { NotificationMessageService } from '../../../../core/service/notification.service';
import { TranslateService } from '@ngx-translate/core';
import { AppAuthService } from '../../../../core/service/auth.service';
import { GapiCalendarService } from '../../../../core/service/gapi.calendar.service';
import { filter, switchMap, take, takeUntil } from 'rxjs/internal/operators';
import { StripeScriptTag } from 'stripe-angular';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { of, Subject, timer } from 'rxjs';
import { Location } from '@angular/common';
import { HelperService } from 'src/app/core/service/helper.service';
import { BookingService } from '../../../../core/service/booking.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-confirm-booking',
  templateUrl: './confirm-booking.component.html',
  styleUrls: ['./confirm-booking.component.scss'],
})
export class ConfirmBookingComponent implements OnInit, OnDestroy {
  public isLoaded: boolean;
  apiUrl: string = environment.ApiUrl;
  @Output() nextStep = new EventEmitter<Boolean>();
  @Input() type;
  public time;
  public product;
  public employee;
  public company;
  public branch;
  public sub;
  public payment_processing = false;
  public signErrorText = '';
  public isGroup: boolean;

  displayDate: string;
  comment = '';
  isSyncWithCalendar = false;
  isSubmitted = false;
  session_id: string;
  private fromDate;
  userInfo: any = {};
  address;
  selectedAddress;
  latitude;
  longitude;
  geoCoder;
  zoom = 2;

  isClient: boolean = false;
  needPrepayment: boolean = false;
  prepayment_value: any;

  @ViewChild('search', { static: false }) public searchElementRef: ElementRef;
  @ViewChild('locationModal', { static: false }) locationModal: ModalComponent;
  @ViewChild('btn_sign', { static: false }) btnSign: ElementRef;

  private unsubscribe: Subject<any> = new Subject();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    public notification: NotificationMessageService,
    public translate: TranslateService,
    public authService: AppAuthService,
    public calendar: GapiCalendarService,
    public stripe: StripeScriptTag,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    public helperService: HelperService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private bookingService: BookingService
  ) {}

  ngOnInit() {
    this.bookingService.isLoaded.pipe(takeUntil(this.unsubscribe)).subscribe((status) => {
      if (status) {

        this.employee = this.bookingService.selectedOptions.employee;
        console.log(this.employee);
        this.product = this.bookingService.selectedOptions.product;
        this.time = this.bookingService.queryParams.bookDate;
        this.company = this.bookingService.companyInfo;
        this.branch = this.bookingService.companyInfo.currentBranch;
        this.displayDate = moment(this.time).format('YYYY-MM-DD');
        this.userInfo = this.apiService.user;
        this.address = this.userInfo['address'];
        this.latitude = this.userInfo['latitude'];
        this.longitude = this.userInfo['longitude'];

        this.loadPlacesAutocomplete();

        this.activatedRoute.queryParams.pipe(takeUntil(this.unsubscribe)).subscribe((params) => {
          if (params['session_id']) {
            this.session_id = params['session_id'];
            this.payment_processing = true;
            this.isSubmitted = true;
          }

          if (params['book_date']) {
            this.fromDate = params['book_date'];
          }

          if (params['action'] && params['action'] === 'AFTER_LOGIN_BOOKING' && this.userInfo['phone_number']) {
            window.localStorage.removeItem('AFTER_LOGIN_BOOKING');
            this.booking();
          }

          if (params['sub']) {
            if (this.checkSelectedSub(Number(params['sub']))) {
              this.sub = params['sub'];
              this.payment_processing = true;
            } else {
              this.notification.pushAlert({ detail: this.translate.instant('booking-prepayment-error-selected-sub') });
              this.delParamsFromUrl(['sub']);
            }
          }
        });

        this.calendar.syncEmail.pipe(take(1)).subscribe((email) => {
          if (email) {
            this.isSyncWithCalendar = true;
          }
        });

        if (this.company['online_payment'] == 1) {
          this.prepayment_value = (this.product['prepayment_value'] == 'default' || this.product['prepayment_value'] == null) ? this.company['prepayment_value'] : this.product['prepayment_value'];
          this.isClient = (this.prepayment_value == 1) ? this.userInfo['relation_branches'].includes(this.branch['id']) : false;
          this.needPrepayment = (this.prepayment_value == 2 || (this.prepayment_value == 1 && !this.isClient));

          if (this.session_id && this.isSubmitted) {
            this.checkSession();
          }
        }

        this.authService.finishRegEvent.pipe(takeUntil(this.unsubscribe)).subscribe(() => {
          this.booking();
        });

        // Декодирование комментария, страховка от утери данных после перезагрузки страницы на этапе выбора абонемента
        let booking_data: any = localStorage.getItem('b_data');
        if (booking_data) {
          booking_data = JSON.parse(booking_data);
          if ('comment' in booking_data) {
            const decodedBytes = new Uint8Array(atob(booking_data['comment']).split('').map(char => char.charCodeAt(0)));
            this.comment = new TextDecoder().decode(decodedBytes);
          }

          delete booking_data['comment'];
          if (!Object.keys(booking_data).length) {
            localStorage.removeItem('b_data');
          } else {
            localStorage.setItem('b_data', JSON.stringify(booking_data));
          }
        }

        this.isLoaded = status;
        this.waitBtnSignInit();

        if (this.sub && this.payment_processing) {
          setTimeout(() => {
            this.doSign(
                this.product['id'],
                this.employee['id'],
                moment(this.time).format('YYYY-MM-DD HH:mm'),
                this.comment
            );
          }, 1500);
        }
      }
    });
  }

  next() {
    this.nextStep.emit(true);
  }

  changeStep(type) {
    if (this.payment_processing && !this.signErrorText) {
      return;
    }
    if (type === 'time' && this.company['session'] === 1 && this.bookingService.queryParams.type === 0) {
      this.router.navigate(['../time'], {
        queryParams: {
          employee_id: null,
          product_id: null,
          sub: null,
        },
        queryParamsHandling: 'merge',
        relativeTo: this.route
      });

      return;
    }
    this.router.navigate([`../${type}`], {
      queryParamsHandling: 'merge',
      queryParams: {
        sub: null,
      },
      relativeTo: this.route,
    });
  }

  checkSelectedSub(sub_id) {
    if (!sub_id) {
      return false;
    }

    const where = {
      is_active: 1,
      sub_id: sub_id,
    };

    return this.apiService.getSubscription(where).pipe(
        take(1),
        switchMap(response => {
          if (response['status'] !== 'success' || response['data'].length === 0) {
            return false;
          }

          if ('subscriptions' in this.product) {
            return this.product['subscriptions'].some(x => response['data'].product_id === x.id);
          } else if ('relations' in this.product) {
            return this.product['relations'].some(x => response['data'].product_id === x);
          }

          return false;
        })
    );
  }

  booking() {
    if (!this.apiService.user['id']) {
      this.handleUserNotAuthenticated();
    } else {
      this.userInfo = this.apiService.user;
      if (this.needPrepayment) {
        this.handlePrepaymentBooking();
      } else {
        if (this.company['required_client_address'] && !this.apiService.user['address']) {
          this.showLocationModal();
        } else {
          this.handleNonPrepaymentBooking();
        }
      }
    }
  }

  handleUserNotAuthenticated() {
    const afterLoginInfo = {
      pathname: window.location.pathname,
      action: 'redirect',
      queryParams: {
        ...this.router.parseUrl(this.location.path()).queryParams,
      },
    };
    window.localStorage.setItem('AFTER_LOGIN_BOOKING', JSON.stringify(afterLoginInfo));
    this.authService.startAuthEvent.emit(true);
  }

  showLocationModal() {
    setTimeout(() => {
      this.locationModal && this.locationModal.showModal();
    }, 1000);
  }

  handleNonPrepaymentBooking() {
    if (this.isSubmitted) {
      return;
    }

    this.isSubmitted = true;
    this.doSign(
        this.product['id'],
        this.employee['id'],
        moment(this.time).format('YYYY-MM-DD HH:mm'),
        this.comment
    );
  }

  getCallbackUrl(route) {
    let callbackUrl = '';
    this.route.queryParams.subscribe(params => {
      const queryParams = { ...params };
      const domain = window.location.origin;
      callbackUrl = `${domain}/${route}?${this.serializeQueryParams(queryParams)}`;
    });

    return callbackUrl;
  }

  serializeQueryParams(params) {
    return Object.keys(params)
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
        .join('&');
  }

  handlePrepaymentBooking() {
    const foundedProduct = this.findProductForPrepayment();
    if (foundedProduct) {
      // Кодирование кириллицы в комментариях;
      if (this.comment) {
        const commentString = this.comment;
        const utf8Bytes = new TextEncoder().encode(commentString);
        const base64String = btoa(String.fromCharCode(...utf8Bytes));
        localStorage.setItem('b_data', JSON.stringify({
          comment: base64String,
        }));
      }
    }

    this.navigateToPaymentPage();
  }

  findProductForPrepayment() {
    if ('subscriptions' in this.product) {
      return this.product['subscriptions'].find(subscription => this.userInfo['client_products'].includes(subscription.id));
    } else if ('relations' in this.product) {
      return this.product['relations'].find(relation => this.userInfo['client_products'].includes(relation));
    }
  }

  navigateToPaymentPage() {
    const queryParams = {
      ...this.router.parseUrl(this.location.path()).queryParams,
    };

    this.router.navigate(['../payment'], {
      queryParams,
      relativeTo: this.route
    });
  }

  doSign(product, employee, time, comment, payment_transaction = null) {
    this.apiService
      .booking(product, employee, time, comment, this.sub, payment_transaction)
      .pipe(
        switchMap((res) => {

          this.isSubmitted = false;

          if (res['status'] === 'success') {
            this.router.navigate(['../finish'], {
              queryParams: {
                finish_type: res['data']['online_accept'],
                session_id: null,
                action: null,
              },
              relativeTo: this.route,
              queryParamsHandling: 'merge',
            });
          } else {
            this.signErrorText = res['errors'][0]['description'];
            this.notification.pushAlert({ detail: this.translate.instant(res['errors'][0]['description']) });
          }

          return of(res);
        }),
        take(1)
      )
      .subscribe((response) => {
        if (this.session_id) {
          this.delParamsFromUrl(['session_id']);
        }
      });
  }

  confirmPosition() {
    if (this.address && this.latitude && this.longitude) {
      this.userInfo['address'] = this.address;
      this.userInfo['latitude'] = this.latitude;
      this.userInfo['longitude'] = this.longitude;

      this.apiService.setUser(this.userInfo).subscribe(() => {
        this.locationModal.hideModal();
        this.booking();
      });
    }
  }

  cancelBooking() {
    this.locationModal.hideModal();
    this.router.navigate([`/card/${this.company['id']}/${this.branch['id']}`]);
  }

  checkSession() {
    this.apiService
      .checkStripeSession(this.session_id, this.company['id'])
      .pipe(take(1))
      .subscribe((response) => {
        if (response['status'] === 'success') {
          if (response['data']['payment_status'] == 1) {
            const time = moment(this.time).format('YYYY-MM-DD HH:mm:ss');
            this.doSign(this.product['id'], this.employee['id'], time, this.comment, response['data']['transaction']);
          } else {
            this.delParamsFromUrl(['session_id']);
            this.notification.pushAlert({ detail: this.translate.instant('online-payment.decline') });
            this.router.navigate([`/card/${this.company['id']}/${this.branch['id']}`]);
          }
        }
      });
  }

  delParamsFromUrl(paramsToRemove: string[]) {
    const queryParams = {};
    paramsToRemove.forEach((param) => {
      queryParams[param] = null;
    });
    const urlTree = this.router.createUrlTree([], {
      relativeTo: this.activatedRoute,
      queryParams: queryParams,
      queryParamsHandling: 'merge',
    });

    this.location.go(urlTree.toString());
  }

  closePositionModal() {
    if (!this.userInfo['address']) {
      this.latitude = 0;
      this.longitude = 0;
      this.address = null;
      this.selectedAddress = null;
      this.zoom = 2;
    }

    this.locationModal.hideModal();
  }

  loadPlacesAutocomplete() {
    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder();
      setTimeout(() => {
        const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
          types: ['address'],
        });
        autocomplete.addListener('place_changed', () => {
          this.ngZone.run(() => {
            // get the place result
            const place: google.maps.places.PlaceResult = autocomplete.getPlace();

            // verify result
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }
            this.latitude = place.geometry.location.lat();
            this.longitude = place.geometry.location.lng();
            this.zoom = 15;

            this.getAddressComponentByPlace(place);
          });
        });
      }, 500);
    });
  }

  public setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }
  private getAddressComponentByPlace(place) {
    const components = place.address_components;
    let country = null;
    let city = null;
    let postalCode = null;
    let street_number = null;
    let route = null;
    let locality = null;

    for (let i = 0, component; (component = components[i]); i++) {
      if (component.types[0] === 'country') {
        country = component['long_name'];
      }
      if (component.types[0] === 'administrative_area_level_1') {
        city = component['long_name'];
      }
      if (component.types[0] === 'postal_code') {
        postalCode = component['short_name'];
      }
      if (component.types[0] === 'street_number') {
        street_number = component['short_name'];
      }
      if (component.types[0] === 'route') {
        route = component['long_name'];
      }
      if (component.types[0] === 'locality') {
        locality = component['short_name'];
      }
    }

    this.selectedAddress = place['formatted_address'];
    this.address = place['formatted_address'];
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ location: { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 15;
          this.getAddressComponentByPlace(results[0]);
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  waitBtnSignInit() {
    if (!this.btnSign) {
      setTimeout(() => {
        this.waitBtnSignInit();
      }, 500);
    } else {
      this.doScrollToSignButton();
    }
  }

  doScrollToSignButton() {
    if (!this.btnSign)
      return;

    const btnSign = this.btnSign.nativeElement;

    function isElementInViewport(el) {
      const rect = el.getBoundingClientRect();
      return (
          rect.top >= 0 &&
          rect.left >= 0 &&
          rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
          rect.right <= (window.innerWidth || document.documentElement.clientWidth)
      );
    }

    function scrollToButton() {
      if (!isElementInViewport(btnSign)) {
        window.scrollBy(0, 10); // Можно настроить скорость
        setTimeout(scrollToButton, 10);
      }
    }

    scrollToButton();
  }

  ngOnDestroy() {
    window.localStorage.removeItem('AFTER_LOGIN_BOOKING');
    this.unsubscribe.next(true);
    this.unsubscribe.complete();
  }
}
