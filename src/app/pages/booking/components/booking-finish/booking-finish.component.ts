import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BookingService } from '../../../../core/service/booking.service';
import moment from 'moment';
import { environment } from '../../../../../environments/environment';
import { ApiService } from 'src/app/core/service/api.service';
import { GapiCalendarService } from 'src/app/core/service/gapi.calendar.service';

@Component({
  selector: 'app-booking-finish',
  templateUrl: './booking-finish.component.html',
  styleUrls: ['./booking-finish.component.scss'],
})
export class BookingFinishComponent implements OnInit, OnDestroy {
  public apiUrl = environment.ApiUrl;
  public finishType: number;
  public employee;
  public product;
  public time;
  public displayDate;
  public company;
  public googleEmail: string;
  isWidget: boolean = false;

  private unsubscribe: Subject<any> = new Subject();

  constructor(private bookingService: BookingService, private apiService: ApiService, public calendar: GapiCalendarService) {}

  ngOnInit() {
    this.bookingService.isLoaded.pipe(takeUntil(this.unsubscribe)).subscribe((status) => {
      if (status) {
        this.isWidget = this.bookingService.isWidget;
        this.employee = this.bookingService.selectedOptions.employee;
        this.product = this.bookingService.selectedOptions.product;
        this.time = this.bookingService.selectedOptions.date;
        this.displayDate = moment(this.time['from_time']).format('YYYY-MM-DD');
        this.company = this.bookingService.companyInfo;
        this.finishType = Number(this.bookingService.queryParams.finishType);
      }
    });

    this.calendar.syncEmail.pipe(takeUntil(this.unsubscribe)).subscribe((email) => {
      this.googleEmail = email;
  });
  }

  doSync(status) {
    if (status) {
      this.calendar.init(true).then((userEmail) => {
        if (userEmail) {
          this.apiService.syncGoogleCalendar(userEmail, status).subscribe((response) => {
            if (response['status'] == 'success') {
              this.calendar.syncEmail.next(userEmail);
              this.calendar.init(false);
            }
          });
        }
      });
    } else {
      this.apiService.syncGoogleCalendar('', status).subscribe((response) => {
        if (response['status'] == 'success') {
          this.calendar.syncEmail.next('');
          this.calendar.calendarSyncDisconnect();
        }
      });
    }
  }
  
  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
