import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookingComponent } from './booking.component';
import { BookingFinishComponent } from './components/booking-finish/booking-finish.component';
import { ChoiceEmployeeComponent } from './components/choice-employee/choice-employee.component';
import { ChoiceServiceComponent } from './components/choice-service/choice-service.component';
import { ChoiceTimeComponent } from './components/choice-time/choice-time.component';
import { ChoiceTypeComponent } from './components/choice-type/choice-type.component';
import { ConfirmBookingComponent } from './components/confirm-booking/confirm-booking.component';
import {BookingPaymentMethodComponent} from './components/payment-method/payment-method.component';

const routes: Routes = [
  {
    path: '',
    component: BookingComponent,
    children: [
      {
        path: 'service',
        component: ChoiceServiceComponent,
      },
      {
        path: 'employee',
        component: ChoiceEmployeeComponent,
      },
      {
        path: 'type',
        component: ChoiceTypeComponent,
      },
      {
        path: 'time',
        component: ChoiceTimeComponent,
      },
      {
        path: 'confirm',
        component: ConfirmBookingComponent,
      },
      {
        path: 'payment',
        component: BookingPaymentMethodComponent,
      },
      {
        path: 'finish',
        component: BookingFinishComponent,
      },
    ],
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)],
})
export class BookingRoutingModule {}
