import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingComponent } from './booking.component';
import { BookingHeaderComponent } from './components/booking-header/booking-header.component';
import { ChoiceServiceComponent } from './components/choice-service/choice-service.component';
import { ChoiceTimeComponent } from './components/choice-time/choice-time.component';
import { ConfirmBookingComponent } from './components/confirm-booking/confirm-booking.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AccordionModule } from 'src/app/shared/accordion/accordion.module';
import { DatePickerModule } from 'src/app/shared/date-picker/date-picker.module';
import { StubModule } from 'src/app/shared/stub/stub.module';
import { BookingRoutingModule } from './booking-routing.module';
import { ChoiceTypeComponent } from './components/choice-type/choice-type.component';
import { ChoiceEmployeeComponent } from './components/choice-employee/choice-employee.component';
import { BookingService } from '../../core/service/booking.service';
import { BookingFinishComponent } from './components/booking-finish/booking-finish.component';
import {BookingFiltersComponent} from './components/booking-filters/booking-filters.component';
import {BookingPaymentMethodComponent} from './components/payment-method/payment-method.component';

@NgModule({
  declarations: [
    BookingComponent,
    BookingHeaderComponent,
    ChoiceServiceComponent,
    ChoiceTimeComponent,
    ChoiceTypeComponent,
    ChoiceEmployeeComponent,
    ChoiceTimeComponent,
    ConfirmBookingComponent,
    BookingFinishComponent,
    BookingFiltersComponent,
    BookingPaymentMethodComponent,
  ],
  imports: [CommonModule, SharedModule, AccordionModule, DatePickerModule, StubModule, BookingRoutingModule],
  exports: [
    BookingComponent,
    BookingHeaderComponent,
    ChoiceServiceComponent,
    ChoiceTimeComponent,
    ChoiceTypeComponent,
    ChoiceEmployeeComponent,
    ConfirmBookingComponent,
    BookingFinishComponent,
    BookingFiltersComponent,
    BookingPaymentMethodComponent,
  ],
})
export class BookingModule {}
