import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-subscriptions-modal',
  templateUrl: './subscriptions-modal.component.html',
  styleUrls: ['./subscriptions-modal.component.scss'],
})
export class SubscriptionsModalComponent implements OnInit {
  // Событие закрытия модального окна для родительского компонента
  @Input() data: any = [];
  @Input() currency;
  @Input() product;
  @Output() closeModalEvent = new EventEmitter<Boolean>();

  subcriptions: any = [];
  Object = Object;

  constructor(public translate: TranslateService) {}

  ngOnInit() {
    Object.keys(this.data).map((key) => {
      Object.keys(this.data[key]['classes']).map((id) => {
        if (id == this.product['product_id']) {
          this.subcriptions = this.data[key]['classes'][id]['subscriptions'];
        }
      });
    });
  }

  closeModal() {
    this.closeModalEvent.emit(true);
  }
}
