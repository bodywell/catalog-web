import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HeaderModule } from '../header/header.module';
import { AccordionModule } from 'src/app/shared/accordion/accordion.module';
import { DatePickerModule } from '../../shared/date-picker/date-picker.module';
import { PhotoSliderModule } from 'src/app/shared/photo-slider/photo-slider.module';
import { StubModule } from 'src/app/shared/stub/stub.module';
import { NgxPaginationModule } from 'ngx-pagination';
import {CoreModule} from '../../core/core.module';

@NgModule({
  declarations: [
      CardComponent,
  ],
    imports: [
        CommonModule,
        SharedModule,
        HeaderModule,
        AccordionModule,
        DatePickerModule,
        PhotoSliderModule,
        StubModule,
        NgxPaginationModule,
    ],
  exports: [
      CardComponent,
  ]
})
export class CardModule { }
