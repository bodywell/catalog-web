import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from 'src/app/core/service/api.service';
import {environment} from '../../../environments/environment';
import * as moment from 'moment';
import {PaginationInstance} from 'ngx-pagination';
import {take, takeUntil} from 'rxjs/internal/operators';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {CompanyEmployeesService} from 'src/app/core/service/employees.service';
import {BookingService} from '../../core/service/booking.service';
import {productType} from '../../shared/constants/product';
import {AppAuthService} from '../../core/service/auth.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit, OnDestroy {
  companyId: number;
  branchId: number;
  companyInfo: object = {};
  branchInfo: object = {};
  locationInfo: number[] = [];
  allBranches = [];
  apiUrl: string = environment.ApiUrl;
  days: string[] = [
    'weekday-monday',
    'weekday-tuesday',
    'weekday-wednesday',
    'weekday-thursday',
    'weekday-friday',
    'weekday-saturday',
    'weekday-sunday',
  ];
  todayWorkTime = '';
  services: any = [];
  subscriptions: any = {};
  service_packages: any = {};
  selectedDate: Date = new Date();
  team: object[];
  times: object[];
  groups: any = [];
  personalGroups: any = [];
  showAllGroups = false;
  dateNow = new Date();
  page: number = 1;
  public config: PaginationInstance = {
    id: 'custom',
    itemsPerPage: 2,
    currentPage: 1,
    totalItems: 0,
  };
  comments = [];
  isShowMobileWorkTime = false;
  Object = Object;
  isShowSubDetailedModal = false;
  isShowSubModal = false;
  subModalData = {};
  subModalProductData;
  visibleBookBtn = false;
  eventsNextDate = null;
  groupDisplayMode = 0;
  productType = productType;
  isEnabledPurchase: any = false;
  isLoading = true;

  private unsubscribe: Subject<any> = new Subject();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public apiService: ApiService,
    public translate: TranslateService,
    private employeeService: CompanyEmployeesService,
    private bookingService: BookingService,
    private authService: AppAuthService,
  ) {
    this.route.params.pipe(takeUntil(this.unsubscribe)).subscribe((params) => {
      this.companyId = parseInt(params['company_id'], 10);
      this.branchId = parseInt(params['branch_id'], 10);
    });
  }

  @HostListener('window:scroll', ['$event']) onScrollEvent($event) {
    if (window.scrollY >= 470) {
      this.visibleBookBtn = true;
    } else {
      this.visibleBookBtn = false;
    }
  }

  ngOnInit() {
    this.checkReferralLink();

    // Получаем компанию
    this.apiService
      .getCompanyById(this.companyId)
      .pipe(take(1))
      .subscribe((response) => {
        if (response['status'] === 'success') {
          // Сохраняем всю информацию о компании
          this.companyInfo = {
            ...response['data'],
            industriesTitle: response['data']['industries'].slice(0, 3),
          };
          this.isEnabledPurchase = Boolean(this.companyInfo['online_payment']);
          // Перебераем все филиалы
          for (let i = 0; i < this.companyInfo['branches'].length; i++) {
            // Записываем долготу и широту
            const lat: number = this.companyInfo['branches'][i]['latitude'];
            const lon: number = this.companyInfo['branches'][i]['longitude'];

            // Если это филиал на котором мы сейчас находимся, записываем его координаты и всю информацию о нем
            if (this.branchId === this.companyInfo['branches'][i]['id']) {
              // Записываем координаты филиала
              this.locationInfo[0] = lat;
              this.locationInfo[1] = lon;

              // Записываем общую инфу о филиали
              this.branchInfo = this.companyInfo['branches'][i];
              // Запрашиваем информацию о членах команды филиала
              this.employeeService
                .getAll({ companyId: this.companyId, branchId: this.companyInfo['branches'][i]['id'] })
                .pipe(take(1))
                .subscribe((res) => {
                  if (res['status'] === 'success') {
                    if (this.companyInfo['type'] === 1) {
                      const owner = res['data'].find((employee) => employee.role === 1);
                      this.companyInfo['employee_id'] = owner ? owner.id : null;
                    }
                    this.team = res['data'].filter((employee) => employee.is_online_booking);
                  }
                });

              if (this.branchInfo['services'].length) {
                this.services = this.bookingService.parseByCategory(this.branchInfo['services'], this.companyInfo);

                // Сортируем services по category_title
                this.services.sort((a, b) => a.category_title.localeCompare(b.category_title));

                // Сортируем items в каждой категории по title
                this.services.forEach(category => {
                  category.items.sort((a, b) => a.title.localeCompare(b.title));
                });
                let _packages = {};
                this.branchInfo['services'].forEach((row) => {
                  row.service_packages.forEach((pack) => {
                    if (Object.keys(_packages).indexOf(pack['id'].toString()) == -1) {
                      _packages[pack.id] = pack;
                    }
                    if (Object.keys(_packages[pack.id]).indexOf('services') == -1) {
                      _packages[pack.id]['services'] = {};
                    }
                    let obj = {};
                    pack.currency = this.companyInfo['currency'];
                    obj[row.id] = row;
                    Object.assign(_packages[pack.id]['services'], obj);
                  });
                });

                const sortedValues = Object.values(_packages);
                this.branchInfo['service_packages'] = sortedValues.sort((a, b) => b['id'] - a['id']);
              } else {
                this.branchInfo['service_packages'] = this.service_packages;
              }

              const parseGroups = function (groups, target) {
                let _sub = {};
                groups.forEach((row) => {
                  row.subscriptions.forEach((sub) => {
                    const subId = sub['id'].toString();
                    if (!(_sub[subId])) {
                      _sub[subId] = sub;
                      _sub[subId]['classes'] = {};
                    }
                    let obj = {};
                    obj[row.id] = row;
                    Object.assign(_sub[subId]['classes'], obj);
                  });
                });
                target['subscriptions'] = { ...target['subscriptions'], ..._sub };
              }

              // Отпарсим занятия для группировки связанных с ними абонементов
              if (this.branchInfo['groups'].length) {
                parseGroups(this.branchInfo['groups'], this.branchInfo);
              }

              if (this.branchInfo['groups_personal'] && this.branchInfo['groups_personal'].length) {
                parseGroups(this.branchInfo['groups_personal'], this.branchInfo);
                this.personalGroups = this.bookingService.parseByCategory(this.branchInfo['groups_personal'], this.companyInfo);
              }

              if (this.branchInfo['subscriptions'] && Object.keys(this.branchInfo['subscriptions']).length) {
                Object.keys(this.branchInfo['subscriptions']).forEach(key => {
                  this.branchInfo['subscriptions'][key].currency = this.companyInfo['currency'];
                });
                const sortedValues = Object.values(this.branchInfo['subscriptions']);
                this.branchInfo['subscriptions'] = sortedValues.sort((a, b) => b['id'] - a['id']);
              }

              // Узнаем номер дня в недели сегодняшнего дня
              const date = new Date();
              let numCurrentDay: number = date.getDay();

              // Немного его изменяем, потому что в js под 0 номером идет ВС, а у нас в колекции под 0 индексом понедельник
              if (numCurrentDay === 0) {
                numCurrentDay = 6;
              } else {
                numCurrentDay--;
              }

              // Разделяем строку с временем работы на две части, время открытия и время закрытия
              if (this.branchInfo['times'][numCurrentDay]['is_active'] === 1) {
                let worksTime = this.branchInfo['times'][numCurrentDay]['time'].split('-');
                this.todayWorkTime = worksTime[1];
                let hours = this.dateNow.getHours();
                if (hours > Number(this.todayWorkTime.split(':')[0]) || hours < Number(worksTime[0].split(':')[0])) {
                  this.todayWorkTime = '';
                }
              }
            } else {
              // Если это не филиал на котором мы находимся то записываем только нужную нам инфу
              const bran = {
                id: this.companyInfo['branches'][i]['id'],
                city: this.companyInfo['branches'][i]['city'],
                street: this.companyInfo['branches'][i]['street'],
                lat: lat,
                lon: lon,
              };
              this.allBranches.push(bran);
            }
          }

          this.isLoading = false;
        }
        if (this.companyInfo['session'] != 0) {
          this.selectDate(new Date());
        }
      });
    this.getComments(0);
  }

  private parseServicesItems(personalGroups): void {
    return personalGroups.reduce((acc, product) => {
      const hours = moment.duration(product['duration'], 'minutes').hours();
      const minutes = moment.duration(product['duration'] - hours * 60, 'minutes').minutes();
      const _product = {
        product_id: product['id'],
        title: product['title'],
        category: product['category'],
        duration: product['duration'],
        description: product['description'],
        h: hours,
        m: minutes,
      };
      let desc;

      try {
        desc = _product.description
          .split('\n')
          .map((str) => {
            // @ts-ignore
            return utf8.decode(str);
          })
          .join('\n');
      } catch (error) {}

      if (desc) {
        _product.description = desc;
      }

      const categoryIndex = acc.findIndex((i) => i.category_title === product['category']);
      if (categoryIndex === -1) {
        acc.push({
          category_title: product['category'],
          items: [_product],
        });
      } else {
        acc[categoryIndex].items.push(_product);
      }

      return acc;
    }, []);
  }

  private parseGroups(groups): void {
    return groups.reduce((acc, group) => {
      if (group['product']['type'] === 3) {
        const hours = moment.duration(group['product']['duration'], 'minutes').hours();
        const minutes = moment.duration(group['product']['duration'] - hours * 60, 'minutes').minutes();

        const products = group.employees.map((employee) => {
          let desc;

          try {
            desc = group['product']['description']
              .split('\n')
              .map((str) => {
                // @ts-ignore
                return utf8.decode(str);
              })
              .join('\n');
          } catch (error) {}

          if (desc) {
            group['product']['description'] = desc;
          }

          return {
            category_title: group['product']['category_title'],
            productId: group['product']['id'],
            employeeId: employee['id'],
            employeeName: employee['first_name'] + ' ' + employee['last_name'],
            fromDate: group['from_time'],
            toDate: group['to_time'],
            price: group['product']['price'],
            price_max: group['product']['price_max'],
            title: group['product']['title'],
            duration: group['product']['duration'],
            description: group['product']['description'],
            h: hours,
            m: minutes,
            clients: group['product']['clients'],
            limit: group['product']['limit'],
          };
        });

        const categoryIndex = acc.findIndex((i) => i.category === group['product']['category_title']);
        if (categoryIndex === -1) {
          acc.push({
            category: group['product']['category_title'],
            items: products,
          });
        } else {
          acc[categoryIndex].items = acc[categoryIndex].items.concat(products);
        }
      }

      return acc;
    }, []);
  }

  getComments(ofset) {
    this.apiService
      .getCompanyReview(`{"company_id": ${this.companyId}}`, ofset, 2, 'id ASC')
      .pipe(take(1))
      .subscribe((data) => {
        this.config.totalItems = data['data']['count'];
        this.comments = data['data']['comments'];
      });
  }

  selectService(service) {
    this.booking(service['product_id']);
  }

  addToFavorites() {
    this.apiService
      .addToFavorites(this.companyId, this.branchId)
      .pipe(take(1))
      .subscribe((response) => {
        if (response['status'] == 'success') {
          this.apiService
            .getSelfUser()
            .pipe(take(1))
            .subscribe((response) => {
              if (response['status'] === 'success') {
                this.apiService.user = response['data'];
              }
            });
        }
      });
  }
  deleteFavorites(id) {
    this.apiService
      .deleteFavorites(id)
      .pipe(take(1))
      .subscribe((response) => {
        if (response['status'] == 'success') {
          this.apiService
            .getSelfUser()
            .pipe(take(1))
            .subscribe((response) => {
              if (response['status'] === 'success') {
                this.apiService.user = response['data'];
              }
            });
        }
      });
  }

  changeCommentPage(event) {
    this.config.currentPage = event;
    this.getComments((event - 1) * 2);
  }

  selectDate(date, category_id = 0) {
      this.selectedDate = date;
      const endDay = new Date(date);
      const isClasses = (this.companyInfo['session'] == 1);
      endDay.setHours(23, 59, 59, 59);
      const startDate = moment(this.selectedDate).format('YYYY-MM-DD HH:mm:ss');
      let endDate = moment(endDay).format('YYYY-MM-DD HH:mm:ss');

      if (isClasses && this.groupDisplayMode == 0) {
        endDate = moment(endDate).add(30, 'days').format('YYYY-MM-DD HH:mm:ss');
      }

      let offset = 0;
      if (isClasses && category_id) {
        this.groups.forEach((row) => {
          if (row['category_id'] == category_id) {
            offset = row['bookings'].length;
          }
        });
      }

      this.apiService
          .getBookingTime(0, this.branchId, 0, startDate, endDate, category_id, offset)
          .pipe(take(1))
          .subscribe((response) => {
            if (response['status'] === 'success') {
              if (isClasses) {
                if (Array.isArray(response['data'])) {
                  if (this.companyInfo['online_payment']) {
                    response['data'] = this.bookingService.parseGroupsForPrepaymentValue(
                        response['data'],
                        this.branchInfo['subscriptions'],
                        this.companyInfo['prepayment_value']
                    );
                  }
                  if (offset && category_id) {
                    Object.keys(this.groups).forEach((key) => {
                      if (this.groups[key]['category_id'] == category_id) {
                        response['data'].forEach((row) => {
                          if (this.groups[key]['category_id'] == row['category_id']) {
                            this.groups[key]['bookings'].push(...row['bookings']);
                          }
                        });
                      }
                    });
                  } else {
                    this.groups = response['data'];
                  }
                } else {
                  this.groups = [];
                }
              } else {
                this.times = response['data'];
              }

              if (!Array.isArray(response['data'])) {
                this.eventsNextDate = response['data'];
              }
            }
          });
  }

  getTime(date) {
    return moment(date).format('HH:mm');
  }

  selectGroup(time, isPersonal) {
    this.booking(time['product_id'], time['employee_id'], time['from_date'], isPersonal);
  }

  lazyLoad(category_id) {
    this.selectDate(this.selectedDate, category_id);
  }

  booking(productId = 0, employeeId = 0, time = '', isPersonal = false) {
    const isIndividual = this.companyInfo['type'] === 1;
    const _employeeId = !isIndividual ? employeeId : this.companyInfo['employee_id'];

    if (productId === 0) {
      this.router.navigate([`/booking/${this.companyInfo['session'] ? 'type' : 'service'}`], {
        queryParams: {
          company_id: this.companyId,
          branch_id: this.branchId,
          employee_id: _employeeId,
        },
      });
    } else if (this.companyInfo['session'] == 0) {
      this.router.navigate([`/booking/${!isIndividual ? 'employee' : 'time'}`], {
        queryParams: {
          company_id: this.companyId,
          branch_id: this.branchId,
          product_id: productId,
          employee_id: _employeeId,
        },
      });
    } else if (this.companyInfo['session'] == 1) {
      this.router.navigate([ `/booking/${_employeeId
              ? (isPersonal ? 'time' : 'confirm')
              : 'employee'
      }`], {
        queryParams: {
          company_id: this.companyId,
          branch_id: this.branchId,
          product_id: productId,
          employee_id: _employeeId,
          book_date: time,
          type: Number(isPersonal),
        },
      });
    }
  }

  purchase() {
    this.router.navigate([`/purchase/products`], {
      queryParams: {
        company_id: this.companyId,
        branch_id: this.branchId,
        type: (this.companyInfo['session']) ? productType.SUBSCRIPTIONS : productType.COURSE
      },
    });
  }

  showSubDetailedModal(data) {
    this.subModalData = data;
    this.isShowSubDetailedModal = true;
  }

  showProductDetailedModal(data) {
    this.subModalProductData = data;
    this.isShowSubModal = true;
  }

  closeSubDetailedModal() {
    this.isShowSubDetailedModal = false;
  }

  setGroupDisplayMode(mode) {
    this.groupDisplayMode = mode;
    this.selectDate(new Date());
  }

  private checkReferralLink(): void {
    const tmpToken = this.apiService.token;
    const ref = this.route.snapshot.queryParamMap.get('ref');
    let storageData: any = window.localStorage.getItem('ref') ? window.localStorage.getItem('ref') : '[]';
    try {
      storageData = JSON.parse(storageData);
      if (!Array.isArray(storageData) || !storageData.every(item => typeof item === 'object')) {
        throw new Error('Invalid format');
      }
    } catch (e) {
      window.localStorage.removeItem('ref');
      storageData = [];
    }

    const isReferral = () => {
      return Array.isArray(storageData) && storageData.some(x => x.bid == this.branchId);
    };

    if (ref && !isReferral()) {
      const refItem = { cid: this.companyId, bid: this.branchId, ref: ref };
      storageData.push(refItem);
      localStorage.setItem('ref', JSON.stringify(storageData));
      this.apiService.setReferralLinkClick(this.companyId, this.branchId, ref).pipe(take(1)).subscribe((response) => {
        if (tmpToken) {
          this.apiService
              .userSyncByReferral(tmpToken, this.companyId, this.branchId, ref)
              .pipe(take(1))
              .subscribe((res) => {});
        }
      });
    }
  }

  closeSubModal() {
    this.isShowSubModal = false;
  }

  public get nextDate(): string {
    return moment(this.eventsNextDate.next_date).format('MMMM DD, YYYY');
  }

  redirectToPurchase(type: any) {
    this.router.navigate([`/purchase/products`], {
      queryParams: {
        company_id: this.companyId,
        branch_id: this.branchId,
        type: type,
      },
    });
  }

  redirectToPurchaseConfirm(data: any, type: any) {
    const queryParams = {
      company_id: this.companyId,
      branch_id: this.branchId,
      type: type,
      product_id: data['id']
    };
    const redirectLink = 'purchase/confirm';

    if (!this.apiService.user['id']) {
      const afterLoginInfo = {
        action: 'redirect',
        pathname: redirectLink,
        queryParams: queryParams,
      };
      window.localStorage.setItem('AFTER_LOGIN_ACTION', JSON.stringify(afterLoginInfo));
      this.authService.startAuthEvent.emit(true);
    } else {
      this.router.navigate([redirectLink], {
        queryParams: queryParams
      });
    }
  }

  ngOnDestroy() {
    this.unsubscribe.next(true);
    this.unsubscribe.complete();
  }
}
