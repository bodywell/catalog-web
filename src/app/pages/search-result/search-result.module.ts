import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchResultComponent } from './search-result.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HeaderModule } from '../header/header.module';
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';
import { PhotoSliderModule } from 'src/app/shared/photo-slider/photo-slider.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
@NgModule({
  declarations: [SearchResultComponent],
  imports: [
    CommonModule,
    SharedModule,
    HeaderModule,
    AgmSnazzyInfoWindowModule,
    PhotoSliderModule,
    InfiniteScrollModule
  ],
  exports: [
    SearchResultComponent
  ]
})
export class SearchResultModule { }
