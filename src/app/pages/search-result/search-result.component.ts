import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/service/api.service';
import { environment } from '../../../environments/environment';
import * as moment from 'moment';
import { take, takeUntil } from 'rxjs/internal/operators';
import { LocationService } from '../../core/service/location.service';
import { Subject } from 'rxjs/index';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss'],
  host: {
    '(document:click)': 'onClick($event)',
  },
})
export class SearchResultComponent implements OnInit {
  title: string = 'My first AGM project';
  lat: number = 51.678418;
  lng: number = 7.809007;
  rate = 4.5;
  mode = true;
  mobile = false;
  isShowCategoryMenu = false;
  isShowLocationMenu = false;
  isShowRatingMenu = false;
  apiUrl: string = environment.ApiUrl;

  search;
  type;
  radius;
  longitude;
  latitude;
  industriesIds;
  ratingIds;
  offset;

  categoryFilter = '';
  industries;
  industriesFilterIds = [];
  industriesFilterIdsSaved = [];
  isCheckAllInd = false;

  searchResult = [];

  locations = [];
  locationFilter = '';
  countryFilter = '';

  ratingFilterIds = [];
  ratingFilterIdsSaved = [];
  city = '';
  date = new Date();

  isMobileFilters = false;
  isMobileCategoryFilters = false;
  mobileRating = 1;

  opendMobileCategoryFilterBeauty = false;
  opendMobileCategoryFilterHealth = false;
  opendMobileCategoryFilterSport = false;
  opendMobileCategoryFilterEducation = false;
  isCheckAllIndBeauty = false;
  isCheckAllIndHealth = false;
  isCheckAllIndSport = false;
  isCheckAllIndEducation = false;

  scrolledObject = 1;

  isLoaded = false;
  private unsubscribe: Subject<any> = new Subject();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public apiService: ApiService,
    public locationService: LocationService,
    private translate: TranslateService
  ) {
    this.route.queryParams.subscribe((params) => {
      this.search = params['search'];
      this.type = params['type'];
      this.radius = params['radius'];
      this.longitude = params['longitude'];
      this.latitude = params['latitude'];
      this.industriesIds = params['industries']
        ? params['industries'].split(',').map((el) => {
            let n = Number(el);
            return n === 0 ? n : n || el;
          })
        : [];
      this.industriesFilterIds = params['industries']
        ? params['industries'].split(',').map((el) => {
            let n = Number(el);
            return n === 0 ? n : n || el;
          })
        : [];
      this.industriesFilterIdsSaved = params['industries']
        ? params['industries'].split(',').map((el) => {
            let n = Number(el);
            return n === 0 ? n : n || el;
          })
        : [];
      this.offset = Number(params['offset']);
      this.city = params['city'];
      this.locationFilter = params['city'];
      this.countryFilter = params['country'];
      this.searchCompany();
    });
    if (window.innerWidth <= 1024) {
      this.mobile = true;
      this.mode = false;
    }
  }

  ngOnInit() {
    this.locationFilter = this.locationService.selectedLocation.city;
    this.apiService
      .getIndustries()
      .pipe(take(1))
      .subscribe((data) => {
        if (data['status'] === 'success') {
          this.industries = data['data'];
        }
      });
    this.apiService
      .getCompanyLocations()
      .pipe(take(1))
      .subscribe((data) => {
        if (data['status'] === 'success') {
          this.locations = data['data'];
        }
      });

    this.locationService.isChanged.pipe(takeUntil(this.unsubscribe)).subscribe((location) => {
      this.locationFilter = this.locationService.selectedLocation.city;
      this.countryFilter = this.locationService.selectedLocation.country;
      this.searchCompany();
    });
  }

  workTime(branch) {
    if (branch['is_active'] == false) {
      return false;
    }

    if (branch['time']) {
      const worksTime = branch['time'].split('-');
      const todayWorkTime = worksTime[1];
      const hours = this.date.getHours();
      if (hours > Number(todayWorkTime.split(':')[0]) || hours < Number(worksTime[0].split(':')[0])) {
        return false;
      }

      return todayWorkTime;
    }
  }

  onScroll() {
    this.offset += 20;
    this.searchCompany(true);
  }

  searchCompany(isScroll = false) {
    if (this.industriesFilterIdsSaved.length > 0) {
      this.industriesIds = this.industriesFilterIdsSaved.join(',');
    } else {
      this.industriesIds = '';
    }

    if (this.ratingFilterIdsSaved.length > 0) {
      this.ratingIds = this.ratingFilterIdsSaved.join(',');
    } else {
      this.ratingIds = '';
    }

    if (!isScroll) {
      this.offset = 0;
    }

    this.apiService
      .search(
        this.offset,
        this.search,
        this.latitude,
        this.longitude,
        this.radius,
        this.industriesIds,
        this.ratingIds,
        this.locationFilter,
        this.countryFilter
      )
      .pipe(take(1))
      .subscribe((data) => {
        this.isLoaded = true;
        if (data['status'] == 'success') {
          const result = data['data'].map((i) => ({
            ...i,
            industriesTitle: i.industries.slice(0, 3),
          }));

          if (isScroll && result.length > 0) {
            this.searchResult = this.searchResult.concat(result);
          } else if (!isScroll) {
            if (result.length > 0) {
              this.searchResult = result;
            } else {
              this.searchResult = [];
            }
          }
        }
      });
  }
  getTime(date) {
    return moment(date).format('HH:mm');
  }

  changeMobileRating(rate, isManual) {
    if (isManual) {
      this.mobileRating = rate;
      this.ratingFilterIdsSaved = Array.from({ length: rate }, (v, k) => k + 1);
      this.searchCompany();
    }
  }

  saveMobileFilters() {
    this.isMobileFilters = false;
  }
  searchEvent(data) {
    this.searchCompany();
  }
  addToFavorites(companyId, branchId) {
    this.apiService
      .addToFavorites(companyId, branchId)
      .pipe(take(1))
      .subscribe((response) => {
        if (response['status'] == 'success') {
          this.apiService
            .getSelfUser()
            .pipe(take(1))
            .subscribe((response) => {
              if (response['status'] === 'success') {
                this.apiService.user = response['data'];
              }
            });
        }
      });
  }
  deleteFavorites(id) {
    this.apiService
      .deleteFavorites(id)
      .pipe(take(1))
      .subscribe((response) => {
        if (response['status'] == 'success') {
          this.apiService
            .getSelfUser()
            .pipe(take(1))
            .subscribe((response) => {
              if (response['status'] === 'success') {
                this.apiService.user = response['data'];
              }
            });
        }
      });
  }

  selectCategoryFilter(key) {
    this.categoryFilter = key;
    this.isCheckAllInd = false;
  }
  selectLocationFilter(city) {
    this.locationFilter = city;
    this.isShowLocationMenu = false;
    this.searchCompany();
  }
  clearLocationFilter(search = true) {
    this.locationFilter = '';
    this.isShowLocationMenu = false;
    if (search === true) {
      this.searchCompany();
    }
  }
  onSaveIndFilter(state, item) {
    if (state == true) {
      this.industriesFilterIds.push(item['id']);
    } else {
      var position = this.industriesFilterIds.indexOf(item['id']);
      if (~position) {
        this.industriesFilterIds.splice(position, 1);
      }
    }
  }

  onSaveRatingFilter(state, item) {
    if (state == true) {
      this.ratingFilterIds.push(item);
    } else {
      var position = this.ratingFilterIds.indexOf(item);
      if (~position) {
        this.ratingFilterIds.splice(position, 1);
      }
    }
  }

  isSelectedIndFilerItem(id) {
    var position = this.industriesFilterIds.indexOf(id);
    if (~position) {
      return true;
    }

    return false;
  }

  isSelectedRatingFilerItem(id) {
    var position = this.ratingFilterIds.indexOf(id);
    if (~position) {
      return true;
    }

    return false;
  }
  selectIndustriesFilter(isMobile = false) {
    this.industriesFilterIdsSaved = JSON.parse(JSON.stringify(this.industriesFilterIds));
    this.searchCompany();
    this.isShowCategoryMenu = false;
    this.categoryFilter = '';

    if (isMobile) {
      this.isMobileCategoryFilters = false;
    }
  }
  selectRatingFilter() {
    this.ratingFilterIdsSaved = JSON.parse(JSON.stringify(this.ratingFilterIds));
    this.searchCompany();
    this.isShowRatingMenu = false;
  }
  closeIndustriesFilter() {
    this.industriesFilterIds = JSON.parse(JSON.stringify(this.industriesFilterIdsSaved));
    this.categoryFilter = '';
    this.isShowCategoryMenu = false;
  }
  closeRatingFilter() {
    this.ratingFilterIds = JSON.parse(JSON.stringify(this.ratingFilterIdsSaved));
    this.isShowRatingMenu = false;
  }
  clearIndustriesFilter(search = true) {
    this.industriesFilterIds = [];
    this.industriesFilterIdsSaved = [];
    this.isShowCategoryMenu = false;
    this.categoryFilter = '';
    if (search === true) {
      this.searchCompany();
    }
  }
  clearAllFilters() {
    this.clearIndustriesFilter(false);
    this.clearLocationFilter(false);
    this.ratingFilterIdsSaved = [];
    this.isCheckAllIndBeauty = false;
    this.isCheckAllIndHealth = false;
    this.isCheckAllIndSport = false;
    this.isCheckAllIndEducation = false;

    this.searchCompany();

    this.isMobileFilters = false;
  }
  selectAllIndustriesFilter(state, key) {
    if (state == true) {
      for (let i = 0; i < this.industries[key].length; i++) {
        const position = this.industriesFilterIds.indexOf(this.industries[key][i]['id']);
        if (!~position) {
          this.industriesFilterIds.push(this.industries[key][i]['id']);
        }
      }
    } else {
      for (let i = 0; i < this.industries[key].length; i++) {
        const position = this.industriesFilterIds.indexOf(this.industries[key][i]['id']);
        if (~position) {
          this.industriesFilterIds.splice(position, 1);
        }
      }
    }
  }
  selectAllFilterFilter(state) {
    if (state == true) {
      this.ratingFilterIds = [1, 2, 3, 4, 5];
    } else {
      this.ratingFilterIds = [];
    }
  }
  onClick(event) {
    let isShowCategoryMenu = false;
    let isShowLocationMenu = false;
    let isShowRatingMenu = false;
    // if (!('path' in Event.prototype)) {
    //   Object.defineProperty(Event.prototype, 'path', {
    //     get: function () {
    //       const path = [];
    //       let currentElem = this.target;
    //       while (currentElem) {
    //         path.push(currentElem);
    //         currentElem = currentElem.parentElement;
    //       }
    //       if (path.indexOf(window) === -1 && path.indexOf(document) === -1)
    //         path.push(document);
    //       if (path.indexOf(window) === -1)
    //         path.push(window);
    //       return path;
    //     }
    //   });
    // }
    for (let i = 0; i < event.path.length; i++) {
      if (event.path[i].id === 'category-menu') {
        isShowCategoryMenu = true;
      }
      if (event.path[i].id === 'head-category-filter' && !isShowCategoryMenu) {
        this.isShowCategoryMenu = !this.isShowCategoryMenu;
        this.isShowLocationMenu = false;
        this.isShowRatingMenu = false;
        return;
      }

      if (event.path[i].id === 'location-menu') {
        isShowLocationMenu = true;
      }
      if (event.path[i].id === 'head-location-filter' && !isShowLocationMenu) {
        this.isShowLocationMenu = !this.isShowLocationMenu;
        this.isShowCategoryMenu = false;
        this.isShowRatingMenu = false;
        return;
      }

      if (event.path[i].id === 'rating-menu') {
        isShowRatingMenu = true;
      }
      if (event.path[i].id === 'head-rating-filter' && !isShowRatingMenu) {
        this.isShowRatingMenu = !this.isShowRatingMenu;
        this.isShowCategoryMenu = false;
        this.isShowLocationMenu = false;
        return;
      }
    }

    if (!isShowCategoryMenu) {
      this.isShowCategoryMenu = false;
    }

    if (!isShowLocationMenu) {
      this.isShowLocationMenu = false;
    }
    if (!isShowRatingMenu) {
      this.isShowRatingMenu = false;
    }
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth <= 1024) {
      this.mobile = true;
    } else {
      this.mobile = false;
    }
  }

  @HostListener('window:scroll', ['$event']) onScrollEvent($event) {
    // Проверяем наличие рабочих со скроллом объектов
    if (document.getElementsByClassName('scroll-object').length) {
      if (this.scrolledObject >= 1) {
        // Присваиваем следующий и предыдущий скролл-объект для работы с ними
        let div = document.getElementsByClassName('scroll-object')[this.scrolledObject];
        let previous_div = document.getElementsByClassName('scroll-object')[this.scrolledObject - 1];

        // Выполняем отслеживание до тех пор пока текущий скролл объект не привышает общее количество объектов
        if (document.getElementsByClassName('scroll-object').length) {
          // Рабзиваем высоту экрана на 4 условных границы, где 1я верхняя граница являетеся местом пересечения с объектом
          if (div) {
            if (window.scrollY + window.innerHeight / 4 >= div['offsetTop']) {
              // Утверждаемся в том, что это конкретно необходимый для нас объект и переходим к следующему
              if (div.getAttribute('data') == this.scrolledObject.toString()) {
                if (this.scrolledObject < document.getElementsByClassName('scroll-object').length) {
                  this.scrolledObject++;
                }
              }
            }
          }

          if (previous_div) {
            // Разбиваем высоту экрана так же на 4 границы, где отслеживаем обратный ход скролла и пересечение с объектом
            if (window.scrollY + window.innerHeight / 4 <= previous_div['offsetTop']) {
              if (previous_div.getAttribute('data') == (this.scrolledObject - 1).toString()) {
                if (this.scrolledObject > 1) {
                  this.scrolledObject--;
                } else {
                  this.scrolledObject = 1;
                }
              }
            }
          }
        }
      } else {
        this.scrolledObject = 1;
      }
    }
  }
}
