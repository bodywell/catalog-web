import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PurchaseComponent } from './purchase.component';
import { PurchaseProductsComponent } from './components/purchase-product/purchase-products.component';
import { PurchaseConfirmComponent } from './components/purchase-confirm/purchase-confirm.component';

const routes: Routes = [
    {
        path: '',
        component: PurchaseComponent,
        children: [
            {
                path: 'products',
                component: PurchaseProductsComponent,
            },
            {
                path: 'confirm',
                component: PurchaseConfirmComponent,
            }
        ],
    },
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)],
})
export class PurchaseRoutingModule {}
