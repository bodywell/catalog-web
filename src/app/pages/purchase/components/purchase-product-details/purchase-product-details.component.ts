import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {productType} from '../../../../shared/constants/product';

@Component({
  selector: 'app-purchase-product-details',
  templateUrl: './purchase-product-details.component.html',
  styleUrls: ['./purchase-product-details.component.scss']
})
export class PurchaseProductDetailsComponent implements OnInit {
  @Output() showDetailsModal = new EventEmitter<Boolean>();
  @Output() selectProduct = new EventEmitter<Boolean>();
  @Input() data;
  @Input() hideConfirmBtn = false;
  type;
  productType = productType;
  constructor() { }

  ngOnInit() {
    const keys = Object.keys(this.data);
    if (keys.includes('classes')) {
      this.type = productType.SUBSCRIPTIONS;
    } else if (keys.includes('services')) {
      this.type = productType.COURSE;
    } else {
      this.type = productType.GOODS;
    }
  }

  emitShowDetails() {
    this.showDetailsModal.emit(true);
  }

  emitSelect() {
    this.selectProduct.emit(true);
  }
}
