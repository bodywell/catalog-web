import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {productType} from '../../../../shared/constants/product';
import {PurchaseService} from '../../../../core/service/purchase.service';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';

@Component({
    selector: 'app-purchase-products',
    templateUrl: './purchase-products.component.html',
    styleUrls: ['./purchase-products.component.scss']
})
export class PurchaseProductsComponent implements OnInit, OnDestroy {
    productType = productType;
    type;
    products = [];
    Object = Object;
    target_id;
    target_title = '';
    isEnabledPurchase: any;

    private unsubscribe: Subject<any> = new Subject();
    constructor(private route: ActivatedRoute, private purchaseService: PurchaseService) {}

    ngOnInit() {
        this.purchaseService.productList.pipe(takeUntil(this.unsubscribe)).subscribe((data: any) => {
            this.products = data;
            const { type, target_id } = this.route.snapshot.queryParams;
            this.type = Number(type);
            this.target_id = Number(target_id);
        });

        this.purchaseService.isEnabledPurchase.pipe(takeUntil(this.unsubscribe)).subscribe((isEnabled: any) => {
           this.isEnabledPurchase = isEnabled;
        });

        if (this.target_id) {
            this.purchaseService.targetProduct.pipe(takeUntil(this.unsubscribe)).subscribe((targetProduct: any) => {
                this.target_title = targetProduct['title'];
            });
        }
    }

    showDetailsEvent(data) {
        this.purchaseService.setDetailsModalData(data);
        this.purchaseService.isOpenDetailsModal.emit(true);
    }

    selectItem(data) {
        this.purchaseService.setSelectedItem(data);
        this.purchaseService.isSelectedItem.emit(true);
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
