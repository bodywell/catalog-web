import {AfterViewInit, Component, DoCheck, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {PurchaseService} from '../../../../core/service/purchase.service';
import {CompanyService} from '../../../../core/service/company.service';
import {environment} from '../../../../../environments/environment';
import {Subject} from 'rxjs';
import {take, takeUntil} from 'rxjs/internal/operators';
import {ApiService} from '../../../../core/service/api.service';
import {StripeScriptTag} from 'stripe-angular';
import * as moment from 'moment';
import {NotificationMessageService} from '../../../../core/service/notification.service';
import {TranslateService} from '@ngx-translate/core';
import {AppAuthService} from '../../../../core/service/auth.service';
import {BookingService} from '../../../../core/service/booking.service';

@Component({
    selector: 'app-purchase-confirm',
    templateUrl: './purchase-confirm.component.html',
    styleUrls: ['./purchase-confirm.component.scss']
})
export class PurchaseConfirmComponent implements OnInit, AfterViewInit, OnDestroy {
    companyId;
    branchId;
    productId;
    sessionId;
    type;
    product;
    company;
    branch;
    apiUrl: string = environment.ApiUrl;
    isLoaded = false;
    status: any = 0;
    detailsData;
    displayContent = {
        PENDING: 0,
        PROCESSING: 1,
        PAYMENT_SUCCESS: 2,
        PAYMENT_ERROR: 3,
        TOTAL: 4
    };
    isWidget: boolean = false;
    private unsubscribe: Subject<any> = new Subject();
    constructor(private route: ActivatedRoute,
                private router: Router,
                private purchaseService: PurchaseService,
                private companyService: CompanyService,
                private apiService: ApiService,
                private bookingService: BookingService,
                public stripe: StripeScriptTag,
                public notifications: NotificationMessageService,
                public translate: TranslateService) {
        const { company_id, branch_id, product_id, session_id, type } = this.route.snapshot.queryParams;
        this.companyId = Number(company_id);
        this.branchId = Number(branch_id);
        this.productId = Number(product_id);
        this.sessionId = session_id;
        this.type = type;

        this.isWidget = this.bookingService.isWidget;
    }

    ngOnInit() {
        this.purchaseService.isInit.pipe(takeUntil(this.unsubscribe)).subscribe((isInit: any) => {
            if (isInit) {
                this.company = this.companyService.getCompanyProperties(['title', 'currency', 'stripe_account', 'session']);
                this.branch = this.companyService.getCompanyBranchProperties(['title'], this.branchId);
                this.product = this.purchaseService.getSelectedItem();

                this.setDetailsData();

                if (this.company['stripe_account']) {
                    this.stripe.setPublishableKey(environment.StripePublicKey, {
                        stripeAccount: this.company['stripe_account'],
                    });

                    if (this.sessionId) {
                        if (!this.apiService.token && localStorage.getItem('token')) {
                            this.apiService.token = localStorage.getItem('token');
                        }
                        this.setActualPaymentStatus(this.displayContent.PROCESSING);
                        this.checkSessionId();
                    } else {
                        this.setActualPaymentStatus(this.displayContent.PENDING);
                    }
                }

                this.isLoaded = isInit;
            }
        });
    }

    ngAfterViewInit() {
    }

    showDetails() {
        this.purchaseService.setDetailsModalData(this.product);
        this.purchaseService.isOpenDetailsModal.next(true);
    }

    setActualPaymentStatus(status) {
        this.status = status;
        this.purchaseService.actualPaymentStatus.emit(status);
    }

    doPurchase() {
        if (!this.productId) {
            console.log('Error: Invalid value of product Id');
            return;
        }

        let url = window.location.href;
        if (url.includes('/widgets')) {
            if (!url.includes('&lang=')) {
                url += '&lang=' + this.translate.currentLang;
            }
        }
        this.apiService.purchaseCreateInvoice({
            callback_url: url,
            product_id: this.productId,
        }).pipe(take(1))
            .subscribe((res) => {
                if (res['status'] == 'success') {
                    if (res['data']['sessionId']) {
                        // Set pending status;
                        this.setActualPaymentStatus(this.displayContent.PROCESSING);

                        setTimeout(() => {
                            this.stripe.StripeInstance.redirectToCheckout({
                                sessionId: res['data']['sessionId'],
                            });
                        },2000);
                    } else if (res['data']['payment_status']) {
                        const status = res['data']['payment_status'] + 1;
                        this.setActualPaymentStatus(status);
                        if (status == 2) {
                            this.displayTransactionResult();
                            if (!this.apiService.user['client_products'].includes(this.productId)) {
                                this.apiService.user['client_products'].push(this.productId);
                            }
                        }
                    }
                } else {
                    console.log(res['errors'][0]['description']);
                    this.notifications.pushAlert({ detail: this.translate.instant(res['errors'][0]['description']) });
                }
            });
    }

    checkSessionId() {
        this.apiService
            .purchaseCheckStripeSession(this.sessionId, this.companyId)
            .pipe(take(1))
            .subscribe((response) => {
                if (response['status'] === 'success') {
                    const status = response['data']['payment_status'] + 1;
                    if (response['data']['isPurchased']) {
                        this.setActualPaymentStatus(this.displayContent.TOTAL);
                    } else {
                        this.setActualPaymentStatus(status);
                        if (status == this.displayContent.PAYMENT_SUCCESS) {
                            this.displayTransactionResult();
                        }
                    }
                } else {
                    this.setActualPaymentStatus(this.displayContent.PAYMENT_ERROR);
                }
            });
    }

    tryAgainEvent() {
        if (this.sessionId) {
            const queryParams = {
                company_id: this.companyId,
                branch_id: this.branchId,
                type: this.type,
                product_id: this.productId
            };
            this.router.navigate(['purchase/confirm'], { queryParams });
        }
        this.setActualPaymentStatus(this.displayContent.PENDING);
    }

    setDetailsData() {
        this.detailsData = {
            ...this.product,
            company_id: this.companyId,
            company_title: this.company['title'],
            branch_title: this.branch['title'],
            company_currency: this.company['currency'],
            api_url: this.apiUrl,
        };
    }

    redirectToPersonalCabinet() {
        this.router.navigate(['user-subscription'], {});
    }

    redirectToBooking() {
        const AFTER_PURCHASING_PRODUCT = localStorage.getItem('AFTER_PURCHASING_PRODUCT');
        let pathname = `${(this.isWidget ? 'widgets/' : '')}/booking/${(this.company.session) ? 'type' : 'service'}`;
        let queryParams = {
            company_id: this.companyId,
            branch_id: this.branchId,
        };
        if (AFTER_PURCHASING_PRODUCT) {
            const data = JSON.parse(AFTER_PURCHASING_PRODUCT);
            if (data['action'] == 'redirect') {
                pathname = data['pathname'];
                queryParams = data['queryParams'];
            }
        }

        this.router.navigate([pathname], {
            queryParams: queryParams
        });
    }

    displayTransactionResult() {
        this.apiService
            .getSelfUser()
            .pipe(take(1))
            .subscribe((response) => {
                if (response['status'] === 'success') {
                    this.apiService.user = response['data'];
                }
            });
        setTimeout(() => {
            this.setActualPaymentStatus(this.displayContent.TOTAL);
        },2000);
    }

    ngOnDestroy() {
        localStorage.removeItem('AFTER_PURCHASING_PRODUCT');
        this.purchaseService.actualPaymentStatus.emit(-1);
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
