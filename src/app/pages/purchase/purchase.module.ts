import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { PurchaseComponent } from './purchase.component';
import { PurchaseRoutingModule } from './purchase-routing.module';
import {PurchaseConfirmComponent} from './components/purchase-confirm/purchase-confirm.component';
import {PurchaseProductsComponent} from './components/purchase-product/purchase-products.component';
import {PurchaseProductDetailsComponent} from './components/purchase-product-details/purchase-product-details.component';

@NgModule({
    declarations: [
        PurchaseComponent,
        PurchaseProductsComponent,
        PurchaseConfirmComponent,
        PurchaseProductDetailsComponent
    ],
    imports: [
        CommonModule,
        TranslateModule,
        SharedModule,
        PurchaseRoutingModule
    ],
    exports: [
        PurchaseComponent,
        PurchaseProductsComponent,
        PurchaseConfirmComponent,
        PurchaseProductDetailsComponent
    ]
})
export class PurchaseModule { }
