import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {CompanyService} from '../../core/service/company.service';
import {productType} from '../../shared/constants/product';
import {PurchaseService} from '../../core/service/purchase.service';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';
import {ApiService} from '../../core/service/api.service';
import {Location} from '@angular/common';
import {AppAuthService} from '../../core/service/auth.service';
import {Menubar} from 'primeng/menubar';
import {BookingService} from '../../core/service/booking.service';

@Component({
    selector: 'app-purchase',
    templateUrl: './purchase.component.html',
    styleUrls: ['./purchase.component.scss']
})
export class PurchaseComponent implements OnInit, OnDestroy {
    company;
    companyId: number;
    branchId: number;
    type: number;
    productId: number;
    targetId: number;

    Object = Object;
    productType = productType;

    isOpenDetailsModal = false;
    detailsModalData: Object;

    isSelectedItem = false;
    selectedItemData: Object;

    isWidget: boolean = false;
    displayContent = {
        CHOICE: -1,
        PENDING: 0,
        PROCESSING: 1,
        PAYMENT_SUCCESS: 2,
        PAYMENT_ERROR: 3,
        TOTAL: 4
    };

    actualPaymentStatus: any = this.displayContent.CHOICE;

    private unsubscribe: Subject<any> = new Subject();
    constructor(private route: ActivatedRoute,
                private router: Router,
                private companyService: CompanyService,
                private purchaseService: PurchaseService,
                private apiService: ApiService,
                private location: Location,
                private authService: AppAuthService,
                private bookingService: BookingService,
                private cdr: ChangeDetectorRef) {
        this.isWidget = this.bookingService.isWidget;
    }

    ngOnInit() {
        const { company_id, branch_id, type, product_id, target_id, action } = this.route.snapshot.queryParams;
        this.companyId = Number(company_id);
        this.branchId = Number(branch_id);
        this.productId = Number(product_id);
        this.targetId = Number(target_id);
        this.type = Number(type);

        if (isNaN(this.companyId) || isNaN(this.branchId) || isNaN(this.type)) {
            // Установить заглушку 404;
            console.log('error: Missing required parameters.');
        }

        if (action === 'AFTER_LOGIN_PURCHASE') {
            window.localStorage.removeItem('AFTER_LOGIN_PURCHASE');
        }

        this.companyService.getById(this.companyId, { branchId: this.branchId}).subscribe((response) => {
           if (response['status'] == 'success') {
               this.company = response['data'];
               this.purchaseService.setPurchaseStatus(Boolean(this.company.online_payment));
               if (this.company['branches'].length) {
                   let products = {};
                   switch (this.type) {
                       case productType.SUBSCRIPTIONS:
                           products = this.companyService.getSubscriptions(this.branchId, this.targetId);
                           break;
                       case productType.COURSE:
                           products = this.companyService.getServicePackages(this.branchId);
                           break;
                       default:
                           break;
                   }

                   this.purchaseService.setProductList(products);

                   if (this.targetId) {
                       const branch = this.company['branches'].find(row => row.id == this.branchId);

                       if (branch) {
                           let target = null;

                           if ('groups' in branch) {
                               target = branch['groups'].find(i => i.id == this.targetId) || branch['groups_personal'].find(i => i.id == this.targetId);
                           } else if ('services' in branch) {
                               target = branch['services'].find(i => i.id == this.targetId);
                           }

                           if (target) {
                               this.purchaseService.setTargetProduct(target);
                           }
                       }
                   }

                   if (Object.keys(products).length) {
                       if (this.productId) {
                           const foundedProduct = Object.values(products).find(item => item['id']== this.productId);
                           if (foundedProduct) {
                               this.purchaseService.setSelectedItem(foundedProduct);
                               this.purchaseService.isSelectedItem.next(true);
                           }
                       }
                       this.purchaseService.isInit.next(true);
                   }
               }
           }
        });

        this.purchaseService.isOpenDetailsModal.pipe(takeUntil(this.unsubscribe)).subscribe((isOpen: any) => {
            this.isOpenDetailsModal = isOpen;
            this.detailsModalData = this.purchaseService.getDetailsModalData();
        });

        this.purchaseService.isSelectedItem.pipe(takeUntil(this.unsubscribe)).subscribe((isSelected: any) => {
            this.isSelectedItem = isSelected;
            if (this.isSelectedItem) {
                const productId = this.purchaseService.getSelectedItemId();
                if (productId && this.purchaseService.isInit.getValue()) {
                    const queryParams = { ...this.route.snapshot.queryParams, product_id: productId };
                    const redirectLink = `${this.isWidget ? 'widgets/booking/' : ''}purchase/confirm`;

                    if (!this.apiService.user['id']) {
                        const afterLoginInfo = {
                            action: 'redirect',
                            pathname: redirectLink,
                            queryParams: queryParams,
                        };
                        window.localStorage.setItem('AFTER_LOGIN_ACTION', JSON.stringify(afterLoginInfo));
                        this.authService.startAuthEvent.emit(true);
                    } else {
                        this.router.navigate(
                            [redirectLink],
                            { queryParams });
                    }
                }
            }
        });

        this.purchaseService.actualPaymentStatus.pipe(takeUntil(this.unsubscribe)).subscribe((status: any) => {
            this.actualPaymentStatus = status;
            this.cdr.detectChanges();
        });
    }

    closeDetailsModal() {
        this.purchaseService.isOpenDetailsModal.next(false);
    }

    moveBack() {
        let redirectLink = `card/${this.companyId}/${this.branchId}`;
        let extras: any = {
            queryParams: {}
        };
        switch (this.actualPaymentStatus) {
            case 0:
                redirectLink = 'products';
                extras = {
                    relativeTo: this.route,
                    queryParams: {
                        company_id: this.companyId,
                        branch_id: this.branchId,
                        type: this.type,
                    }
                };
                break;
            default:
                break;
        }
        this.router.navigate([redirectLink], extras);
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
