import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HeaderModule } from '../header/header.module';
import { PopularContentComponent } from "../../shared/popular-content/popular-content.component";
import { RecommendedContentComponent } from "../../shared/recommended-content/recommended-content.component";

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    SharedModule,
    HeaderModule
  ],
  exports: [HomeComponent]
})
export class HomeModule { }
