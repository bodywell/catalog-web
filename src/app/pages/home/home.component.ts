import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { ApiService } from 'src/app/core/service/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationService } from "../../core/service/location.service";
import { Subject, timer} from "rxjs/index";
import { takeUntil } from "rxjs/internal/operators";
import {animate, style, transition, trigger} from '@angular/animations';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
    animations: [
        trigger('carouselAnimation', [
            transition('void => *', [
                style({ opacity: 0 }),
                animate('1200ms', style({ opacity: 1 }))
            ]),
            transition('* => void', [
                animate('1200ms', style({ opacity: 0 }))
            ])
        ])
    ]
})
export class HomeComponent implements OnInit {

  industries;
  showAllIndustries = false;
  mobile = false;
  openBeauty = false;
  openHealth = false;
  openSport = false;
  openEducation = false;
  backgroundImg = 1;
  autoPlayingPictures = false;

  public currentSlide = 1;
  public slides = [
      'url(assets/img/BG1.png)',
      'url(assets/img/BG2.png)',
      'url(assets/img/BG3.png)',
      'url(assets/img/BG4.png)',
  ];
  public mobileSlides = [
      'url(assets/img/BG-mobile-1.png)',
      'url(assets/img/BG-mobile-2.png)',
      'url(assets/img/BG-mobile-3.png)',
      'url(assets/img/BG-mobile-4.png)',
  ];

  unsubscribe: Subject<any> = new Subject<any>();

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      public apiService: ApiService,
      public locationService: LocationService
  ) {
    if (window.innerWidth <= 1024) {
      this.mobile = true;
    }
  }

  ngOnInit() {
    this.apiService.getIndustries().subscribe(data => {
      if (data['status'] === 'success') {
        this.industries = data['data'];
      }
    });
    this.backgroundImg = this.randomInteger(1, 4);
    this.startPlayingBgPictures();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth <= 1024) {
      this.mobile = true;
    } else {
      this.mobile = false;
    }
  }
  randomInteger(min, max) {
    const rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
  }
  search(data) {
    this.router.navigate([`/search-result`], { queryParams: {
                                                              search: data['search'],
                                                              type: data['type'],
                                                              radius: data['radius'],
                                                              longitude: data['longitude'],
                                                              latitude: data['latitude'],
                                                              industries: data['industries'],
                                                              offset: data['offset'],
                                                              address: data['address'],
                                                            }
                                              });
  }
  searchIndustries(data) {
    this.router.navigate([`/search-result`], { queryParams: {
                                                              search: '',
                                                              industries: data,
                                                              offset: 0,
                                                              city: this.locationService.selectedLocation.city,
                                                              country: this.locationService.selectedLocation.country
                                                            }
                                              });
  }

  startPlayingBgPictures() {
      timer(0, 2000)
          .pipe(
              takeUntil(this.unsubscribe),
          ).subscribe(() => {
          if (this.currentSlide !== 4) {
              this.currentSlide++;
          } else {
              this.currentSlide = 1;
          }
      });
  }

  ngOnDestroy() {
     this.unsubscribe.next(null);
     this.unsubscribe.complete();
  }

}
