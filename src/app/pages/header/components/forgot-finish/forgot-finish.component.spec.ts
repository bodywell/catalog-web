import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotFinishComponent } from './forgot-finish.component';

describe('ForgotFinishComponent', () => {
  let component: ForgotFinishComponent;
  let fixture: ComponentFixture<ForgotFinishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgotFinishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotFinishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
