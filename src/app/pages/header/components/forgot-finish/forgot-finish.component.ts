import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from 'src/app/core/service/api.service';
import { NotificationMessageService } from 'src/app/core/service/notification.service';

enum Steps {
  general = 1,
  confEmail = 2,
}
@Component({
  selector: 'app-forgot-finish',
  templateUrl: './forgot-finish.component.html',
  styleUrls: ['./forgot-finish.component.scss'],
})
export class ForgotFinishComponent implements OnInit {
  // Событие закрытия модального окна для родительского компонента
  @Output() closeModalEvent = new EventEmitter<Boolean>();
  // Событие открытия модального окна регистрации для родительского компонента
  @Output() showLoginWithEmailModalEvent = new EventEmitter<Boolean>();

  @Input() code;

  currentStep: any;
  Steps: typeof Steps = Steps;
  password = '';
  repassword = '';

  passwordInput = new FormControl('');
  repasswordInput = new FormControl('');
  constructor(
    private apiService: ApiService,
    private notificationMessageService: NotificationMessageService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.currentStep = Steps.general;
  }

  // Закрытие модального окна
  closeModal() {
    this.closeModalEvent.emit(true);
  }

  nextStep(step) {
    this.currentStep = step;
  }

  goToGeneralStep() {
    this.currentStep = Steps.general;
  }

  blurPassword() {
    if (
      !this.password ||
      this.password.length < 6 ||
      !/^(?=.*\d)(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?!.*\s).*$/.test(this.password)
    ) {
      this.passwordInput.setErrors({ passwordError: true });
      return false;
    } else {
      this.passwordInput.setErrors({ passwordError: false });
      return true;
    }
  }

  blurRepassword() {
    if (!this.repassword || !this.repassword || this.password !== this.repassword) {
      this.repasswordInput.setErrors({ passwordError: true });
      return false;
    } else {
      this.repasswordInput.setErrors({ passwordError: false });
      return true;
    }
  }

  forgot() {
    const passwordValidate = this.blurPassword();
    const repasswordValidate = this.blurRepassword();

    if (!passwordValidate || !repasswordValidate) {
      return;
    }
    this.apiService.forgotFinish(this.code, this.password).subscribe((response) => {
      if (response['status'] === 'success') {
        this.notificationMessageService.pushAlert({
          detail: this.translate.instant('notification-success-password-change'),
        });
        this.closeModal();
      } else {
        response['errors'].forEach((error) => {
          this.notificationMessageService.pushAlert({
            detail: this.translate.instant(
              `notification-restore-password-error-${error['code'] ? error['code'] : 'unknown'}`
            ),
          });
        });
      }
    });
  }
}
