import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ApiService } from 'src/app/core/service/api.service';
import { CookieService } from 'ngx-cookie-service';
import { LocationService } from '../../../../core/service/location.service';

@Component({
  selector: 'app-select-city-modal',
  templateUrl: './select-city-modal.component.html',
  styleUrls: ['./select-city-modal.component.scss'],
})
export class SelectCityModalComponent implements OnInit {
  public selectedCountry = null;

  // Событие закрытия модального окна для родительского компонента
  @Output() closeModalEvent = new EventEmitter<Boolean>();
  locations;

  constructor(public locationService: LocationService) {}

  ngOnInit() {
    this.locations = this.locationService.locations.filter(location => {
      return location.country !== 'Russia';
    });
  }

  closeModal() {
    this.selectedCountry = null;
    window.scroll(0, 0);
    this.closeModalEvent.emit(true);
  }

  public getCurrentPosition(): void {
    window.localStorage.removeItem('location');
    this.locationService.getCurrentPosition().then((_) => {
      this.closeModal();
    });
  }

  public searchEverywhere(): void {
    this.selectLocation();
  }

  selectLocation(city = null, country = null, countryCode = null) {
    this.locationService.selectLocation(city, country, countryCode);
    this.closeModal();
  }

  toggleCoutry(country) {
    if (this.selectedCountry === country) {
      this.selectedCountry = null;
    } else {
      this.selectedCountry = country;
    }
  }
}
