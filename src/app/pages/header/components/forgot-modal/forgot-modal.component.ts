import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { of, Subject } from 'rxjs';
import { mergeMap, tap, takeUntil, take } from 'rxjs/operators';
import { ApiService } from 'src/app/core/service/api.service';
import { commonDataService } from 'src/app/core/service/commonData.service';
import { LocationService } from 'src/app/core/service/location.service';
import { NotificationMessageService } from 'src/app/core/service/notification.service';
import * as moment from 'moment';

enum Steps {
  general = 1,
  confEmail = 2,
}

@Component({
  selector: 'app-forgot-modal',
  templateUrl: './forgot-modal.component.html',
  styleUrls: ['./forgot-modal.component.scss'],
})
export class ForgotModalComponent implements OnInit, OnDestroy {
  // Событие закрытия модального окна для родительского компонента
  @Output() closeModalEvent = new EventEmitter<Boolean>();
  // Событие открытия модального окна регистрации для родительского компонента
  @Output() showLoginWithEmailModalEvent = new EventEmitter<Boolean>();

  currentStep: any = 1;
  Steps: typeof Steps = Steps;

  public forgotForm: FormGroup;
  public phoneCodes = [];
  public code = '';
  public codeInput = new FormControl('');
  public passwordInput = new FormControl('');
  public repasswordInput = new FormControl('');

  private timerInterval;
  public resendTimer = 0;
  public isResend = false;

  public forgotInfo = {};

  private unsubscribe: Subject<any> = new Subject();

  constructor(
    private apiService: ApiService,
    private locationService: LocationService,
    private commonDataService: commonDataService,
    private fb: FormBuilder,
    private notificationMessageService: NotificationMessageService,
    private translate: TranslateService
  ) {
    this.forgotForm = this.fb.group({
      phoneCode: [
        {
          code: null,
          phone_code: null,
        },
      ],
      phoneNumber: [null, [Validators.minLength(8), Validators.maxLength(22), Validators.required]],
    });
  }

  ngOnInit() {
    // this.currentStep = Steps.general;

    this.locationService.userIpDefined
      .pipe(
        mergeMap((status) => {
          if (status !== null) {
            return this.commonDataService.getPhoneCodes().pipe(
              tap((phoneCodes) => {
                this.setPhoneData(phoneCodes);
              })
            );
          }

          return of(status);
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe();
  }

  private setPhoneData(phoneCodes): void {
    this.phoneCodes = [].concat(phoneCodes);

    const phoneCode = phoneCodes.find(
      (i) => i.value.code === String(this.locationService.userIpData.code).toLowerCase()
    );

    this.forgotForm.patchValue({
      phoneCode: {
        code: String(phoneCode.value.code).toLowerCase(),
        phone_code: phoneCode.value.phone_code,
      },
    });
  }

  // Закрытие модального окна
  closeModal() {
    this.closeModalEvent.emit(true);
  }

  nextStep(step) {
    this.currentStep = step;
  }

  goToGeneralStep() {
    this.currentStep = Steps.general;
  }

  goToLoginWithEmail() {
    this.closeModalEvent.emit(true);
    this.showLoginWithEmailModalEvent.emit(true);
  }

  blurPassword() {
    if (
      !this.forgotInfo['password'] ||
      this.forgotInfo['password'].length < 6 ||
      !/^(?=.*\d)(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?!.*\s).*$/.test(this.forgotInfo['password'])
    ) {
      this.passwordInput.setErrors({ passwordError: true });
      return false;
    } else {
      this.passwordInput.setErrors({ passwordError: false });
      return true;
    }
  }

  blurRepassword() {
    if (
      !this.forgotInfo['repassword'] ||
      !this.forgotInfo['password'] ||
      this.forgotInfo['password'] != this.forgotInfo['repassword']
    ) {
      this.repasswordInput.setErrors({ passwordError: true });
      return false;
    } else {
      this.repasswordInput.setErrors({ passwordError: false });
      return true;
    }
  }

  blurCode() {
    if (this.code == '') {
      this.codeInput.setErrors({ required: true });
      return false;
    } else {
      this.codeInput.setErrors({ required: false });
      return true;
    }
  }

  blurPhone() {
    if (
      !this.forgotForm.value.phoneNumber ||
      this.forgotForm.value.phoneNumber.length < 8 ||
      this.forgotForm.value.phoneNumber.length > 15
    ) {
      this.forgotForm.controls.phoneNumber.setErrors({ errorPhone: true });
      return false;
    } else {
      this.forgotForm.controls.phoneNumber.setErrors({ errorPhone: false });
      return true;
    }
  }

  sanitizePhoneNumber() {
    const phoneNumber = this.forgotForm.get('phoneNumber');
    const phoneCode = this.forgotForm.get('phoneCode');
    if (phoneNumber) {
      let sanitizedValue = phoneNumber.value.replace(/\D/g, '');
      if (sanitizedValue.startsWith(phoneCode.value.phone_code)) {
        sanitizedValue = sanitizedValue.slice(phoneCode.value.phone_code.toString().length);
      }
      phoneNumber.setValue(sanitizedValue, { emitEvent: false });
    }
  }

  forgot() {
    if (!this.blurPhone()) {
      return;
    }

    this.apiService
      .forgot(this.forgotForm.value.phoneCode.phone_code, this.forgotForm.value.phoneNumber)
      .pipe(take(1))
      .subscribe((response) => {
        if (response['status'] === 'success') {
          this.nextStep(Steps.confEmail);

          const eventTime = Number(window.localStorage.getItem('resendPasswordTimer'));

          if (!isNaN(eventTime)) {
            const currentTime = moment().unix();
            const diffTime = eventTime - currentTime;
            let duration = moment.duration(diffTime, 'milliseconds');
            this.resendTimer = +duration;

            if (+duration > 0) {
              this.timerInterval = setInterval(() => {
                duration = moment.duration(+duration - 1, 'milliseconds');

                this.resendTimer = +duration;

                if (+duration <= 0) {
                  window.localStorage.removeItem('resendPasswordTimer');
                  clearInterval(this.timerInterval);
                }
              }, 1000);
            } else {
              window.localStorage.removeItem('resendPasswordTimer');
            }
          }
        } else if (response['status'] === 'error') {
          for (let i = 0; i < response['errors'].length; i++) {
            this.forgotForm.controls.phoneNumber.setErrors({ notExist: true });
          }
        }
      });
  }

  public resendConfirmCode(): void {
    if (this.resendTimer > 0) {
      return;
    }

    this.apiService
      .forgot(this.forgotForm.value.phoneCode.phone_code, this.forgotForm.value.phoneNumber)
      .pipe(take(1))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          const eventTime = moment().add(3, 'minutes').unix();
          const currentTime = moment().unix();
          const diffTime = eventTime - currentTime;
          let duration = moment.duration(diffTime, 'milliseconds');

          this.resendTimer = +duration;
          this.isResend = true;

          window.localStorage.setItem('resendPasswordTimer', String(eventTime));

          this.timerInterval = setInterval(() => {
            duration = moment.duration(+duration - 1, 'milliseconds');

            this.resendTimer = +duration;

            if (+duration <= 0) {
              window.localStorage.removeItem('resendPasswordTimer');
              clearInterval(this.timerInterval);
            }
          }, 1000);

          this.notificationMessageService.pushAlert({ detail: this.translate.instant('notification-code-resended') });
        } else {
          res['errors'].forEach((error) => {
            if (error.code === 6) {
              this.notificationMessageService.pushAlert({
                detail: this.translate.instant('notification-activate-code-already-used'),
              });
            }
          });
        }
      });
  }

  forgotChange() {
    if (!this.blurCode || !this.blurPassword || !this.blurRepassword) {
      return;
    }

    this.apiService.forgotFinish(this.code, this.forgotInfo['password']).subscribe((response) => {
      if (response['status'] === 'success') {
        this.notificationMessageService.pushAlert({
          detail: this.translate.instant('notification-success-password-change'),
        });
        this.closeModal();
      } else {
        response['errors'].forEach((error) => {
          this.notificationMessageService.pushAlert({
            detail: this.translate.instant(
              `notification-restore-password-error-${error['code'] ? error['code'] : 'unknown'}`
            ),
          });
        });
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
