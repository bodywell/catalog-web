import {Component, HostListener, OnInit} from '@angular/core';

@Component({
    selector: 'app-up-button',
    templateUrl: './up-button.component.html',
    styleUrls: ['./up-button.component.scss']
})

export class UpButtonComponent implements OnInit {

    visible = false;

    constructor() {}

    ngOnInit() {}

    goUp() {
        scroll(0, 0);
    }

    @HostListener('window:scroll', ['$event']) onScrollEvent($event) {
        if (window.scrollY >= (window.innerHeight / 1.5)) {
            this.visible = true;
        } else {
            this.visible = false;
        }
    }
}