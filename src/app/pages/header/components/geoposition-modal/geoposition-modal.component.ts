import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ApiService } from 'src/app/core/service/api.service';
import { CookieService } from 'ngx-cookie-service';
import { AuthService, FacebookLoginProvider } from 'angularx-social-login';
import { AppAuthService } from 'src/app/core/service/auth.service';
import { countries } from '../../../../shared/select/countries';
import { take, takeUntil } from 'rxjs/internal/operators';
import { ReplaySubject } from 'rxjs/index';
import { GapiCalendarService } from '../../../../core/service/gapi.calendar.service';
import { LocationService } from 'src/app/core/service/location.service';

enum Steps {
  general = 1,
  loginWithEmail = 2,
  phoneNumber = 3,
}

@Component({
  selector: 'app-geoposition-modal',
  templateUrl: './geoposition-modal.component.html',
  styleUrls: ['./geoposition-modal.component.scss'],
})
export class GeopositionModalComponent implements OnInit, OnDestroy {
  @Output() closeModalEvent = new EventEmitter<Boolean>();

  @Input() geopositionStatus: string;

  destroy: ReplaySubject<any> = new ReplaySubject<any>(1);

  constructor(private locationService: LocationService) {}

  ngOnInit() {}

  ngOnDestroy() {
    this.destroy.next(null);
    this.destroy.complete();
  }

  // Закрытие модального окна
  closeModal() {
    this.closeModalEvent.emit(true);
  }
}
