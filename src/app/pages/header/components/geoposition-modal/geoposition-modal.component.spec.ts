import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeopositionModalComponent } from './geoposition-modal.component';

describe('GeopositionModalComponent', () => {
  let component: GeopositionModalComponent;
  let fixture: ComponentFixture<GeopositionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeopositionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeopositionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
