/// <reference types="@types/googlemaps" />

import {Component, OnInit, Input, ViewChild, Output, EventEmitter, AfterViewInit, DoCheck, HostListener} from '@angular/core';
import { ApiService } from 'src/app/core/service/api.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {LocationService} from "../../../../core/service/location.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateService} from "@ngx-translate/core";
import {filter} from "rxjs/internal/operators";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, AfterViewInit, DoCheck {

  @Input() mode;
  @Input() adressType: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @Output() search: EventEmitter<any> = new EventEmitter();

  @ViewChild('searchWrapper', {static: false}) searchWrapper: any;
  @ViewChild('addresstext', {static: false}) addresstext: any;
  autocompleteInput: string;
  queryWait: boolean;
  searchText = '';
  locations = [];
  selectedLocation = [];

  isShowMenu = false;
  isShowLocationMenu = false;
  lat;
  lng;
  selectedType = 2;
  selectedTypeText = 'search-services';
  autocompleteData = [];
  autocompleteItems;
  isShowAutocomplete = false;
  autocomplateLang;

  constructor(
      private route: ActivatedRoute,
      public router: Router,
      private apiService: ApiService,
      public locationService: LocationService,
      public http: HttpClient,
      public translate: TranslateService
  ) {
    this.route.queryParams.subscribe(params => {
      if (params['search']) {
        this.searchText = params['search'];
      }
      if (params['type']) {
        this.selectedType = params['type'];
      }
      if (params['longitude']) {
        this.lng = params['longitude'];
      }

      if (params['latitude']) {
        this.lat = params['latitude'];
      }

      if (params['city']) {
        this.selectedLocation['city'] = params['city'];
      }

      if (params['country']) {
        this.selectedLocation['country'] = params['country'];
      }

      if (params['address']) {
        this.autocompleteInput = params['address'];
      }

      if (this.selectedType == 1) {
        this.selectedTypeText = 'search-businesses';
      } else if (this.selectedType == 2) {
        this.selectedTypeText = 'search-services';
      } else if (this.selectedType == 3) {
        this.selectedTypeText = 'search-specialists';
      }

    });
   }

  @HostListener('document:click', ['$event'])
  _hideModal(e) {
    if (this.searchWrapper) {
      const targetElementPath = (e.composedPath && e.composedPath()) || e.path;
      const elementRefInPath = targetElementPath.find((path) => path === this.searchWrapper.nativeElement);
      
      if (this.isShowAutocomplete && !elementRefInPath) {
        this.isShowAutocomplete = false;
      }
    }
  }

  ngOnInit() {
    //TODO: Сделано временно для статических локаций.
    /*this.apiService.getCompanyLocations().subscribe(data => {
      if (data['status'] === 'success') {
        this.locations = data['data'];
      }
    });*/
    this.getAutocomplateJson();
  }

  ngDoCheck() {
    if (this.autocomplateLang != this.translate.currentLang) {
      this.getAutocomplateJson();
    }
  }

  getAutocomplateJson() {
      this.autocomplateLang = this.translate.currentLang;
      this.http.get(
          'https://language.bodywell.io/get?Source=BodywellAutocompleteDatabase&Language=' +
          this.autocomplateLang)
          .subscribe((res) => {
              this.autocompleteData = [];
              Object.keys(res).forEach((key) => {
                this.autocompleteData.push(res[key]);
              });
              if (this.searchText) {
                this.autocomplete();
              }
          });
  }

  autocomplete() {
    if (this.searchText != '') {
        this.autocompleteItems = this.autocompleteData.filter(val => (val.toLowerCase()).indexOf(this.searchText.toLowerCase()) != -1).slice(0, 7);

    if (this.autocompleteItems != '' && this.autocompleteItems.length > 0) {
      this.autocompleteItems = [this.searchText, ...this.autocompleteItems];
        this.isShowAutocomplete = true;
    }
    } else {
      this.isShowAutocomplete = false;
    }
  }
  selectAutocompleteItem(item) {
    this.searchText = item;
    this.isShowAutocomplete = false;
  }

  ngAfterViewInit() {
    //this.getPlaceAutocomplete();
  }
  private getPlaceAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
    {
        types: ['geocode']
    });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
        const place = autocomplete.getPlace();
        this.autocompleteInput = place['formatted_address'];
        this.invokeEvent(place);
    });
  }

  invokeEvent(place: Object) {
    this.lat = place['geometry']['location'].lat();
    this.lng = place['geometry']['location'].lng();
    this.setAddress.emit(place);
  }
  selectItemMenu(type) {
    this.selectedType = type;
    if (this.selectedType === 1) {
      this.selectedTypeText = 'search-businesses';
    } else if (this.selectedType === 2) {
      this.selectedTypeText = 'search-services';
    } else if (this.selectedType === 3) {
      this.selectedTypeText = 'search-specialists';
    }
  }

  selectLocationItemMenu(city, country) {
    this.selectedLocation['city'] = city;
    this.selectedLocation['country'] = country;
  }
  toggleMenu() {
    this.isShowMenu = !this.isShowMenu;
  }
  toggleLocationMenu() {
    this.isShowLocationMenu = !this.isShowLocationMenu;
  }
  searchClick() {
    // this.search.emit({
    //   type: this.selectedType,
    //   search: this.searchText,
    //   latitude: this.lat,
    //   longitude: this.lng,
    //   radius: 200,
    //   industries: '',
    //   offset: 0,
    //   address: this.autocompleteInput
    // });

    this.router.navigate([`/search-result`], { queryParams: {
        type: 0,
        search: this.searchText,
        // latitude: this.lat,
        // longitude: this.lng,
        // radius: 200,
        industries: '',
        offset: 0,
        // address: this.autocompleteInput,
        city: this.locationService.selectedLocation['city'],
        country: this.locationService.selectedLocation['country']
      }
    });
  }
}
