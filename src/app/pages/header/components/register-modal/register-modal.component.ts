import { Component, OnInit, Output, EventEmitter, ViewEncapsulation, OnDestroy } from '@angular/core';
import { countries } from '../../../../shared/select/countries';
import { ApiService } from 'src/app/core/service/api.service';
import { TranslateService } from '@ngx-translate/core';
import { FormControl } from '@angular/forms';
import { LocationService } from '../../../../core/service/location.service';
import { mergeMap, take, takeUntil, tap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { Observable, of, Subject } from 'rxjs';
import { GapiCalendarService } from 'src/app/core/service/gapi.calendar.service';
import { AppAuthService } from 'src/app/core/service/auth.service';
import { commonDataService } from 'src/app/core/service/commonData.service';
import { NotificationMessageService } from 'src/app/core/service/notification.service';
import * as moment from 'moment';

enum Steps {
  general = 1,
  confEmail = 2,
}

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.scss'],
})
export class RegisterModalComponent implements OnInit, OnDestroy {
  // Событие закрытия модального окна для родительского компонента
  @Output() closeModalEvent = new EventEmitter<Boolean>();
  // Событие открытия модального окна регистрации для родительского компонента
  @Output() showLoginWithEmailModalEvent = new EventEmitter<Boolean>();

  currentStep: any;
  Steps: typeof Steps = Steps;
  countries = countries;
  selectedCounries;
  iAgree = false;
  registerInfo = {};
  phoneCode = {
    code: '',
    phone_code: null,
  };
  code = '';
  public phoneCodes = [];

  nameInput = new FormControl('');
  surnameInput = new FormControl('');
  phoneInput = new FormControl('');
  emailInput = new FormControl('');
  passwordInput = new FormControl('');
  iAgreeInput = new FormControl('');
  codeInput = new FormControl('');

  defaultLocation;

  private unsubscribe: Subject<any> = new Subject();

  constructor(
    private apiService: ApiService,
    public translate: TranslateService,
    public locationService: LocationService,
    public calendar: GapiCalendarService,
    private appAuthService: AppAuthService,
    private commonDataService: commonDataService,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {
    this.currentStep = Steps.general;

    this.locationService.userIpDefined
      .pipe(
        mergeMap((status) => {
          if (status !== null) {
            return this.commonDataService.getPhoneCodes().pipe(
              tap((phoneCodes) => {
                this.setPhoneData(phoneCodes);
              })
            );
          }

          return of(status);
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe();
  }

  private setPhoneData(phoneCodes): void {
    this.phoneCodes = [].concat(phoneCodes);

    const phoneCode = phoneCodes.find(
      (i) => i.value.code === String(this.locationService.userIpData.code).toLowerCase()
    );

    this.phoneCode = {
      code: String(phoneCode.value.code).toLowerCase(),
      phone_code: phoneCode.value.phone_code,
    };
  }

  // Закрытие модального окна
  closeModal() {
    this.closeModalEvent.emit(true);
  }

  nextStep(step) {
    this.currentStep = step;
  }

  goToGeneralStep() {
    this.currentStep = Steps.general;
  }

  goToLoginWithEmail() {
    this.closeModalEvent.emit(true);
    this.showLoginWithEmailModalEvent.emit(true);
  }

  blurFirstName() {
    if (!this.registerInfo['first_name'] || this.registerInfo['first_name'].length === 0) {
      this.nameInput.setErrors({ required: true });
      return false;
    } else {
      this.nameInput.setErrors({ required: false });
      return true;
    }
  }

  blurLastName() {
    if (!this.registerInfo['last_name'] || this.registerInfo['last_name'].length === 0) {
      this.surnameInput.setErrors({ required: true });
      return false;
    } else {
      this.surnameInput.setErrors({ required: false });
      return true;
    }
  }

  blurEmail() {
    if (this.registerInfo['email'] && !this.validateEmail(this.registerInfo['email'])) {
      this.emailInput.setErrors({ errorEmail: true });
      return false;
    } else {
      this.emailInput.setErrors({ errorEmail: false });
      return true;
    }
  }

  blurPhone() {
    if (
      !this.registerInfo['phone_number'] ||
      this.registerInfo['phone_number'].length < 8 ||
      this.registerInfo['phone_number'].length > 15
    ) {
      this.phoneInput.setErrors({ errorPhone: true });
      return false;
    } else {
      this.phoneInput.setErrors({ errorPhone: false });
      return true;
    }
  }

  blurPassword() {
    if (
      !this.registerInfo['password'] ||
      this.registerInfo['password'].length < 6 ||
      !/^(?=.*\d)(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?!.*\s).*$/.test(this.registerInfo['password'])
    ) {
      this.passwordInput.setErrors({ passwordError: true });
      return false;
    } else {
      this.passwordInput.setErrors({ passwordError: false });
      return true;
    }
  }

  blurCode() {
    if (this.code == '') {
      this.codeInput.setErrors({ required: true });
      return false;
    } else {
      this.codeInput.setErrors({ required: false });
      return true;
    }
  }

  validateEmail(email) {
    const re =
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  allValidate() {
    const email = this.blurEmail();
    const name = this.blurFirstName();
    const surname = this.blurLastName();
    const password = this.blurPassword();
    const phone = this.blurPhone();
    let agree;

    if (this.iAgree !== true) {
      this.iAgreeInput.setErrors({ error: true });
      agree = false;
    } else {
      this.iAgreeInput.setErrors({ error: false });
      agree = true;
    }
    if (!email || !name || !password || !phone || !agree || !surname) {
      return false;
    }
    return true;
  }

  register() {
    if (!this.allValidate()) {
      return false;
    }
    this.emailInput.setErrors({ exist: false });
    this.phoneInput.setErrors({ exist: false });

    this.registerInfo['phone_country'] = this.phoneCode.code;
    this.registerInfo['phone_code'] = this.phoneCode.phone_code;
    this.registerInfo['language'] = this.translate.currentLang.toLowerCase();

    const refRecords = window.localStorage.getItem('ref');
    if (refRecords) {
      this.registerInfo['invite_code'] = this.appAuthService.getInvites(refRecords);
    }

    this.apiService
      .register(this.registerInfo)
      .pipe(
        mergeMap((res) => {
          if (res['status'] === 'success') {
            const eventTime = moment().add(3, 'minutes').unix();
            window.localStorage.setItem('resendEmailTimer', String(eventTime));
            return this.login();
          } else if (res['status'] === 'error') {
            for (let i = 0; i < res['errors'].length; i++) {
              if (res['errors'][i]['code'] === 4) {
                if (res['errors'][i]['description'] === 'Phone already exist!') {
                  this.phoneInput.setErrors({ exist: true });
                } else {
                  this.emailInput.setErrors({ exist: true });
                }
              } else if (res['errors'][i]['code'] === 5) {
                this.passwordInput.setErrors({ passwordError: true });
              } else if (res['errors'][i]['code'] === 12) {
                this.notificationMessageService.pushAlert({
                  detail: this.translate.instant('notification-error-reg-email-social-exists'),
                });
              }
            }
          }

          return of(res);
        }),
        take(1)
      )
      .subscribe();
  }

  private login(): Observable<any> {
    return this.apiService
      .login({
        phoneCode: this.registerInfo['phone_code'],
        phoneNumber: this.registerInfo['phone_number'],
        password: this.registerInfo['password'],
      })
      .pipe(
        mergeMap((res) => {
          if (res['status'] === 'success') {
            return this.apiService.getSelfUser(res['data']['token']).pipe(
              tap((selfUserResponse) => {
                if (selfUserResponse['status'] === 'success') {
                  this.apiService.user = selfUserResponse['data'];

                  let syncEmail = selfUserResponse['data']['calendar_sync_email']
                    ? selfUserResponse['data']['calendar_sync_email']
                    : '';

                  this.calendar.syncEmail.next(syncEmail);

                  window.localStorage.setItem('token', res['data']['token']);
                  this.apiService.token = res['data']['token'];
                  this.closeModal();

                  if (!selfUserResponse['data']['is_phone']) {
                    this.appAuthService.isEmailConfirm.next(false);
                  } else {
                    this.appAuthService.finishRegEvent.emit(true);
                  }
                }
              })
            );
          }

          return of(res);
        }),
        take(1)
      );
  }

  sendCode() {
    this.codeInput.setErrors({ invalid: false });
    if (!this.blurCode()) {
      return;
    }
    this.apiService
      .approveRegister(this.registerInfo['email'], this.code)
      .pipe(take(1))
      .subscribe((data) => {
        if (data['status'] == 'success') {
          this.goToLoginWithEmail();
        } else if (data['status'] == 'error') {
          this.codeInput.setErrors({ invalid: true });
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
