import { Component, OnInit, Output, EventEmitter, Input, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/core/service/api.service';
import { CookieService } from 'ngx-cookie-service';
import { AuthService, FacebookLoginProvider } from 'angularx-social-login';
import { AppAuthService } from 'src/app/core/service/auth.service';
import { countries } from '../../../../shared/select/countries';
import { mergeMap, take, takeUntil, tap } from 'rxjs/internal/operators';
import { Observable, of, ReplaySubject } from 'rxjs/index';
import { GapiCalendarService } from '../../../../core/service/gapi.calendar.service';
import { NotificationMessageService } from 'src/app/core/service/notification.service';
import { TranslateService } from '@ngx-translate/core';
import { commonDataService } from 'src/app/core/service/commonData.service';
import { LocationService } from 'src/app/core/service/location.service';
import * as moment from 'moment';
import { Router } from '@angular/router';

enum Steps {
  general = 1,
  loginPhone = 2,
  loginPassword = 3,
  phoneNumber = 4,
  loginCreatePassword = 5,
}

@Component({
  selector: 'app-auth-modal',
  templateUrl: './auth-modal.component.html',
  styleUrls: ['./auth-modal.component.scss'],
})
export class AuthModalComponent implements OnInit, OnDestroy {
  public authForm: FormGroup;
  public isSubmitted = false;

  currentStep: any;
  Steps: typeof Steps = Steps;

  password = '';

  public phone = new FormGroup({
    phoneCode: new FormControl(''),
    phoneNumber: new FormControl(''),
  });
  emailInput = new FormControl('');
  passwordInput = new FormControl('');
  countries = countries;
  public phoneCodes = [];

  phoneCode;
  phoneNumber;
  phoneInput = new FormControl('');

  iAgreeInput = new FormControl('');
  iAgree = false;

  private timerInterval;
  public resendTimer = 0;
  public isResend = false;

  isShowMobileNumberModal = false;
  token;

  facebookError = {
    status: 0,
    text: '',
  };

  private queryParams = {};

  // Событие закрытия модального окна для родительского компонента
  @Output() closeModalEvent = new EventEmitter<Boolean>();
  // Событие открытия модального окна регистрации для родительского компонента
  @Output() showRegisterModalEvent = new EventEmitter<Boolean>();
  // Событие открытия модального окна востановления пароля для родительского компонента
  @Output() showForgotModalEvent = new EventEmitter<Boolean>();
  @Output() cancelPhoneAprove = new EventEmitter<Boolean>();

  @Input() initStep;

  destroy: ReplaySubject<any> = new ReplaySubject<any>(1);

  constructor(
    private apiService: ApiService,
    private appAuthService: AppAuthService,
    private calendar: GapiCalendarService,
    private notificationMessageService: NotificationMessageService,
    private translate: TranslateService,
    private commonDataService: commonDataService,
    private locationService: LocationService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.currentStep = this.initStep;
    window.scrollTo(0, 0);

    this.queryParams = window.location.search
      ? window.location.search
          .slice(1)
          .split('&')
          .reduce((acc, param) => {
            let [id, value] = param.split('=');
            acc[id] = decodeURIComponent(value);

            return acc;
          }, {})
      : {};

    this.initForm();

    this.locationService.userIpDefined
      .pipe(
        mergeMap((status) => {
          if (status !== null) {
            return this.commonDataService.getPhoneCodes().pipe(
              tap((phoneCodes) => {
                this.setPhoneData(phoneCodes);
              })
            );
          }

          return of(status);
        }),
        takeUntil(this.destroy)
      )
      .subscribe();
  }

  private initForm(): void {
    this.authForm = this.fb.group({
      phoneCode: [
        {
          code: null,
          phone_code: null,
        },
      ],
      phoneNumber: [null, [Validators.minLength(8), Validators.maxLength(22), Validators.required]],
      password: [null, [Validators.required]],
      password2: [null, [Validators.required]],
      smsCode: [null, [Validators.required]],
      iAgree: [false],
    });
    console.log(this.authForm);
  }

  private setPhoneData(phoneCodes): void {
    this.phoneCodes = [].concat(phoneCodes);

    const phoneCode = phoneCodes.find(
      (i) => i.value.code === String(this.locationService.userIpData.code).toLowerCase()
    );

    this.authForm.patchValue({
      phoneCode: {
        code: this.apiService.user.phone_country
          ? String(this.apiService.user.phone_country).toLowerCase()
          : String(phoneCode.value.code).toLowerCase(),
        phone_code: this.apiService.user.phone_code || phoneCode.value.phone_code,
      },
      phoneNumber: this.apiService.user.phone_number || null,
    });
  }

  // Закрытие модального окна
  closeModal() {
    this.closeModalEvent.emit(true);
  }

  nextStep(step) {
    this.currentStep = step;

    if (step === Steps.loginCreatePassword) {
      let eventTime = Number(window.localStorage.getItem('resendEmailTimer'));

      if (!eventTime) {
        eventTime = moment().add(3, 'minutes').unix();
        window.localStorage.setItem('resendEmailTimer', String(eventTime));
      }

      if (!isNaN(eventTime)) {
        const currentTime = moment().unix();
        const diffTime = eventTime - currentTime;
        let duration = moment.duration(diffTime, 'milliseconds');
        this.resendTimer = +duration;

        if (+duration > 0) {
          this.timerInterval = setInterval(() => {
            duration = moment.duration(+duration - 1, 'milliseconds');

            this.resendTimer = +duration;

            if (+duration <= 0) {
              window.localStorage.removeItem('resendEmailTimer');
              clearInterval(this.timerInterval);
            }
          }, 1000);
        } else {
          window.localStorage.removeItem('resendEmailTimer');
        }
      }
    }
  }

  goToGeneralStep() {
    this.currentStep = Steps.general;
  }

  goToRegister() {
    this.closeModalEvent.emit(true);
    this.showRegisterModalEvent.emit(true);
  }

  goToForgot() {
    this.closeModalEvent.emit(true);
    this.showForgotModalEvent.emit(true);
  }

  blurPhone() {
    if (
      !this.authForm.value.phoneNumber ||
      this.authForm.value.phoneNumber.length < 8 ||
      this.authForm.value.phoneNumber.length > 15
    ) {
      this.authForm.controls.phoneNumber.setErrors({ errorPhone: true });
      return false;
    } else {
      this.authForm.controls.phoneNumber.setErrors({ errorPhone: false });
      return true;
    }
  }

  sanitizePhoneNumber() {
    const phoneNumber = this.authForm.get('phoneNumber');
    const phoneCode = this.authForm.get('phoneCode');
    if (phoneNumber) {
      let sanitizedValue = phoneNumber.value.replace(/\D/g, '');
      if (sanitizedValue.startsWith(phoneCode.value.phone_code)) {
        sanitizedValue = sanitizedValue.slice(phoneCode.value.phone_code.toString().length);
      }
      phoneNumber.setValue(sanitizedValue, { emitEvent: false });
    }
  }

  toRegister() {
    this.closeModalEvent.emit(true);
    this.cancelPhoneAprove.emit(true);
  }

  public sendPhone() {
    if (this.authForm.value.iAgree !== true) {
      this.authForm.controls.iAgree.setErrors({ error: true });
    } else {
      this.authForm.controls.iAgree.setErrors({ error: false });
    }
    if (!this.blurPhone() || !this.authForm.value.iAgree) {
      return false;
    }

    this.apiService.user['phone_country'] = this.authForm.value.phoneCode['code'];
    this.apiService.user['phone_code'] = this.authForm.value.phoneCode['phone_code'];
    this.apiService.user['phone_number'] = this.authForm.value.phoneNumber;

    this.apiService
      .setUser(this.apiService.user)
      .pipe(take(1))
      .subscribe((data) => {
        if (data['status'] === 'success') {
          window.localStorage.setItem('token', this.apiService['token']);
          const eventTime = moment().add(3, 'minutes').unix();
          window.localStorage.setItem('resendEmailTimer', String(eventTime));
          this.resendConfirmCode();
          this.closeModal();
          this.appAuthService.finishAuthEvent.emit(true);
        } else if (data['status'] === 'error') {
          data['errors'].forEach((error) => {
            if (error.code === 6) {
              this.notificationMessageService.pushAlert({
                detail: this.translate.instant('notification-error-phone-number-already-used'),
              });
            }
          });
        }
      });
  }

  public createPasswordAndLogin() {
    this.isSubmitted = true;

    if (!this.authForm.value.password || !this.authForm.value.smsCode) {
      return;
    }

    if (this.authForm.value.password !== this.authForm.value.password2) {
      return;
    }

    this.apiService
      .addPasswordToGoogleAcc(
        this.authForm.value.phoneCode['phone_code'],
        this.authForm.value.phoneNumber,
        this.authForm.value.password,
        this.authForm.value.smsCode
      )
      .pipe(take(1))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.login();
          this.isSubmitted = false;
          this.notificationMessageService.pushAlert({detail: this.translate.instant('notification-password-created')})
        }
      });
  }

  public resendConfirmCode(): void {
    if (this.resendTimer > 0) {
      return;
    }

    this.apiService
      .resendPasswordVerifyCode(this.authForm.value.phoneCode['phone_code'], this.authForm.value.phoneNumber)
      .pipe(take(1))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          const eventTime = moment().add(3, 'minutes').unix();
          const currentTime = moment().unix();
          const diffTime = eventTime - currentTime;
          let duration = moment.duration(diffTime, 'milliseconds');

          this.resendTimer = +duration;
          this.isResend = true;

          window.localStorage.setItem('resendEmailTimer', String(eventTime));

          this.timerInterval = setInterval(() => {
            duration = moment.duration(+duration - 1, 'milliseconds');

            this.resendTimer = +duration;

            if (+duration <= 0) {
              window.localStorage.removeItem('resendEmailTimer');
              clearInterval(this.timerInterval);
            }
          }, 1000);

          this.notificationMessageService.pushAlert({ detail: this.translate.instant('notification-code-resended') });
        } else {
          res['errors'].forEach((error) => {
            if (error.code === 6) {
              this.notificationMessageService.pushAlert({
                detail: this.translate.instant('notification-activate-code-already-used'),
              });
            }
          });
        }
      });
  }

  public checkPhonePassword(): void {
    if (!this.authForm.value.phoneCode['phone_code'] && !this.authForm.value.phoneNumber) {
      return;
    }

    if (this.hasPhoneNumberError()) {
      return;
    }

    this.apiService
      .checkPhoneNumber(this.authForm.value.phoneCode['phone_code'], this.authForm.value.phoneNumber)
      .pipe(take(1))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          if (res['data']['hasPassword']) {
            this.nextStep(Steps.loginPassword);
          }
          if (!res['data']['hasPassword']) {
            this.nextStep(Steps.loginCreatePassword);
          }
        } else {
          if (res['errors'][0]['description'] == 'Phone does not exist!') {
            this.authForm.controls.phoneNumber.setErrors({ notFound: true });
          }
        }
      });
  }

  public login() {
    const password = this.authForm.controls.password;
    if (password.invalid) {
      password.setErrors({ error: true });
    }

    this.apiService
      .login({
        phoneCode: this.authForm.value.phoneCode.phone_code,
        phoneNumber: this.authForm.value.phoneNumber,
        password: this.authForm.value.password,
      })
      .subscribe((response) => {
        if (response['status'] === 'success') {
          this.afterLogin(response['data']['token']);
        } else if (response['status'] === 'error') {
          for (let i = 0; i < response['errors'].length; i++) {
            if (response['errors'][i]['code'] === 5) {
              this.authForm.controls.password.setErrors({ password: true });
            }
            if (response['errors'][i]['code'] === 12) {
              this.notificationMessageService.pushAlert({
                detail: this.translate.instant('notification-error-login-email-social-exists'),
              });
            }
          }
        }
      });
  }

  private afterLogin(token) {
    this.apiService
      .getSelfUser(token)
      .pipe(take(1))
      .subscribe((selfUserResponse) => {
        if (selfUserResponse['status'] === 'success') {
          this.apiService.user = selfUserResponse['data'];

          let syncEmail = selfUserResponse['data']['calendar_sync_email']
            ? selfUserResponse['data']['calendar_sync_email']
            : '';
          this.calendar.syncEmail.next(syncEmail);
          if (!this.apiService.user['phone_number']) {
            this.currentStep = Steps.phoneNumber;
            this.apiService['token'] = token;
          } else {
            window.localStorage.setItem('token', token);
            this.apiService.token = token;
            this.closeModal();
            this.appAuthService.finishAuthEvent.emit(true);
          }
        }
      });
  }

  logOut() {
    this.apiService
      .logOut()
      .pipe(take(1))
      .subscribe((response) => {
        document.body.style.overflow = 'auto';
        window.localStorage.removeItem('token');
        this.apiService.token = '';
        this.apiService.user = {};
        this.router.navigate([], {
          queryParams: {
            action: null,
          },
          queryParamsHandling: 'merge',
        });
        this.closeModal();
      });
  }

  hasPhoneNumberError(): boolean {
    const phoneControl = this.authForm.controls.phoneNumber;
    return phoneControl.getError('errorPhone') || phoneControl.getError('exist') || phoneControl.getError('notFound');
  }

  ngOnDestroy() {
    this.destroy.next(null);
    this.destroy.complete();
  }
}
