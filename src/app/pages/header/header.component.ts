import { AfterViewInit, Component, ElementRef, Inject, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ApiService } from 'src/app/core/service/api.service';
import { CookieService } from 'ngx-cookie-service';
import { AppAuthService } from 'src/app/core/service/auth.service';
import { environment } from '../../../environments/environment';
import { LocationService } from '../../core/service/location.service';
import { filter, switchMap, take, takeUntil, tap } from 'rxjs/internal/operators';
import { GapiCalendarService } from '../../core/service/gapi.calendar.service';
import { FormControl } from '@angular/forms';
import { of, Subject } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { NotificationMessageService } from 'src/app/core/service/notification.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ModalType } from 'src/app/shared/constants/modal';
import * as moment from 'moment';
import {CookiesPrefService} from '../../core/service/cookiesPref.service';

declare var gapi: any;

enum AuthModalSteps {
  general = 1,
  loginPhone = 2,
  loginPassword = 3,
  phoneNumber = 4,
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  host: {
    '(document:click)': 'onClick($event)',
  },
})
export class HeaderComponent implements OnInit, OnDestroy, AfterViewInit {
  private readonly document: Document;

  public authModalSteps = AuthModalSteps;
  public ModalType = ModalType;

  public isModalOpen = {
    [ModalType.Auth]: false,
    [ModalType.Registration]: false,
    [ModalType.Forgot]: false,
    [ModalType.ForgotFinish]: false,
    [ModalType.SelectCity]: false,
  };

  private timerInterval;
  public resendTimer = 0;
  public isResend = false;

  authModalStepActive;
  isOpenMobileMenu = false;
  geopositionStatus: string;
  geopositionDefined = null;

  isEmailConfirm = true;

  isShowUserMenu = false;
  isShowLanguageMenu = false;
  isShowNotificationMenu = false;
  isShowLocationMenu = false;
  urlEmail;
  urlCode;
  emailUpdateCode;
  forgotCode;

  notificationPromise;
  notifications = [];
  countNewNotifications = 0;

  apiUrl: string = environment.ApiUrl;
  avatarVersion = 1;

  code = '';
  codeInput = new FormControl('');

  hashParams = {};
  googleAuth;

  cookiesJson;
  cookiesAccepted;
  displayCookieCustomizeModal: boolean = false;

  private unsubscribe: Subject<any> = new Subject();

  @ViewChild('geopositionModal', { static: false }) geopositionModal: ModalComponent;
  @ViewChild('geopositionTooltip', { static: false }) geopositionTooltip: ElementRef;

  constructor(
    @Inject(DOCUMENT) document,
    public translate: TranslateService,
    private route: ActivatedRoute,
    public apiService: ApiService,
    private cookieService: CookieService,
    private authService: AppAuthService,
    public router: Router,
    public locationService: LocationService,
    public calendar: GapiCalendarService,
    private renderer2: Renderer2,
    private notificationMessageService: NotificationMessageService,
    private cookiesPrefService: CookiesPrefService
  ) {
    this.document = document;
  }

  async ngOnInit() {
    this.cookiesJson = await this.cookiesPrefService.getJsonPreferences();
    if (true) {
      window.localStorage.removeItem('AFTER_LOGIN_ACTION');
    }
    let hash = window.location.hash || window.top.location.hash;
    (hash
      ? hash
          .slice(1)
          .split('&')
          .map((param) => {
            let [id, value] = param.split('=');
            return { id, value };
          })
      : []
    ).forEach((param) => {
      this.hashParams[param.id] = decodeURIComponent(param.value); // I store it in `this.hasParams`
    });

    if (this.hashParams['access_token']) {
      const xhr = new XMLHttpRequest();
      xhr.open('GET', 'https://www.googleapis.com/oauth2/v3/userinfo?access_token=' + this.hashParams['access_token']);

      xhr.onreadystatechange = (e) => {
        if (xhr.readyState === 4 && xhr.status === 200) {
          this.afterLoginSocial({
            ...JSON.parse(xhr.response),
            provider: 'GOOGLE',
            redirect: JSON.parse(decodeURIComponent(this.hashParams['state'])),
          });
        } else if (xhr.readyState === 4 && xhr.status !== 200) {
          // Token invalid, so prompt for user permission.
        }
      };

      xhr.send(null);
    }

    this.authService.checkTokenExists();
    this.cookiesAccepted = this.cookiesPrefService.isCookiesAccepted();
    setTimeout(() => {
      this.cookiesAccepted = this.cookiesPrefService.isCookiesAccepted();
      this.cookiesPrefService.tracking();
    }, 1500);
    this.watchQueryParams();
    this.setEmailConfirmDialog();
    this.initObservers();
  }

  ngAfterViewInit() {
    this.locationService.geopositionStatus.pipe(takeUntil(this.unsubscribe)).subscribe((status) => {
      this.geopositionStatus = status;

      if (status === undefined || status === 'GEOPOSITION_OFF') {
        this.geopositionModal.showModal();
      }
    });
  }

  private initObservers(): void {
    this.authService.isEmailConfirm.pipe(takeUntil(this.unsubscribe)).subscribe((status) => {
      if (!status) {
        const eventTime = Number(window.localStorage.getItem('resendEmailTimer'));

        if (!isNaN(eventTime)) {
          const currentTime = moment().unix();
          const diffTime = eventTime - currentTime;
          let duration = moment.duration(diffTime, 'milliseconds');
          this.resendTimer = +duration;

          if (+duration > 0) {
            this.timerInterval = setInterval(() => {
              duration = moment.duration(+duration - 1, 'milliseconds');

              this.resendTimer = +duration;

              if (+duration <= 0) {
                window.localStorage.removeItem('resendEmailTimer');
                clearInterval(this.timerInterval);
              }
            }, 1000);
          } else {
            window.localStorage.removeItem('resendEmailTimer');
          }
        }

        this.isEmailConfirm = false;
        this.renderer2.setStyle(this.document.body, 'overflow', 'hidden');
        window.scroll(0, 0);
      }
    });
    this.authService.finishAuthEvent.pipe(takeUntil(this.unsubscribe)).subscribe((status) => {
      this.getNotifications();

      if (this.apiService.user['id'] && !this.apiService.user['is_phone']) {
        this.authService.isEmailConfirm.next(false);
      }

      if (status == true) {
        // DO REDIRECT IF EXIST TRIGGER;
        let AFTER_LOGIN_ACTION = window.localStorage.getItem('AFTER_LOGIN_ACTION');
        if (AFTER_LOGIN_ACTION) {
          AFTER_LOGIN_ACTION = JSON.parse(AFTER_LOGIN_ACTION);
          if (AFTER_LOGIN_ACTION['action'] === 'redirect') {
            window.localStorage.removeItem('AFTER_LOGIN_ACTION');
            if (AFTER_LOGIN_ACTION['pathname'].include('purchase/confirm')) {
              if (!this.apiService.user['client_products'].includes(AFTER_LOGIN_ACTION['queryParams']['product_id'])) {
                this.router.navigate([AFTER_LOGIN_ACTION['pathname']], {
                  queryParams: AFTER_LOGIN_ACTION['queryParams'],
                });
              }
            }
          }
        }
      }

    });
    this.authService.startAuthEvent.subscribe((data) => {
      this.authModalStepActive = this.authModalSteps.general;
      this.isModalOpen[ModalType.Auth] = true;
    });

    this.apiService.avatarUpdate.subscribe((data) => {
      this.avatarVersion++;
    });

    this.locationService.positionDefined.pipe(takeUntil(this.unsubscribe)).subscribe((status) => {
      if (status !== null) {
        this.geopositionDefined = status;

        if (this.geopositionTooltip && this.geopositionTooltip.nativeElement) {
          this.renderer2.setStyle(this.geopositionTooltip.nativeElement, 'display', 'block');
          this.renderer2.addClass(this.geopositionTooltip.nativeElement, 'show');

          setTimeout(() => {
            this.renderer2.removeClass(this.geopositionTooltip.nativeElement, 'show');

            setTimeout(() => {
              this.renderer2.setStyle(this.geopositionTooltip.nativeElement, 'display', 'none');
            }, 400);
          }, 4000);
        }
      }
    });
  }

  private watchQueryParams(): void {
    this.route.queryParams.subscribe((params) => {
      this.urlEmail = params['email'];
      this.urlCode = params['approve_code'];
      this.emailUpdateCode = params['code'];
      this.forgotCode = params['forgot_code'];

      if (this.urlEmail !== undefined && this.urlCode !== undefined) {
        this.apiService.approveRegister(this.urlEmail, this.urlCode).subscribe(() => {});
      }

      if (this.urlEmail !== undefined && this.emailUpdateCode !== undefined) {
        this.apiService.changeEmailAccept(this.urlEmail, this.emailUpdateCode).subscribe((res) => {
          if (res['status'] === 'success') {
            this.apiService.user['is_email'] = 1;
            this.apiService.user['email'] = this.urlEmail;
            this.notificationMessageService.pushAlert({
              detail: this.translate.instant('notification-email-change-success'),
            });
          }
        });
      }

      if (this.forgotCode !== undefined) {
        this.isModalOpen[ModalType.ForgotFinish] = true;
      }
    });
  }

  private setEmailConfirmDialog(): void {
    this.router.events.pipe(filter((e) => e instanceof NavigationEnd)).subscribe((event: NavigationEnd) => {
      this.authService.isEmailConfirm.emit(true);

      if (this.apiService.user['id'] && !this.apiService.user['is_phone'] && this.apiService.user['phone_number']) {
        this.authService.isEmailConfirm.emit(false);
        this.renderer2.setStyle(this.document.body, 'overflow', 'hidden');
        window.scroll(0, 0);
      }
    });
  }

  /*public cookieApprove() {
    this.cookieService.set('cookie-approve', 'true', 365, '/', window.location.hostname, false, 'Strict');
    this.cookie = false;
  }*/

  /*private checkCookieApprove(): void {
    const cookieApprove: boolean = this.cookieService.check('cookie-approve');

    if (!cookieApprove) {
      this.cookie = true;
    } else {
      this.cookie = false;
    }
  }*/

  toggleMobileMenu() {
    if (this.isOpenMobileMenu == true) {
      document.body.style.overflow = 'auto';
    } else {
      document.body.style.overflow = 'hidden';
    }
    this.isOpenMobileMenu = !this.isOpenMobileMenu;
  }

  overflowOff() {
    document.body.style.overflow = 'auto';
  }

  afterLoginSocial(user) {
    if (user && user != null && user['provider'] == 'GOOGLE') {
      return this.apiService
        .loginWithGoogle(user['sub'], user['given_name'], user['family_name'], user['email'])
        .pipe(
          switchMap((googleLoginRes) => {
            if (googleLoginRes['status'] == 'success') {
              return this.authService.afterLogin(googleLoginRes['data']['token']).pipe(
                tap((res) => {
                  if (res['status'] === 'success') {
                    if (!res['data']['phone_number']) {
                      this.toggleModal(ModalType.Auth, { authModalStep: 3 });
                    }
                    let afterLoginAction = window.localStorage.getItem('AFTER_LOGIN_ACTION');

                    if (afterLoginAction) {
                      afterLoginAction = JSON.parse(afterLoginAction);
                      if (afterLoginAction['action'] === 'redirect') {
                        window.localStorage.removeItem('AFTER_LOGIN_ACTION');
                        this.router.navigate([afterLoginAction['pathname']], {
                          queryParams: afterLoginAction['queryParams'],
                        });
                      }
                    } else if (user['redirect']) {
                      this.router.navigate([user['redirect']['pathname']], {
                        queryParams: user['redirect']['queryParams'],
                      });
                    } else {
                      this.router.navigate(['/']);
                    }
                  }
                })
              );
            } else {
              googleLoginRes['errors'].forEach((error) => {
                if (error.code === 6) {
                  this.router.navigate(['/']);
                  this.notificationMessageService.pushAlert({
                    detail: this.translate.instant('notification-error-google-email-is-exists'),
                  });
                }
              });
            }

            return of(googleLoginRes);
          }),
          take(1)
        )
        .subscribe((res) => {});
    }
  }

  getNotifications() {
    if (!this.apiService.token) {
      return;
    }
    let lastId = 0;
    let notificationTmp;
    if (this.notifications.length > 0) {
      lastId = this.notifications[0]['id'];
    }

    this.apiService
      .getNotification(lastId)
      .pipe(take(1))
      .subscribe((data) => {
        if (data['status'] == 'success') {
          notificationTmp = data['data'];
          for (let i = 0; i < notificationTmp.length; i++) {
            notificationTmp[i]['text'] = JSON.parse(notificationTmp[i]['text']);

            if (notificationTmp[i]['is_show'] == 0) {
              this.countNewNotifications++;
            }
          }
          notificationTmp = notificationTmp.reverse();
          this.notifications = notificationTmp.concat(this.notifications);
          this.getNotifications();
        } else {
          this.getNotifications();
        }
      });
  }

  // 1. Change isOpen status of modal
  // 2. Do side effects, then toggle some modal
  public toggleModal(type: ModalType = null, params = null): void {
    this.isModalOpen = {
      ...this.isModalOpen,
      [type]: !this.isModalOpen[type],
    };

    Object.keys(this.isModalOpen).forEach((key) => {
      if (!type) {
        this.isModalOpen[key] = false;
      }

      switch (type) {
        case ModalType.Auth:
          if (this.isModalOpen[key]) {
            if (params) {
              this.authModalStepActive = params.authModalStep;
            }
          } else {
            //window.localStorage.removeItem('AFTER_LOGIN_ACTION');
          }

          break;
        case ModalType.ForgotFinish:
          if (!this.isModalOpen[key]) {
            this.router.navigate(['']);
          }
          break;
        default:
          break;
      }
    });
  }

  geopositionModalClose() {
    this.geopositionModal.hideModal();

    if (this.geopositionStatus === 'GEOPOSITION_OFF') {
      this.locationService.setGeopostionStatus('GEOPOSITION_OFF_DONE');
    }
  }

  toggleUserMenu() {
    this.isShowUserMenu = !this.isShowUserMenu;
  }

  toggleLanguageMenu() {
    this.isShowLanguageMenu = !this.isShowLanguageMenu;
  }

  showNotificationMenu() {
    this.isShowNotificationMenu = true;
    this.countNewNotifications = 0;
  }

  closeNotificationMenu() {
    this.isShowNotificationMenu = false;
  }

  selectItemUserMenu() {}

  setLanguage(language) {
    this.translate.use(language);
    localStorage.setItem('lang', language);
  }

  public cancelConfirm(): void {
    const { google_reg } = this.apiService.user;
    this.isEmailConfirm = !this.isEmailConfirm;

    if (google_reg) {
      this.authModalStepActive = AuthModalSteps.phoneNumber;
      this.toggleModal(ModalType.Auth);
    } else {
      this.logOut();
      this.toggleModal(ModalType.Registration);
    }
  }

  public resendConfirmCode(): void {
    if (this.resendTimer > 0) {
      return;
    }

    this.apiService
      .resendConfirmCode()
      .pipe(take(1))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          const eventTime = moment().add(3, 'minutes').unix();
          const currentTime = moment().unix();
          const diffTime = eventTime - currentTime;
          let duration = moment.duration(diffTime, 'milliseconds');

          this.resendTimer = +duration;
          this.isResend = true;

          window.localStorage.setItem('resendEmailTimer', String(eventTime));

          this.timerInterval = setInterval(() => {
            duration = moment.duration(+duration - 1, 'milliseconds');

            this.resendTimer = +duration;

            if (+duration <= 0) {
              window.localStorage.removeItem('resendEmailTimer');
              clearInterval(this.timerInterval);
            }
          }, 1000);

          this.notificationMessageService.pushAlert({ detail: this.translate.instant('notification-code-resended') });
        } else {
          res['errors'].forEach((error) => {
            if (error.code === 6) {
              this.notificationMessageService.pushAlert({
                detail: this.translate.instant('notification-activate-code-already-used'),
              });
            }
          });
        }
      });
  }

  logOut() {
    this.apiService
      .logOut()
      .pipe(take(1))
      .subscribe((response) => {
        document.body.style.overflow = 'auto';
        window.localStorage.removeItem('token');
        this.apiService.token = '';
        this.apiService.user = {};

        this.router.navigate([], {
          queryParams: {
            action: null,
          },
          queryParamsHandling: 'merge'
        });

      });
  }

  onClick(event) {
    let userMenuClick = false;
    let languageMenu = false;
    let locationMenu = false;
    let notificationMenu = false;
    let closeNotificationMenu = false;
    // if (!('path' in Event.prototype)) {
    //   Object.defineProperty(Event.prototype, 'path', {
    //     get: function () {
    //       const path = [];
    //       let currentElem = this.target;
    //       while (currentElem) {
    //         path.push(currentElem);
    //         currentElem = currentElem.parentElement;
    //       }
    //       if (path.indexOf(window) === -1 && path.indexOf(document) === -1)
    //         path.push(document);
    //       if (path.indexOf(window) === -1)
    //         path.push(window);
    //       return path;
    //     }
    //   });
    // }
    for (let i = 0; i < event.path.length; i++) {
      if (event.path[i].id === 'user-info-menu') {
        userMenuClick = true;
      }
      if (event.path[i].id === 'language-menu') {
        languageMenu = true;
      }
      if (event.path[i].id === 'location-menu') {
        locationMenu = true;
      }
      if (event.path[i].id === 'notification-menu' || event.path[i].id === 'notif-container') {
        notificationMenu = true;
      }
      if (event.path[i].id == 'notification-close') {
        closeNotificationMenu = true;
      }
    }

    if (!userMenuClick) {
      this.isShowUserMenu = false;
    }

    if (!languageMenu) {
      this.isShowLanguageMenu = false;
    }

    if (!locationMenu) {
      this.isShowLocationMenu = false;
    }

    if (!notificationMenu || closeNotificationMenu) {
      this.isShowNotificationMenu = false;
    }
  }

  blurCode() {
    if (this.code == '') {
      this.codeInput.setErrors({ required: true });
      return false;
    } else {
      this.codeInput.setErrors({ required: false });
      return true;
    }
  }

  sendCode() {
    this.codeInput.setErrors({ invalid: false });
    if (!this.blurCode()) {
      return;
    }
    this.apiService
      .approvePhone(this.apiService['user']['phone_code'], this.apiService['user']['phone_number'], this.code)
      .pipe(take(1))
      .subscribe((data) => {
        if (data['status'] === 'success') {
          this.apiService.user['is_phone'] = 1;
          this.isEmailConfirm = true;
          this.renderer2.removeStyle(this.document.body, 'overflow');
        } else if (data['status'] == 'error') {
          this.codeInput.setErrors({ invalid: true });
        }
      });
  }

  navigete() {
    window.location.href = 'https://bodywell.io/';
  }

  submitCookies(isConfirm: boolean) {
    if (!isConfirm) {
      this.cookiesAccepted = true;
      this.displayCookieCustomizeModal = false;
      document.body.style.overflow = 'auto';
    }

    this.cookiesPrefService.submitCookies(this.cookiesJson, isConfirm);
    this.cookiesAccepted = true;
    this.toggleCookieCustomizeModal(false);
  }

  toggleCookieCustomizeModal(isOpen) {
    this.displayCookieCustomizeModal = isOpen;
    document.body.style.overflow = (isOpen) ? 'hidden' : 'auto';
  }

  ngOnDestroy() {
    clearInterval(this.timerInterval);
    this.unsubscribe.next(true);
    this.unsubscribe.complete();
  }
}
