import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { AuthModalComponent } from './components/auth-modal/auth-modal.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RegisterModalComponent } from './components/register-modal/register-modal.component';
import { SelectModule } from 'src/app/shared/select/select.module';
import { ForgotModalComponent } from './components/forgot-modal/forgot-modal.component';
import { SearchComponent } from './components/search/search.component';
import { ForgotFinishComponent } from './components/forgot-finish/forgot-finish.component';
import { SelectCityModalComponent } from './components/select-city-modal/select-city-modal.component';
import { UpButtonComponent } from './components/up-button/up-button.component';
import { GeopositionModalComponent } from './components/geoposition-modal/geoposition-modal.component';

@NgModule({
  declarations: [
    HeaderComponent,
    AuthModalComponent,
    RegisterModalComponent,
    ForgotModalComponent,
    SearchComponent,
    ForgotFinishComponent,
    SelectCityModalComponent,
    UpButtonComponent,
    GeopositionModalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SelectModule,
  ],
  exports: [
    HeaderComponent,
    SearchComponent,
    UpButtonComponent,
    GeopositionModalComponent
  ]
})
export class HeaderModule { }
