import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { ProfileMenuComponent } from './components/profile-menu/profile-menu.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HeaderModule } from '../header/header.module';
import { TabModule } from 'src/app/shared/tab/tab.module';
import { UserSessionsComponent } from './components/user-sessions/user-sessions.component';
import { UserSubscriptionComponent } from './components/user-subscription/user-subscription.component';
import { UserFavoritesComponent } from './components/user-favorites/user-favorites.component';
import { DpDatePickerModule } from 'ng2-date-picker';
import { StubModule } from 'src/app/shared/stub/stub.module';
import { NgxFileDropModule } from 'ngx-file-drop';
import { ImageCropperModule } from 'ngx-image-cropper';
import { UserGcalendarComponent } from './components/user-gcalendar/user-gcalendar.component';
import { OverlayPanelModule } from 'primeng/overlaypanel';

@NgModule({
  declarations: [
    ProfileComponent,
    UserSettingsComponent,
    ProfileMenuComponent,
    UserSessionsComponent,
    UserSubscriptionComponent,
    UserFavoritesComponent,
    UserGcalendarComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    HeaderModule,
    TabModule,
    DpDatePickerModule,
    StubModule,
    NgxFileDropModule,
    ImageCropperModule,
    OverlayPanelModule,
  ],
  exports: [
    ProfileComponent,
    UserSettingsComponent,
    ProfileMenuComponent,
    UserSessionsComponent,
    UserSubscriptionComponent,
    UserFavoritesComponent,
    UserGcalendarComponent,
  ],
})
export class ProfileModule {}
