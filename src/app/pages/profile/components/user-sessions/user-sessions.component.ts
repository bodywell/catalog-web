import { Component, Inject, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/core/service/api.service';
import { environment } from '../../../../../environments/environment';
import { DOCUMENT } from '@angular/common';
import { take } from 'rxjs/operators';
import * as moment from 'moment';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-user-sessions',
  templateUrl: './user-sessions.component.html',
  styleUrls: ['./user-sessions.component.scss']
})
export class UserSessionsComponent implements OnInit {
  private readonly document: Document;

  @ViewChild('modal', { static: false }) modal: ModalComponent;

  constructor(@Inject(DOCUMENT) document, private apiService: ApiService, private renderer2: Renderer2) {
    this.document = document;
  }

  activeTab = 1;
  apiUrl: string = environment.ApiUrl;
  tabs = [
    { id: 1, title: 'profile-appointments-tab-waiting', },
    { id: 2, title: 'profile-appointments-tab-completed' },
    { id: 3, title: 'profile-appointments-tab-cancelled' }
  ];

  isShowModal = false;
  isShowCancelModal = false;
  isShowReviewModal = false;
  userInfo;

  cancelItem;
  reviewItem;
  rate = 1;
  descriptionReview = '';

  waiting = [];
  completed = [];
  cancelled = [];

  ngOnInit() {

    this.userInfo = this.apiService.user;

    if (!this.userInfo['id']) {
      const timer = setInterval(() => {
        this.userInfo = this.apiService.user;

        if (this.userInfo['id']) {
          this.getBookingInfo();
          clearInterval(timer);
        }
      }, 25);
    } else {
      this.getBookingInfo();
    }
  }

  getBookingInfo() {
    this.apiService.getBooking().pipe(take(1)).subscribe(res => {
      if (res['status'] == 'success') {
        this.waiting = [];
        this.completed = [];
        this.cancelled = [];

        res['data'].forEach(session => {
          if (session.client_status === 4 || session.client_status === 5) {
            this.cancelled.push(session);
          } else if ((session.status === 3 && session.client_status === 3) || moment().diff(moment(session.to_date)) > 0) {
            this.completed.push(session);
          } else if ((session.status <= 2 || session.client_status <= 2) && moment().diff(moment(session.to_date)) < 0) {

            this.waiting.push(session);

          }
        });
      }
    });
  }


  cancelBook() {
    this.apiService.cancelBooking(this.cancelItem['id']).pipe(take(1)).subscribe(data => {
      if (data['status'] == 'success') {
        this.getBookingInfo();
        this.hideCancelModal();
      }
    });
  }

  sendReview() {
    this.apiService.sendReview(this.reviewItem['company_id'], 'Catalog', this.descriptionReview, this.rate).pipe(take(1)).subscribe(data => {
      if (data['status'] == 'success') {
        this.getBookingInfo();
        this.hideReviewModal();
      }
    });
  }

  rateChange(rate) {
    this.rate = rate;
  }
  changeTab(tabId) {
    this.activeTab = tabId;
  }

  showCancelModal(item) {
    this.isShowModal = true;
    this.isShowCancelModal = true;
    this.renderer2.addClass(this.document.body, 'overflow-hidden');
    this.cancelItem = item;
  }

  hideCancelModal() {
    this.isShowModal = false;
    this.renderer2.removeClass(this.document.body, 'overflow-hidden');
    this.isShowCancelModal = false;
  }

  showReviewModal(item) {
    this.isShowModal = true;
    this.isShowReviewModal = true;
    this.reviewItem = item;
  }

  hideReviewModal() {
    this.isShowModal = false;
    this.isShowReviewModal = false;
  }

  getTime(item) {
    const tmp = item.split(' ')[1];
    return tmp.split(':')[0] + ':' + tmp.split(':')[1];
  }

}
