import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { countries } from '../../../../shared/select/countries';
import { ApiService } from 'src/app/core/service/api.service';
import { FormControl } from '@angular/forms';
import { NgxFileDropEntry, FileSystemFileEntry } from 'ngx-file-drop';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { environment } from '../../../../../environments/environment';
import { NotificationMessageService } from '../../../../core/service/notification.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { take } from 'rxjs/operators';
import { commonDataService } from 'src/app/core/service/commonData.service';
import { MapsAPILoader } from '@agm/core';

class UserEmailNotifySettings {
  public email_notif_booking_request = false;
  public email_notif_booking_accept = false;
  public email_notif_booking_reject = false;
  public email_notif_booking_cancel = false;
  public email_notif_booking_complete = false;
}

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss'],
})
export class UserSettingsComponent implements OnInit {
  public userEmailNotifySettings = new UserEmailNotifySettings();
  public files: NgxFileDropEntry[] = [];
  Object = Object;
  countries = countries;
  registerInfo = {};
  sexList = [
    { key: 3, value: 'profile-gender-none' },
    { key: 1, value: 'profile-gender-male' },
    { key: 2, value: 'profile-gender-female' },
  ];
  activeTab = 1;

  tabs = [
    { id: 1, title: 'profile-settings-tab-about' },
    { id: 2, title: 'profile-settings-tab-email' },
    { id: 3, title: 'profile-settings-tab-security' },
    { id: 4, title: 'profile-settings-tab-email-notify-settings' },
  ];
  apiUrl: string = environment.ApiUrl;

  isShowModal = false;
  isAvatarUploadModal = false;
  isShowAvaratCroper = false;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  imageFile;
  public phoneCodes = [];

  userPasswords = {
    password: '',
    passwordnew: '',
    repasswordnew: '',
  };
  userInfo: any = {};
  zoom = 2;
  passwordInput = new FormControl('');
  passwordNewInput = new FormControl('');
  repasswordNewInput = new FormControl('');
  changeEmailPasswordInput = new FormControl('');
  changeEmailPassword = '';
  changeEmailInput = new FormControl('');
  newEmail = '';
  avatarVersion = 1;

  public birthdayCalendarMaxDate = new Date();

  address;
  geoCoder;

  @ViewChild('search', { static: false }) public searchElementRef: ElementRef;

  constructor(
    public apiService: ApiService,
    public notifications: NotificationMessageService,
    public translate: TranslateService,
    private commonDataService: commonDataService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) {}

  ngOnInit() {
    if (!this.userInfo['id']) {
      const timer = setInterval(() => {
        this.userInfo = {
          ...this.apiService.user,
          phone_code: {
            phone_code: this.apiService.user['phone_code'],
            code: this.apiService.user['phone_country'],
          },
        };

        Object.keys(this.userEmailNotifySettings).forEach((key) => {
          this.userEmailNotifySettings[key] = Boolean(this.apiService.user[key]);
        });

        if (this.userInfo['id']) {
          this.parseUserData();

          this.loadPlacesAutocomplete();
          clearInterval(timer);
        }
      }, 25);
    } else {
      this.parseUserData();
      this.loadPlacesAutocomplete();
    }

    this.commonDataService
      .getPhoneCodes()
      .pipe(take(1))
      .subscribe((phoneCodes) => {
        this.phoneCodes = phoneCodes;
      });
  }

  showUploadAvatarModal() {
    this.isShowModal = true;
    this.isAvatarUploadModal = true;
  }

  closeUploadAvatarModal() {
    this.isShowModal = false;
    this.isAvatarUploadModal = false;
  }

  showAvatarCropModal() {
    this.isShowModal = true;
    this.isShowAvaratCroper = true;
  }

  closeAvatarCropModal() {
    this.isShowModal = false;
    this.isShowAvaratCroper = false;
  }

  backStepAvatarUloader() {
    this.isShowAvaratCroper = false;
    this.isAvatarUploadModal = true;
  }

  changeTab(tabId) {
    this.activeTab = tabId;
  }

  saveUserSettings() {
    this.apiService
      .setUser({
        ...this.userInfo,
        phone_country: this.userInfo['phone_code']['code'],
        phone_code: this.userInfo['phone_code']['phone_code'],
        birth_date: moment(this.userInfo['birth_date']).isValid()
          ? moment(this.userInfo['birth_date']).format('YYYY-MM-DD')
          : '',
      })
      .subscribe((data) => {
        if (data['status'] === 'success') {
          this.notifications.pushAlert({ detail: this.translate.instant('alert.update-settings') });
        } else if (data['status'] === 'error') {
          data['errors'].forEach((error) => {
            if (error.code === 6) {
              this.notifications.pushAlert({
                detail: this.translate.instant('notification-error-phone-number-already-used'),
              });
            }
          });
        }
      });
  }

  loadPlacesAutocomplete() {
    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder();
      setTimeout(() => {
        const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
          types: ['address'],
        });
        autocomplete.addListener('place_changed', () => {
          this.ngZone.run(() => {
            // get the place result
            const place: google.maps.places.PlaceResult = autocomplete.getPlace();

            // verify result
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }
            this.userInfo['latitude'] = place.geometry.location.lat();
            this.userInfo['longitude'] = place.geometry.location.lng();
            this.zoom = 15;

            this.getAddressComponentByPlace(place);
          });
        });
      }, 500);
    });
  }

  public setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.userInfo['latitude'] = position.coords.latitude;
        this.userInfo['longitude'] = position.coords.longitude;
        this.zoom = 15;
        this.getAddress(this.userInfo['latitude'], this.userInfo['longitude']);
      });
    }
  }
  private getAddressComponentByPlace(place) {
    const components = place.address_components;
    let country = null;
    let city = null;
    let postalCode = null;
    let street_number = null;
    let route = null;
    let locality = null;

    for (let i = 0, component; (component = components[i]); i++) {
      if (component.types[0] === 'country') {
        country = component['long_name'];
      }
      if (component.types[0] === 'administrative_area_level_1') {
        city = component['long_name'];
      }
      if (component.types[0] === 'postal_code') {
        postalCode = component['short_name'];
      }
      if (component.types[0] === 'street_number') {
        street_number = component['short_name'];
      }
      if (component.types[0] === 'route') {
        route = component['long_name'];
      }
      if (component.types[0] === 'locality') {
        locality = component['short_name'];
      }
    }

    this.address = place['formatted_address'];

    this.userInfo['address'] = this.address;
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ location: { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 15;
          this.getAddressComponentByPlace(results[0]);
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  blurPasswordNew() {
    if (
      !this.userPasswords['passwordnew'] ||
      this.userPasswords['passwordnew'].length < 6 ||
      !/^(?=.*\d)(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?!.*\s).*$/.test(this.userPasswords['passwordnew'])
    ) {
      this.passwordNewInput.setErrors({ passwordError: true });
      return false;
    } else {
      this.passwordNewInput.setErrors({ passwordError: false });
      return true;
    }
  }

  blurRepasswordNew() {
    if (
      !this.userPasswords['repasswordnew'] ||
      !this.userPasswords['passwordnew'] ||
      this.userPasswords['passwordnew'] != this.userPasswords['repasswordnew']
    ) {
      this.repasswordNewInput.setErrors({ passwordError: true });
      return false;
    } else {
      this.repasswordNewInput.setErrors({ passwordError: false });
      return true;
    }
  }

  blurChangeEmailPasswordInput() {
    if (
      !this.changeEmailPassword ||
      this.changeEmailPassword.length < 6 ||
      !/^(?=.*\d)(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?!.*\s).*$/.test(this.changeEmailPassword)
    ) {
      this.changeEmailPasswordInput.setErrors({ passwordError: true });
      return false;
    } else {
      this.changeEmailPasswordInput.setErrors({ passwordError: false });
      return true;
    }
  }

  passwordsValidate() {
    const passwordNew = this.blurPasswordNew();
    const repasswordNew = this.blurRepasswordNew();

    if (passwordNew && repasswordNew) {
      return true;
    } else {
      return false;
    }
  }

  changePassword() {
    const valid = this.passwordsValidate();

    if (!valid) {
      return;
    }

    this.apiService
      .changePassword(this.userPasswords.password, this.userPasswords.passwordnew)
      .pipe(take(1))
      .subscribe((data) => {
        if (data['status'] === 'success') {
          this.userPasswords = {
            password: '',
            passwordnew: '',
            repasswordnew: '',
          };
          this.notifications.pushAlert({ detail: 'notification-success-password-change' });
        } else if (data['status'] === 'error') {
          if (data['errors'][0]['code'] === 2) {
            this.passwordInput.setErrors({ passwordError: true });
          } else if (data['errors'][0]['code'] === 5) {
            this.passwordInput.setErrors({ passwordError: true });
          }
        }
      });
  }

  changeEmail() {
    if (!this.blurEmail()) {
      return;
    }
    this.apiService.changeEmail(this.newEmail, this.changeEmailPassword).subscribe((data) => {
      if (data['status'] === 'error') {
        if (data['errors'][0]['code'] == 6) {
          this.changeEmailInput.setErrors({ errorUsedEmail: true, errorEmail: true });
        } else if (data['errors'][0]['code'] == 2) {
          this.changeEmailPasswordInput.setErrors({ passwordError: true });
        } else if (data['errors'][0]['code'] == 5) {
          this.changeEmailPasswordInput.setErrors({ passwordError: true });
        }
      } else {
        this.changeEmailInput.setErrors({ errorUsedEmail: false, errorEmail: false });
        this.changeEmailPasswordInput.setErrors({ passwordError: false });
        this.userInfo['is_email'] = 0;
        this.userInfo['email'] = this.newEmail;
      }
    });
  }

  public changeSetting(setting, value): void {
    this.userEmailNotifySettings[setting] = value;
  }

  public submitEmailNotifySettings(): void {
    const data = Object.keys(this.userEmailNotifySettings).reduce((acc, key) => {
      acc[key] = Number(this.userEmailNotifySettings[key]);

      return acc;
    }, {});

    this.apiService
      .setUser(data)
      .pipe(take(1))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.notifications.pushAlert({ detail: 'notification-update-user-email-settings' });
        }
      });
  }

  blurEmail() {
    if (!this.newEmail || this.newEmail.length === 0 || !this.validateEmail(this.newEmail)) {
      this.changeEmailInput.setErrors({ errorValidEmail: true, errorEmail: true });
      return false;
    } else {
      this.changeEmailInput.setErrors({ errorValidEmail: false, errorEmail: false });
      return true;
    }
  }

  public setDate(date): void {
    this.userInfo['birth_date'] = date;
  }

  validateEmail(email) {
    const re =
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;

    for (const droppedFile of files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.imageFile = file;
          this.closeUploadAvatarModal();
          this.showAvatarCropModal();
          return;
        });
      }
    }
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.closeUploadAvatarModal();
    this.showAvatarCropModal();
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  uploadAvatar() {
    const user = { avatar: this.croppedImage };

    this.apiService.setUser(user).subscribe((data) => {
      if (data['status'] == 'success') {
        this.apiService.avatarUpdate.emit(true);
        this.avatarVersion++;
        this.closeAvatarCropModal();
      }
    });
  }

  parseUserData() {
    if (!this.userInfo['birth_date']) {
      this.userInfo['birth_date'] = '';
    } else {
      this.userInfo['birth_date'] = moment(this.userInfo['birth_date'], 'YYYY-MM-DD');
    }
    if (this.userInfo['sex'] == 0) {
      this.userInfo['sex'] = 3;
    }

    if (this.userInfo['email']) {
      this.newEmail = this.userInfo['email'];
    }

    if (this.userInfo['latitude'] && this.userInfo['longitude']) {
      this.zoom = 15;
    }
  }

  public setDisabledDays(day: Date): boolean {
    if (day) {
      return moment(day).startOf('day').diff(moment().startOf('day')) < 0;
    } else {
      return true;
    }
  }
}
