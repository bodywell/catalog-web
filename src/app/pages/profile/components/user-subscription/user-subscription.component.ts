import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/service/api.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-user-subscription',
  templateUrl: './user-subscription.component.html',
  styleUrls: ['./user-subscription.component.scss']
})
export class UserSubscriptionComponent implements OnInit {


  activeTab = 1;

  tabs = [
    { id: 1, title: 'profile-pass-tab-active', },
    { id: 2, title: 'profile-pass-tab-inactive' }
  ];
  waiting;
  completed;
  cancelled;
  sessions = [];
  apiUrl: string = environment.ApiUrl;

  active = [];
  inactive = [];
  userInfo;

  constructor(public apiService: ApiService) { }

  ngOnInit() {
    this.userInfo = this.apiService.user;

    if (!this.userInfo['id']) {
      const timer = setInterval(() => {
        this.userInfo = this.apiService.user;

        if (this.userInfo['id']) {
          this.getBookingInfo();
          clearInterval(timer);
        }
       }, 25);
    } else {
      this.getBookingInfo();
    }
  }
  getBookingInfo() {
    this.apiService.getSubscription().subscribe(data => {
      if (data['status'] == 'success') {
        this.sessions = data['data'];

        for (let i = 0; i < this.sessions.length; i++) {
          if (this.sessions[i]['is_active'] == 1 && this.sessions[i]['count'] != this.sessions[i]['limit']) {
            this.active.push(this.sessions[i]);
          } else {
            this.inactive.push(this.sessions[i]);
          }
        }
        // this.waiting = this.sessions.find(x => x.product_type == 1 && (x.status == 0 || x.status == 1 || x.status == 2 || x.status == 3));
        // this.completed = this.sessions.find(x => x.product_type == 1 && (x.status == 4));
        // this.cancelled = this.sessions.find(x => x.product_type == 1 && (x.status == 5));
      }
    });
  }
  changeTab(tabId) {
    this.activeTab = tabId;
  }

}
