import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ApiService} from "../../../../core/service/api.service";
import {CookieService} from "ngx-cookie-service";
import {AuthService} from "angularx-social-login";

@Component({
  selector: 'app-profile-menu',
  templateUrl: './profile-menu.component.html',
  styleUrls: ['./profile-menu.component.scss']
})
export class ProfileMenuComponent implements OnInit {

  public href: string = "";

  constructor(
      private router: Router,
      public apiService: ApiService,
      public cookieService: CookieService,
      public socialAuthService: AuthService
  ) {}

  ngOnInit() {
      this.href = this.router.url;
  }

  logOut() {
    this.apiService.logOut().subscribe((response) => {
      window.localStorage.removeItem('token')
      this.apiService.token = '';
      this.apiService.user = {};
      this.socialAuthService.signOut();
      this.router.navigateByUrl('/');
    });
  }
}
