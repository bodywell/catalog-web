import {AfterViewChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import { ApiService } from 'src/app/core/service/api.service';
import {GapiCalendarService} from "../../../../core/service/gapi.calendar.service";
import {Subject} from "rxjs/index";
import {takeUntil} from "rxjs/internal/operators";
import {take} from 'rxjs/operators';

@Component({
    selector: 'app-user-gcalendar',
    templateUrl: './user-gcalendar.component.html',
    styleUrls: ['./user-gcalendar.component.scss']
})
export class UserGcalendarComponent implements OnInit, AfterViewChecked {

    public googleEmail: string;
    public syncAborted: boolean;
    public isInit = false;

    constructor(public apiService: ApiService, public calendar: GapiCalendarService) {}

    ngOnInit() {

    }

    ngAfterViewChecked() {
        if (!this.isInit) {
            if (this.apiService.user['id']) {
                this.isInit = true;
                this.googleEmail = (this.apiService.user['calendar_sync_email'].length) && this.apiService.user['calendar_sync_email'];
                this.syncAborted = (this.apiService.user['calendar_sync_aborted'] == true) && this.apiService.user['calendar_sync_aborted'];
            }
        }
    }

    doSync(status) {
        if (status) {
            const params = {
                token: this.apiService.token,
                callback_url: window.location.href,
            };
            const syncUrl = `${this.apiService.apiUrl}/account/sync?state=${btoa(JSON.stringify(params))}`;
            document.location.href = syncUrl;
        } else {
            this.apiService
                .syncCalendarDisable()
                .pipe(take(1))
                .subscribe((response) => {
                    if (response['status'] == 'success') {
                        this.googleEmail = '';
                        this.apiService.user['calendar_sync_email'] = '';
                    }
                });
        }
    }
}
