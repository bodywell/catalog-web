import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { ApiService } from 'src/app/core/service/api.service';
import {take} from "rxjs/internal/operators";

@Component({
  selector: 'app-user-favorites',
  templateUrl: './user-favorites.component.html',
  styleUrls: ['./user-favorites.component.scss']
})
export class UserFavoritesComponent implements OnInit {

  apiUrl: string = environment.ApiUrl;
  constructor(public apiService: ApiService) { }

  ngOnInit() {
  }

  deleteFavorites(id) {
    this.apiService.deleteFavorites(id).subscribe(response => {
      if (response['status'] == 'success') {
        this.apiService.getSelfUser().pipe(take(1)).subscribe((response) => {
          if (response['status'] === 'success') {
            this.apiService.user = response['data'];
          }
        });
      }
    });
  }

}
