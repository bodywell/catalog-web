import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeModule } from './home/home.module';
import { KitModule } from './kit/kit.module';
import { SharedModule } from '../shared/shared.module';
import { HeaderModule } from './header/header.module';
import { FooterModule } from './footer/footer.module';
import { SearchResultModule } from './search-result/search-result.module';
import { CardModule } from './card/card.module';
import { BookingModule } from './booking/booking.module';
import { ProfileModule } from './profile/profile.module';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { PurchaseModule } from './purchase/purchase.module';
@NgModule({
  declarations: [PagesComponent],
  imports: [
    CommonModule,
    SharedModule,
    HomeModule,
    KitModule,
    HeaderModule,
    FooterModule,
    SearchResultModule,
    CardModule,
    ProfileModule,
    PagesRoutingModule
  ],
  exports: [
    HomeModule,
    KitModule,
    HeaderModule,
    FooterModule,
    SearchResultModule,
    CardModule,
    BookingModule,
    ProfileModule,
    PurchaseModule
  ]
})
export class PagesModule { }
