import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KitComponent } from './kit.component';
import { SelectModule } from 'src/app/shared/select/select.module';
import { SharedModule } from 'src/app/shared/shared.module';
@NgModule({
  declarations: [KitComponent],
  imports: [
    CommonModule,
    SharedModule,
    SelectModule
  ],
  exports: [
    KitComponent
  ]
})
export class KitModule { }
