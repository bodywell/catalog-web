import { Component, OnInit } from '@angular/core';
import { countries } from '../../shared/select/countries';
@Component({
  selector: 'app-kit',
  templateUrl: './kit.component.html',
  styleUrls: ['./kit.component.scss']
})
export class KitComponent implements OnInit {

  constructor() { }
  test;
  test2;
  countries = countries;
  testSelectData = [
    {key: 'key1', value: 'value1'},
    {key: 'key2', value: 'value2'},
    {key: 'key3', value: 'value3'},
    {key: 'key4', value: 'value4'},
  ];
  ngOnInit() {
  }

}
