import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HomeModule } from './home/home.module';
import { KitComponent } from './kit/kit.component';
import { KitModule } from './kit/kit.module';
import { SearchResultComponent } from './search-result/search-result.component';
import { CardComponent } from './card/card.component';
import { UserSettingsComponent } from './profile/components/user-settings/user-settings.component';
import { UserSessionsComponent } from './profile/components/user-sessions/user-sessions.component';
import { UserSubscriptionComponent } from './profile/components/user-subscription/user-subscription.component';
import { UserFavoritesComponent } from './profile/components/user-favorites/user-favorites.component';
import { UserGcalendarComponent } from './profile/components/user-gcalendar/user-gcalendar.component';
import { PagesComponent } from './pages.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'changeforgot', component: HomeComponent },
      { path: 'kit', component: KitComponent },
      { path: 'search-result', component: SearchResultComponent },
      { path: 'card/:company_id/:branch_id', component: CardComponent },
      { path: 'user-settings', component: UserSettingsComponent },
      { path: 'user-sessions', component: UserSessionsComponent },
      { path: 'user-subscription', component: UserSubscriptionComponent },
      { path: 'user-favorites', component: UserFavoritesComponent },
      { path: 'user-gcalendar', component: UserGcalendarComponent },
      {
        path: 'booking',
        loadChildren: () => import('./booking/booking.module').then((m) => m.BookingModule),
      },
      {
        path: 'purchase',
        loadChildren: () => import('./purchase/purchase.module').then((m) => m.PurchaseModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), HomeModule, KitModule],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
