import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  host: {
    '(document:click)': 'onClick($event)',
   }
})
export class FooterComponent implements OnInit {

  isShowLanguageMenu = false;
  constructor(public translate: TranslateService) { }

  ngOnInit() {
  }

  toggleLanguageMenu() {
    this.isShowLanguageMenu = !this.isShowLanguageMenu;
  }

  setLanguage(language) {
    this.translate.use(language);
    localStorage.setItem('lang', language);
  }

  onClick(event) {
    let languageMenu = false;
    // if (!('path' in Event.prototype)) {
    //   Object.defineProperty(Event.prototype, 'path', {
    //     get: function () {
    //       const path = [];
    //       let currentElem = this.target;
    //       while (currentElem) {
    //         path.push(currentElem);
    //         currentElem = currentElem.parentElement;
    //       }
    //       if (path.indexOf(window) === -1 && path.indexOf(document) === -1)
    //         path.push(document);
    //       if (path.indexOf(window) === -1)
    //         path.push(window);
    //       return path;
    //     }
    //   });
    // }
    for (let i = 0; i < event.path.length; i++) {
      if (event.path[i].id === 'language-menu') {
        languageMenu = true;
      }
    }

    if (!languageMenu) {
      this.isShowLanguageMenu = false;
    }
  }
}
