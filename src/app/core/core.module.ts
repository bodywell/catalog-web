import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesModule } from '../pages/pages.module';
import { ApiService } from './service/api.service';
import { AppAuthService } from './service/auth.service';
import { LocationService } from "./service/location.service";
import { NotificationMessageService } from "./service/notification.service";
import { GapiCalendarService } from "./service/gapi.calendar.service";
import { CompanyEmployeesService } from './service/employees.service';
import { CompanyService } from './service/company.service';
import { BookingService } from './service/booking.service';
import { commonDataService } from './service/commonData.service';
import { HelperService } from './service/helper.service';
import { ErrorsService } from './service/errors.service';
import { PurchaseService } from './service/purchase.service';
import {CookiesPrefService} from './service/cookiesPref.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PagesModule
  ],
  providers: [
    ApiService,
    AppAuthService,
    LocationService,
    NotificationMessageService,
    GapiCalendarService,
    CompanyEmployeesService,
    CompanyService,
    BookingService,
    PurchaseService,
    commonDataService,
    HelperService,
    CookiesPrefService,
    ErrorsService
  ],
  exports: [
    PagesModule,
  ]
})
export class CoreModule { }
