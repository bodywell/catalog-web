import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable()
export class NotificationMessageService {
    constructor(private messageService: MessageService) {}

    pushAlert({ life = 5000, detail = '', data = null }) {
        this.messageService.add({closable: false, detail, life, data });
    }
}
