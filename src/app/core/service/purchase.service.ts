import {EventEmitter, Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Subject} from 'rxjs/index';

@Injectable()
export class PurchaseService {
    productList: BehaviorSubject<Object> = new BehaviorSubject({});
    targetProduct: BehaviorSubject<Object> = new BehaviorSubject({});
    isInit: BehaviorSubject<boolean> = new BehaviorSubject(false);

    detailsModalData: Object;
    selectedItemData: Object;

    isOpenDetailsModal = new EventEmitter<boolean>();
    isSelectedItem = new EventEmitter<boolean>();

    actualPaymentStatus = new EventEmitter<any>();
    isEnabledPurchase = new BehaviorSubject(false);

    constructor() { }

    setProductList(data: Object) {
        this.productList.next(data);
    }

    setPurchaseStatus(status: any) {
        this.isEnabledPurchase.next(status);
    }

    setDetailsModalData(data) {
        this.detailsModalData = data;
    }

    setSelectedItem(data: Object) {
        this.selectedItemData = data;
    }

    setTargetProduct(data: any) {
        this.targetProduct.next(data);
    }

    getDetailsModalData() {
        return this.detailsModalData;
    }

    getSelectedItem() {
        return this.selectedItemData;
    }

    getSelectedItemId() {
        return (this.selectedItemData['id']) ? this.selectedItemData['id'] : null;
    }

    getPurchaseStatus() {
        return this.isEnabledPurchase.getValue();
    }
}
