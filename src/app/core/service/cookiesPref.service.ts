import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class CookiesPrefService {
    public cookiesJson;
    public allowCookies: Array<string> = [];
    public trackingInterval = 5000;
    constructor(private http: HttpClient, private cookiesService: CookieService) {

    }

    async getJsonPreferences(): Promise<any> {
        try {
            const data: any = await this.http.get('assets/cookiespref/cookies_pref.json').toPromise();
            if ('categories' in data) {
                Object.keys(data['categories']).forEach((category) => {

                    data['categories'][category]['enabled'] = true;

                    if ('details' in data['categories'][category]) {
                        data['categories'][category]['is_open'] = false;
                    }
                });
            }
            this.cookiesJson = data;
            return data;
        } catch (error) {
            throw error;
        }
    }

    public submitCookies(cookiesJson, isConfirm) {
        if (cookiesJson && 'categories' in cookiesJson) {
            const cookies = {};
            Object.keys(cookiesJson['categories']).forEach((category) => {
                if ('enabled' in cookiesJson['categories'][category]) {
                    const name = category.split('_')[1];
                    cookies[name] = (isConfirm) ? cookiesJson['categories'][category]['enabled'] :
                        (!!(cookiesJson['categories'][category]['required']));
                }
            });

            if (cookies) {
                localStorage.setItem('cookies_pref', JSON.stringify(cookies));
            }
        }
    }

    public isCookiesAccepted() {
        return (localStorage.getItem('cookies_pref') != null);
    }

    public getAllowCookies() {
        const localStorageData = JSON.parse(localStorage.getItem('cookies_pref'));
        const allowCategories = Object.keys(localStorageData).filter(key => localStorageData[key] == true);
        allowCategories.forEach((category) => {
            const matchingCategory = Object.keys(this.cookiesJson['categories']).find(key => key.includes(category));
            Object.keys(this.cookiesJson['categories'][matchingCategory]['details']).forEach((item) => {
                this.allowCookies.push(this.cookiesJson['categories'][matchingCategory]['details'][item]['cookie']);
            });
        });
    }

    public isCookieAllowed(cookie) {
        return this.allowCookies.some((allowedCookie) => {
            const regexPattern = new RegExp('^' + allowedCookie.replace(/\*/g, '.*') + '$');
            return regexPattern.test(cookie);
        });
    }

    public tracking() {
        setInterval(() => {
            if (!this.isCookiesAccepted()) {
                return;
            }

            if (!this.allowCookies.length) {
                this.getAllowCookies();
            }

            const cookies = this.cookiesService.getAll();

            Object.keys(cookies).forEach((cookie) => {
                if (!this.isCookieAllowed(cookie)) {
                    this.cookiesService.delete(cookie);
                }
            });
        }, this.trackingInterval);
    }
}
