import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { emailRegExp, passwordRegExp } from 'src/app/shared/constants/regexp';

@Injectable()
export class ErrorsService {
  constructor() {}

  public checkFirstName(name: string, control: AbstractControl): boolean {
    const isError = !String(name);

    control.setErrors({ 'empty-field': isError });

    return !isError;
  }

  public checkLastName(surname: string, control: AbstractControl): boolean {
    const isError = !String(surname);

    control.setErrors({ 'empty-field': isError });

    return !isError;
  }

  public checkEmail(email: string, control: AbstractControl): boolean {
    const isError = email && !emailRegExp.test(email);

    control.setErrors({ 'invalid-email-format': isError });

    return !isError;
  }

  public checkPhone(phoneNumber: string, control: AbstractControl): boolean {
    const isError = !String(phoneNumber) || String(phoneNumber).length < 8 || String(phoneNumber).length > 15;

    control.setErrors({ 'phone-rule': isError });

    return !isError;
  }

  public checkPassword(password: string, control: AbstractControl): boolean {
    const isError = !String(password) || String(password).length < 6 || !passwordRegExp.test(String(password));

    control.setErrors({ 'pass-rule': isError });

    return !isError;
  }

  public checkIsEmpty(value, control: AbstractControl, errorName: string): boolean {
    const isError = !value;

    control.setErrors({ [errorName]: isError });

    return isError;
  }
}
