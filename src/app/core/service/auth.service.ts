import { Injectable, EventEmitter } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { ApiService } from './api.service';
import { GapiCalendarService } from './gapi.calendar.service';

@Injectable()
export class AppAuthService {
  public startAuthEvent = new EventEmitter<boolean>();
  public finishAuthEvent = new EventEmitter<boolean>();
  public finishRegEvent = new EventEmitter<boolean>();
  public isEmailConfirm = new EventEmitter<boolean>();
  public isPhoneExists = new EventEmitter<boolean>();

  constructor(private apiService: ApiService, private calendar: GapiCalendarService) {}

  public checkTokenExists(): boolean {
    const token = window.localStorage.getItem('token');

    if (token) {
      this.apiService
        .getSelfUser(token)
        .pipe(take(1))
        .subscribe((response) => {
          if (response['status'] === 'success') {
            this.apiService.user = response['data'];
            this.apiService.token = token;
            this.finishAuthEvent.emit(true);

            if (this.apiService.user['id'] && !this.apiService.user['is_phone']) {
              this.isEmailConfirm.emit(false);
            }
          } else {
            window.localStorage.removeItem('token');
          }
        });
    }

    return Boolean(token);
  }

  public afterLogin(token): Observable<any> {
    return this.apiService.getSelfUser(token).pipe(
      tap((res) => {
        if (res['status'] === 'success') {
          this.apiService.user = res['data'];
          this.apiService.token = token;
          if (this.apiService.user['phone_number']) {
            this.apiService.token = token;
            window.localStorage.setItem('token', token);
          }
        }
      }),
      take(1)
    );
  }

  public getInvites(storedData): string {
    if (storedData) {
      const parsedData = JSON.parse(storedData);
      const invitesArray = parsedData.map(item => item.ref);
      return invitesArray.join(',');
    }

    return '';
  }
}
