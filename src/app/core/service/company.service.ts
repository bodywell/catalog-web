import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { ApiService } from './api.service';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class CompanyService {
  companyInfo;
  _isInit: BehaviorSubject<boolean> = new BehaviorSubject(false);
  constructor(private apiService: ApiService, private translate: TranslateService) {}

  public isInit() {
    return this._isInit;
  }

  public getById(companyId: number, params = {}) {
    return this.apiService.getCompanyById(companyId, params).pipe(
      map((res) => {
        if (res.status === 'success') {
          const firstThreeIndustries = res.data.industries
            .slice(0, 3)
            .map((title) => this.translate.instant(title))
            .join(', ');

          this.companyInfo = res.data;
          this._isInit.next(true);

          return {
            ...res,
            data: {
              ...res.data,
              firstThreeIndustries,
            },
          };
        }

        return res;
      })
    );
  }

  public setCompanyInfo(companyInfo: any) {
    this.companyInfo = companyInfo;
  }

  public getCompanyInfo() {
    return this.companyInfo;
  }

  public getCompanyProperties(properties = []) {
    const selectedData = {};

    Object.keys(this.companyInfo).forEach(prop => {
      if (properties.includes(prop)) {
        selectedData[prop] = this.companyInfo[prop];
      }
    });

    return selectedData;
  }

  public getCompanyBranchProperties(properties = [], branch_id) {
    let selectedData = {};
    this.companyInfo['branches'].forEach(branch => {
      if (branch['id'] == branch_id) {
        if (!properties.length) {
          selectedData = branch;
        } else {
          Object.keys(branch).forEach(prop => {
            if (properties.includes(prop)) {
              selectedData[prop] = branch[prop];
            }
          });
        }
      }
    });

    return selectedData;
  }

  public getSubscriptions(branchId: any, class_id: any = null) {
    const branch = this.companyInfo['branches'].find((row) => row.id == branchId);
    const _sub = {};

    const processSubscription = (sub, row) => {
      if (Object.keys(_sub).indexOf(sub['id'].toString()) == -1) {
        _sub[sub.id] = sub;
      }
      if (Object.keys(_sub[sub.id]).indexOf('classes') == -1) {
        _sub[sub.id]['classes'] = {};
      }
      let obj = {};
      sub.currency = this.companyInfo['currency'];
      obj[row.id] = row;
      Object.assign(_sub[sub.id]['classes'], obj);
    };

    const processGroup = (group) => {
      if (class_id && group.id != class_id) {
        return;
      }
      group.subscriptions.forEach((sub) => {
        processSubscription(sub, group);
      });
    };

    if (branch['groups'].length) {
      branch['groups'].forEach((group) => {
        processGroup(group);
      });
    }

    console.log(branch);
    if ('groups_personal' in branch && branch['groups_personal'].length) {
      branch['groups_personal'].forEach((group) => {
        processGroup(group);
      });
    }

    Object.assign({ currency: this.companyInfo['currency'] }, _sub);
    const sortedValues = Object.values(_sub);
    return sortedValues.sort((a, b) => b['id'] - a['id']);
  }

  public getServicePackages(branchId: any) {
    const branch = this.companyInfo['branches'].find((row) => row.id == branchId);
    const _packages = {};
    branch['services'].forEach((row) => {
      row.service_packages.forEach((pack) => {
        if (Object.keys(_packages).indexOf(pack['id'].toString()) == -1) {
          _packages[pack.id] = pack;
        }
        if (Object.keys(_packages[pack.id]).indexOf('services') == -1) {
          _packages[pack.id]['services'] = {};
        }
        let obj = {};
        pack.currency = this.companyInfo['currency'];
        obj[row.id] = row;
        Object.assign(_packages[pack.id]['services'], obj);
      });
    });
    const sortedValues = Object.values(_packages);
    return sortedValues.sort((a, b) => b['id'] - a['id']);
  }
}
