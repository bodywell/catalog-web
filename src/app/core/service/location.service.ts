import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs/index';
import { take, tap } from 'rxjs/operators';
import { ApiService } from './api.service';
import { commonDataService } from './commonData.service';

export interface LocationIntefrace {
  city?: string;
  country?: string;
  code?: string;
  country_code?: string;
  home?: string;
  postal_code?: string;
  region?: string;
  street?: string;
}

export interface UserIpInformation {
  ip?: string;
  country?: string;
  code?: string;
  phone_code?: any;
}

@Injectable()
export class LocationService {
  public geopositionStatus: BehaviorSubject<string> = new BehaviorSubject('');
  public selectedLocation: LocationIntefrace = {};
  private currentPosition;
  public userIpData: UserIpInformation;
  private geocoder;
  public locations = [];
  public isChanged: Subject<any> = new Subject<any>();
  public positionDefined: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  public userIpDefined: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);

  constructor(private api: ApiService, private commonDataService: commonDataService) {
    // Выбираем из локал строредж выбранную локацию.
    let location: LocationIntefrace = {};
    if (localStorage.getItem('location')) {
      location = JSON.parse(localStorage.getItem('location'));
    }
    if (
      this.locations.findIndex((x) => x.cities.indexOf(location.city) != -1 && x.country === location.country) != -1
    ) {
      this.selectedLocation = location;
    } else {
      this.selectedLocation = {};
    }
  }

  public getCurrentPosition(isReset = false) {
    this.geopositionStatus.next(window.localStorage.getItem('geopositionStatus'));
    this.currentPosition = !isReset ? window.localStorage.getItem('location') : null;

    if (!this.geopositionStatus || isReset) {
      window.localStorage.removeItem('location');
    }

    return this.getAllLocations()
      .then((_) => {
        return this.getPositionByIp();
      })
      .then((_) => {
        return this.getCurrentGeopostion();
      });
  }

  private getAllLocations(): Promise<any> {
    return this.api
      .getCompanyLocations()
      .toPromise()
      .then((res) => {
        if (res['status'] === 'success') {
          this.locations = res['data'];
        }

        return res;
      });
  }

  public getPositionByIp(): Promise<any> {
    return this.api
      .getUserLocation()
      .toPromise()
      .then((userPositionRes) => {
        this.userIpData = userPositionRes['data'];
        this.userIpDefined.next(true);
      });
  }

  private getCurrentGeopostion(): Promise<any> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (res) => this.onGeopositionSuccess(res, resolve),
        (err) => this.onGeopositionError(err, resolve)
      );
    });
  }

  private onGeopositionSuccess(res, resolve) {
    this.geocoder = new google.maps.Geocoder();

    this.geocoder.geocode({ location: { lat: res.coords.latitude, lng: res.coords.longitude } }, (result, status) => {
      this.setGeopostionStatus('GEOPOSITION_DONE');

      if (this.currentPosition) {
        this.selectedLocation = JSON.parse(this.currentPosition);
      } else {
        const parsedAddress = this.getAddressObject(result[0].address_components);

        if (
          this.selectedLocation.country !== parsedAddress['country'] ||
          this.selectedLocation.city !== parsedAddress['city']
        ) {
          this.selectedLocation = parsedAddress;
        }
      }

      this.saveLocation();
      this.positionDefined.next(true);
      this.isChanged.next(true);
      resolve(this.selectedLocation);
    });
  }

  private onGeopositionError(err, resolve) {
    if (err.code === 1) {
      if (!this.geopositionStatus || this.geopositionStatus.getValue() === 'GEOPOSITION_DONE') {
        this.setGeopostionStatus('GEOPOSITION_OFF');
      }

      if (this.currentPosition) {
        this.selectedLocation = JSON.parse(this.currentPosition);
      } else if (this.selectedLocation.country !== this.userIpData['country']) {
        this.selectedLocation['city'] = null;
        this.selectedLocation['country'] = this.userIpData['country'];
        this.selectedLocation['country_code'] = this.userIpData['code'];
      }

      this.saveLocation();
      this.positionDefined.next(false);
      this.isChanged.next(true);
      resolve(this.selectedLocation);
    }
  }

  private saveLocation() {
    let match;

    if (this.selectedLocation['city']) {
      match = this.locations.find((l) => l.cities.some((city) => city === this.selectedLocation['city']));

      if (!match) {
        match = this.locations.find((l) => l.country === this.selectedLocation['country']);
      }
    } else {
      match = this.locations.find((l) => l.country === this.selectedLocation['country']);
    }

    if (match) {
      localStorage.setItem(
        'location',
        JSON.stringify({
          city: match.cities.find((city) => city === this.selectedLocation['city']),
          country: match.country,
          country_code: match.params.code,
        })
      );
    }
  }

  public setGeopostionStatus(status: string): void {
    this.geopositionStatus.next(status);
    window.localStorage.setItem('geopositionStatus', status);
  }

  public selectLocation(city, country, country_code) {
    let data = { city, country, country_code };

    localStorage.setItem('location', JSON.stringify(data));
    this.selectedLocation = data;
    this.isChanged.next(true);
  }

  private getAddressObject(address_components) {
    const ShouldBeComponent = {
      home: ['street_number'],
      postal_code: ['postal_code'],
      street: ['street_address', 'route'],
      region: [
        'administrative_area_level_1',
        'administrative_area_level_2',
        'administrative_area_level_3',
        'administrative_area_level_4',
        'administrative_area_level_5',
      ],
      city: [
        'locality',
        'sublocality',
        'sublocality_level_1',
        'sublocality_level_2',
        'sublocality_level_3',
        'sublocality_level_4',
      ],
      country: ['country'],
      country_code: ['country'],
    };

    const address = {
      home: '',
      postal_code: '',
      street: '',
      region: '',
      city: '',
      country: '',
      country_code: '',
    };

    address_components.forEach((component) => {
      for (let shouldBe in ShouldBeComponent) {
        if (ShouldBeComponent[shouldBe].indexOf(component.types[0]) !== -1) {
          if (shouldBe === 'country_code') {
            address[shouldBe] = String(component.short_name).toLowerCase();
          } else {
            address[shouldBe] = component.long_name;
          }
        }
      }
    });

    return address;
  }
}
