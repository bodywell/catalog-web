import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { take } from 'rxjs/internal/operators';
import * as moment from 'moment';
import { NotificationMessageService } from './notification.service';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs/index';

declare var gapi: any;

@Injectable()
export class GapiCalendarService {
  API_KEY: string = 'AIzaSyBRfm_lHFUWVI0joLhto10fE9JxFD_YjF8';
  CLIENT_ID: string = '194065600216-79dgp21qoi8j3h0thr8qqmcg2rfguumt.apps.googleusercontent.com';
  DISCOVERY_DOCS: any = ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'];
  SCOPES: any =
    'https://www.googleapis.com/auth/calendar.events https://www.googleapis.com/auth/calendar.readonly https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email';

  googleAuth;

  public syncEmail: BehaviorSubject<any> = new BehaviorSubject<any>('');

  constructor(
    public apiService: ApiService,
    public notifications: NotificationMessageService,
    public translate: TranslateService
  ) {}

  /**
   *  Connect
   */

  init(syncOnly = false) {
    console.log('1: Initial connect');
    let params = {
      apiKey: this.API_KEY,
      clientId: this.CLIENT_ID,
      discoveryDocs: this.DISCOVERY_DOCS,
      scope: this.SCOPES,
    };
    return new Promise((resolve, reject) => {
      gapi.load('client:auth2', () => {
        gapi.client.init({ params }).then(() => {
          this.googleAuth = gapi.auth2.getAuthInstance();
          if (syncOnly) {
            this.googleAuth
              .signIn({
                scope: params.scope,
              })
              .then(
                (res) => {
                  setTimeout(() => {
                    console.log('2: GrantOfflineAccess(SignIn)');
                    resolve(this.googleAuth.currentUser.get().getBasicProfile().getEmail());
                  }, 500);
                },
                (err) => {
                  console.error('Error signing in', err);
                  resolve();
                }
              );
          } else {
            console.log(this.isSignedIn, 'isSignedIn');
            if (this.isSignedIn) {
              let authResponse = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse();
              if (authResponse.expires_at - Date.now() < 60000) {
                console.log('новый токен!');
                gapi.auth2
                  .getAuthInstance()
                  .currentUser.get()
                  .reloadAuthResponse()
                  .then(() => {
                    this.loadingGAPI();
                  });
              } else {
                if (authResponse.scope.indexOf('https://www.googleapis.com/auth/calendar') == -1) {
                  console.log('scope calendar was not found, init auth');
                  const option = new gapi.auth2.SigninOptionsBuilder();

                  option.setScope('https://www.googleapis.com/auth/calendar');
                  gapi.auth2
                    .getAuthInstance()
                    .currentUser.get()
                    .grant(option)
                    .then((res) => {
                      const used_email = res.getBasicProfile().getEmail();
                      if (this.syncEmail.value != used_email) {
                        this.apiService.syncGoogleCalendar(used_email, 1).subscribe((response) => {
                          if (response['status'] == 'success') {
                            this.init(false);
                          }
                        });
                      } else {
                        this.loadingGAPI();
                      }
                    });
                } else {
                  this.loadingGAPI();
                }
              }
            }
          }
        });
      });
    });
  }

  get isSignedIn(): boolean {
    return this.googleAuth.isSignedIn.get();
  }

  loadingGAPI() {
    //this.notifications.pushAlert({detail: this.translate.instant('alert.events-sync-start')});
    gapi.client.load(this.DISCOVERY_DOCS[0]).then(
      () => {
        console.log('3: GAPI client loaded for API');
        this.calendarEventsGet();
      },
      (err) => {
        console.error('Error loading GAPI client for API', err);
      }
    );
  }

  getUserEvents(calendar_events) {
    console.log('5: Get new events from bodywell api');
    this.apiService
      .syncUserEvents()
      .pipe(take(1))
      .subscribe((response) => {
        if (response['status'] == 'success') {
          response['data'] = response['data'].filter((event) => {
            return moment(event.from_date).diff(moment()) >= 0;
          });

          if (response['data'].length) {
            response['data']
              .filter((event) => {
                return !calendar_events.some((x) => {
                  return (
                    x.summary == event.product_title &&
                    moment(x.start.dateTime).format() == moment(event.from_date).format()
                  );
                });
              })
              .forEach((event) => {
                this.calendarEventsInsert(this.calendarGenerateEvent(event));
              });

            calendar_events
              .filter((event) => {
                return !response['data'].some(
                  (e) =>
                    event.summary == e.product_title &&
                    moment(event.start.dateTime).format() == moment(e.from_date).format()
                );
              })
              .forEach((event) => {
                this.calendarEventsDelete(event.id);
              });
          } else {
            //this.notifications.pushAlert({detail: this.translate.instant('alert.events-sync-nothing')});
          }
        }
      });
  }

  calendarEventsGet() {
    console.log('4: Get user calendar events for sync');
    gapi.client.calendar.events
      .list({
        calendarId: 'primary',
        timeMin: moment().format(),
      })
      .then((response) => {
        let calendar_events = response.result.items;
        this.getUserEvents(calendar_events);
      });
  }

  calendarListGet() {
    console.log('4: Send request to calendar list');
    gapi.client.calendar.calendarList
      .get({
        calendarId: 'primary',
      })
      .then(
        (response) => {
          console.log('Response', response);
        },
        (err) => {
          console.error('Execute error', err);
        }
      );
  }

  calendarGenerateEvent(data) {
    return {
      summary: data['product_title'],
      location: data['street'],
      description: data['product_description'],
      start: {
        dateTime: moment(data['from_date']).format(),
        timeZone: data['timezone'],
        //'timeZone': 'Europe/Kiev'
      },
      end: {
        dateTime: moment(data['to_date']).format(),
        timeZone: data['timezone'],
        //'timeZone': 'Europe/Kiev'
      },
      attendees: [
        { email: data['employee_email'], displayName: `${data['employee_first_name']} ${data['employee_last_name']}` },
      ],
      reminders: {
        useDefault: false,
        overrides: [
          { method: 'email', minutes: 24 * 60 },
          { method: 'popup', minutes: 60 },
        ],
      },
    };
  }

  calendarEventsInsert(event) {
    console.log('6: Insert new calendar event');
    gapi.client.calendar.events
      .insert({
        calendarId: 'primary',
        resource: event,
      })
      .then(
        (response) => {
          //this.notifications.pushAlert({detail: this.translate.instant('alert.events-sync-success')});
        },
        (err) => {
          console.error('Insert error', err);
        }
      );
  }

  calendarEventsDelete(eventId) {
    console.log('6: Удаление');
    gapi.client.calendar.events
      .delete({
        calendarId: 'primary',
        eventId: eventId,
      })
      .then(
        (response) => {
          //this.notifications.pushAlert({detail: this.translate.instant('alert.events-sync-success')});
        },
        (err) => {
          console.error('Insert error', err);
        }
      );
  }

  calendarSyncDisconnect() {
    console.log('gapi disconnect...');
    gapi.auth2.getAuthInstance().disconnect();
  }
}
