import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ApiResponseError, ApiResponseSuccess } from 'src/app/shared/interfaces/api-response.model';
import { Employee } from 'src/app/shared/interfaces/employees.model';
import { ApiService } from './api.service';

@Injectable()
export class CompanyEmployeesService {
  constructor(private apiService: ApiService) {}

  public getAll({ companyId, branchId, showNextBooking = 0, productId = 0 }) {
    return this.apiService.getEmployees({ companyId, branchId, showNextBooking, productId });
  }
}
