import { throwError as observableThrowError, Observable, BehaviorSubject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import moment from 'moment';
import { ApiResponseSuccess, ApiResponseError } from 'src/app/shared/interfaces/api-response.model';
import { Employee } from 'src/app/shared/interfaces/employees.model';
import { Company } from 'src/app/shared/interfaces/company.model';

@Injectable()
export class ApiService {
  public apiUrl = environment.ApiUrl;
  private headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
  public token = '';
  public user: any = {};
  public purchased: any = {
    subscriptions: [],
    service_packs: [],
  };
  public avatarUpdate = new EventEmitter<boolean>();

  constructor(private http: HttpClient) {}

  public register(registerInfo) {
    const params = new URLSearchParams();
    for (const key of Object.keys(registerInfo)) {
      params.set(key, registerInfo[key]);
    }

    return this.http.post(this.apiUrl + `/account/register`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getUserLocation() {
    return this.http.get(this.apiUrl + `/location/ip`).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getPhoneCodes() {
    return this.http.get(this.apiUrl + `/location/get`).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public approveRegister(email, code) {
    const params = new URLSearchParams();
    params.set('email', email);
    params.set('email_code', code);

    return this.http.post(this.apiUrl + `/account/approve_email`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public approvePhone(phoneCode, phoneNumber, code) {
    const params = new URLSearchParams();
    params.set('phone_code', phoneCode);
    params.set('phone_number', phoneNumber);
    params.set('verification_code', code);

    return this.http.post(this.apiUrl + `/account/approve_phone`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public checkPhoneNumber(phoneCode: string, phoneNumber: string) {
    const params = new URLSearchParams();
    params.set('phone_code', phoneCode);
    params.set('phone_number', phoneNumber);

    console.log('check_phone_number');

    return this.http
      .post(this.apiUrl + `/account/check_phone_number`, params.toString(), { headers: this.headers })
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public resendPasswordVerifyCode(phoneCode: string, phoneNumber: string) {
    const params = new URLSearchParams();
    params.set('phone_code', phoneCode);
    params.set('phone_number', phoneNumber);

    console.log('resend_verify_code');

    return this.http
      .post(this.apiUrl + `/account/resend_verify_code`, params.toString(), { headers: this.headers })
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public addPasswordToGoogleAcc(phoneCode: string, phoneNumber: string, password: string, code: string) {
    const params = new URLSearchParams();
    params.set('phone_code', phoneCode);
    params.set('phone_number', phoneNumber);
    params.set('password', password);
    params.set('code', code);

    console.log('add_password');

    return this.http.post(this.apiUrl + `/account/add_password`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public login({ phoneCode = null, phoneNumber = null, email = null, password }) {
    const params = new URLSearchParams();
    if (phoneCode && phoneNumber) {
      params.set('phone_code', phoneCode);
      params.set('phone_number', phoneNumber);
    }

    if (email) {
      params.set('email', email);
    }

    params.set('password', password);

    return this.http.post(this.apiUrl + `/account/login`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getSelfUser(token = this.token) {
    const params = new URLSearchParams();

    params.set('token', token);

    return this.http.post(this.apiUrl + `/account/self`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public setUser(user) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('data', JSON.stringify(user));

    return this.http.post(this.apiUrl + `/account/user/set`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public changePassword(password, newPassword) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('old_password', password);
    params.set('new_password', newPassword);

    return this.http.post(this.apiUrl + `/account/update_password`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public logOut() {
    const params = new URLSearchParams();
    params.set('token', this.token);

    return this.http.post(this.apiUrl + `/account/logout`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public forgot(phoneCode, phoneNumber) {
    const params = new URLSearchParams();
    params.set('phone_code', phoneCode);
    params.set('phone_number', phoneNumber);

    return this.http.post(this.apiUrl + `/account/forgot`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public forgotFinish(code, password) {
    const params = new URLSearchParams();
    params.set('forgot_code', code);
    params.set('password', password);

    return this.http.post(this.apiUrl + `/account/forgot_change`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getCompanyById(id, options = {}) {
    const params = new URLSearchParams();
    if (this.token) {
      params.set('token', this.token);
    }
    params.set('company_id', id);

    if (options['branchId']) {
      params.set('branch_id', options['branchId']);
    }

    return this.http
      .post<ApiResponseSuccess<Company> | ApiResponseError>(this.apiUrl + `/company/id`, params.toString(), {
        headers: this.headers,
      })
      .pipe(
        map((res) => {
          if (res['status'] === 'success') {
            let descArr;
            try {
              descArr = res.data.description
                .split('\n')
                .map((str) => {
                  // @ts-ignore
                  return utf8.decode(str);
                })
                .join('\n');
            } catch (error) {}

            if (descArr) {
              res = { ...res, data: { ...res.data, description: descArr } };
            }
          }

          return res;
        }),
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public getEmployees({
    branchId,
    companyId,
    showNextBooking = 0,
    productId = 0,
  }): Observable<ApiResponseSuccess<Employee[]> | ApiResponseError> {
    const params = new URLSearchParams();
    params.set('branch_id', branchId);
    params.set('company_id', companyId);
    params.set('show_next_bookings', String(showNextBooking));

    if (showNextBooking) {
      params.set('from', moment().add(2, 'hours').format('YYYY-MM-DD HH:mm'));

      if (productId) {
        params.set('product_id', String(productId));
      }
    }

    return this.http
      .post<ApiResponseSuccess<Employee[]> | ApiResponseError>(
        this.apiUrl + `/company/employee/team`,
        params.toString(),
        { headers: this.headers }
      )
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public getIndustries() {
    return this.http.get(this.apiUrl + `/company/industries`).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getAddressByLatLng(lat, lng) {
    return this.http
      .get(
        'https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
          lat +
          ' ' +
          lng +
          '&key=AIzaSyCokv39DsotQK9pui7dzIpbuQXjGHItq-k'
      )
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public getBookingTime(productId, branchId, employeeId, currentDate, nextDate, categoryId = null, offset = null) {
    const params = new URLSearchParams();
    // params.set('token', this.token);
    params.set('product_id', productId);
    params.set('branch_id', branchId);
    params.set('employee_id', employeeId);
    params.set('from', currentDate);
    params.set('to', nextDate);

    if (categoryId != null)
      params.set('category_id', categoryId);

    if (offset != null)
      params.set('offset', offset);


    return this.http.post(this.apiUrl + `/company/booking/time`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public booking(productId, employeeId, fromDate, comment, sub = null, payment_transaction = null) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('product_id', productId);
    params.set('employee_id', employeeId);
    params.set('from_date', fromDate);
    params.set('comment', comment);

    if (sub) {
      params.set('sub', sub);
    }

    if (payment_transaction) {
      params.set('payment_transaction', payment_transaction);
    }

    return this.http.post(this.apiUrl + `/company/booking/sign`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getBooking() {
    const params = new URLSearchParams();
    params.set('token', this.token);

    return this.http.post(this.apiUrl + `/company/booking/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getSubscription(where = {}) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    if (Object.keys(where).length) {
      params.set('where', JSON.stringify(where));
    }

    return this.http.post(this.apiUrl + `/company/subscription/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }
  public cancelBooking(id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('booking_id', id);

    return this.http.post(this.apiUrl + `/company/booking/cancel`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }
  public setReferralLinkClick(companyId, branchId, refId) {
    const params = new URLSearchParams();
    params.set('company_id', String(companyId));
    params.set('branch_id', String(branchId));
    params.set('code', String(refId));

    return this.http
      .post(this.apiUrl + `/company/referral_link_click`, params.toString(), { headers: this.headers })
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public changeEmail(email, password) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('new_email', email);
    // params.set('password', password);

    return this.http.post(this.apiUrl + `/account/update_email`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public changeEmailAccept(email, code) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('email', email);
    params.set('code', code);

    return this.http
      .post(this.apiUrl + `/account/update_email_accept`, params.toString(), { headers: this.headers })
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public search(offset, search, latitude, longitude, radius, industries, rating, location, country) {
    const params = new URLSearchParams();
    params.set('offset', offset);
    params.set('search', search);
    // params.set('latitude', latitude);
    // params.set('longitude', longitude);
    // params.set('radius', radius);
    params.set('industries', industries);
    if (location) {
      params.set('city', location);
    }
    if (country) {
      params.set('country', country);
    }
    params.set('ratings', rating);
    params.set('lang', String(window.localStorage.getItem('lang')).toLowerCase());

    return this.http.post(this.apiUrl + `/company/search/new`, params.toString(), { headers: this.headers }).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          let companies = res['data'].map((company) => {
            let descArr;

            try {
              descArr = company.description
                .split('\n')
                .map((str) => {
                  // @ts-ignore
                  return utf8.decode(str);
                })
                .join('\n');
            } catch (error) {}

            if (descArr) {
              return { ...company, description: descArr };
            }

            return company;
          });

          return { ...res, data: companies };
        }

        return res;
      }),
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public addToFavorites(companyId: number, branchId: number) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', String(companyId));
    params.set('branch_id', String(branchId));

    return this.http.post(this.apiUrl + `/company/save`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public deleteFavorites(companyId) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', companyId);

    return this.http.post(this.apiUrl + `/company/unsave`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public sendReview(companyId, title, description, rating) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('company_id', companyId);
    params.set('title', title);
    params.set('description', description);
    params.set('rating', rating);

    return this.http.post(this.apiUrl + `/comment/create`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getCompanyReview(where, offset, count, order) {
    const params = new URLSearchParams();
    params.set('where', where);
    params.set('offset', offset);
    params.set('count', count);
    params.set('order', order);

    return this.http.post(this.apiUrl + `/comment/company`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public isFavorites(id) {
    if (this.user['companies_saves'] && this.user['companies_saves'].length > 0) {
      let res = this.user['companies_saves'].find((x) => x.company_id === id);

      if (res != undefined) {
        return true;
      } else {
        return false;
      }
    }

    return false;
  }

  public getNotification(id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('notification_id', id);

    return this.http.post(this.apiUrl + `/notification/get`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public clearNotification(id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('notification_id', id);

    return this.http.post(this.apiUrl + `/notification/clear`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public getCompanyLocations() {
    return this.http.get(this.apiUrl + `/company/locations`).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public loginWithGoogle(googleId, googleFirstName, googleLastName, googleEmail) {
    const params = new URLSearchParams();
    params.set('google_id', googleId);
    params.set('google_first_name', googleFirstName);
    params.set('google_last_name', googleLastName);
    params.set('google_email', googleEmail);
    params.set('language', 'en');

    return this.http.post(this.apiUrl + `/account/google`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public loginWithFB(facebookId, facebookFirstName, facebookLastName, facebookEmail, facebookBirthday, facebookGender) {
    const params = new URLSearchParams();
    params.set('facebook_id', facebookId);
    params.set('facebook_first_name', facebookFirstName);
    params.set('facebook_last_name', facebookLastName);
    params.set('facebook_email', facebookEmail);
    params.set('facebook_birthday', facebookBirthday);
    params.set('facebook_gender', facebookGender);
    params.set('language', 'en');

    return this.http.post(this.apiUrl + `/account/facebook`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public autocomplete(type, search) {
    const params = new URLSearchParams();
    params.set('type', type);
    params.set('search', search);

    return this.http
      .post(this.apiUrl + `/company/search/autocomplete`, params.toString(), { headers: this.headers })
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public getRecommendedContent(city, country) {
    const params = new URLSearchParams();

    if (city) {
      params.set('city', city);
    }
    if (country) {
      params.set('country', country);
    }

    return this.http
      .post(this.apiUrl + `/company/search/recommended`, params.toString(), { headers: this.headers })
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public userSyncByReferral(token, companyId, branchId, ref) {
    const params = new URLSearchParams();
    params.set('token', token);
    params.set('company_id', companyId);
    params.set('branch_id', branchId);
    params.set('code', ref);

    return this.http.post(this.apiUrl + `/company/referral_sync`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public syncUserEvents() {
    const params = new URLSearchParams();
    params.set('token', this.token);
    return this.http.post(this.apiUrl + `/account/sync_events`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public resendConfirmCode() {
    const params = new URLSearchParams();
    params.set('token', this.token);

    return this.http
      .post(this.apiUrl + `/account/resend_phone_code`, params.toString(), { headers: this.headers })
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public syncGoogleCalendar(email, status) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('status', status);
    params.set('email', email);
    return this.http.post(this.apiUrl + `/account/sync_calendar`, params.toString(), { headers: this.headers }).pipe(
      catchError((error: any) => {
        return observableThrowError(error);
      })
    );
  }

  public createInvoice({ callback_url, company_id, branch_id, product_id, employee_id, book_date, amount, currency }) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('callback_url', callback_url);
    params.set('company_id', company_id);
    params.set('branch_id', branch_id);
    params.set('product_id', product_id);
    params.set('employee_id', employee_id);
    params.set('book_date', book_date);
    params.set('amount', amount);
    params.set('currency', currency);

    return this.http
      .post(this.apiUrl + `/company/booking/create_invoice`, params.toString(), { headers: this.headers })
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public purchaseCreateInvoice({ callback_url, product_id}) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('callback_url', callback_url);
    params.set('product_id', product_id);

    return this.http
        .post(this.apiUrl + `/company/purchase/create_invoice`, params.toString(), { headers: this.headers })
        .pipe(
            catchError((error: any) => {
              return observableThrowError(error);
            })
        );
  }

  public checkStripeSession(session_id, company_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('session_id', session_id);
    params.set('company_id', company_id);

    return this.http
      .post(this.apiUrl + `/company/booking/check_invoice`, params.toString(), { headers: this.headers })
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public purchaseCheckStripeSession(session_id, company_id) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('session_id', session_id);
    params.set('company_id', company_id);

    return this.http
        .post(this.apiUrl + `/company/purchase/check_invoice`, params.toString(), { headers: this.headers })
        .pipe(
            catchError((error: any) => {
              return observableThrowError(error);
            })
        );
  }

  public accept_payment(booking_id, payment_transaction) {
    const params = new URLSearchParams();
    params.set('token', this.token);
    params.set('booking_id', booking_id);
    params.set('transaction', payment_transaction);

    return this.http
      .post(this.apiUrl + `/company/booking/accept_payment`, params.toString(), { headers: this.headers })
      .pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
      );
  }

  public syncCalendarDisable() {
    const params = new URLSearchParams();
    params.set('token', this.token);

    return this.http.post(this.apiUrl + `/account/disable_sync`, params.toString(), { headers: this.headers }).pipe(
        catchError((error: any) => {
          return observableThrowError(error);
        })
    );
  }
}
