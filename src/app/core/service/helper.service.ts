import { Injectable } from '@angular/core';

@Injectable()
export class HelperService {
  constructor() {}

  copyToClipboard(text) {
    if (document.queryCommandSupported && document.queryCommandSupported('copy')) {
      const textarea = document.createElement('textarea');
      textarea.textContent = text;
      textarea.style.position = 'fixed';
      document.body.appendChild(textarea);
      textarea.select();
      try {
        return document.execCommand('copy');
      } catch (ex) {
        return false;
      } finally {
        document.body.removeChild(textarea);
      }
    }
  }

  plural(count: number, titles: Array<string>) {
    const cases = [2, 0, 1, 1, 1, 2];
    return titles[count % 100 > 4 && count % 100 < 20 ? 2 : cases[count % 10 < 5 ? count % 10 : 5]];
  }
}
