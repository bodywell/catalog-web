import {Injectable, OnInit} from '@angular/core';
import moment from 'moment';
import _ from 'lodash';

import {switchMap, tap, take, find} from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { BranchGroup, BranchService, Company } from 'src/app/shared/interfaces/company.model';
import { companyType } from 'src/app/shared/constants/company';
import { CompanyEmployeesService } from 'src/app/core/service/employees.service';
import { CompanyService } from 'src/app/core/service/company.service';
import { Employee } from 'src/app/shared/interfaces/employees.model';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {LocationStrategy} from '@angular/common';
import {environment} from '../../../environments/environment';
import {StripeScriptTag} from 'stripe-angular';
import {ApiService} from './api.service';

interface InitInfo {
  companyId: number;
  branchId: number;
  productId?: number;
  employeeId?: number;
  bookDate?: string;
  type?: number;
  paymentSessionId?: string;
  finishType?: number;
}

interface CategoryService {
  description: string;
  duration: number;
  id: number;
  price: number;
  price_max: number;
  title: string;
  vendor: string;
}

interface Category<T> {
  [key: string]: T;
}

@Injectable({
  providedIn: 'root',
})
export class BookingService {
  public queryParams: InitInfo = {
    companyId: null,
    branchId: null,
    productId: null,
    employeeId: null,
    bookDate: null,
    type: null,
    paymentSessionId: null,
    finishType: null,
  };
  public companyInfo: Company;
  public services: any = [];
  public personalGroups: any;
  public employees: Employee[];

  public selectedOptions: any = {
    employee: null,
    product: null,
    date: null,
    sub: null,
  };

  isWidget = false;

  public currentUrl;

  public isLoaded: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private employeeService: CompanyEmployeesService,
    private companyService: CompanyService,
    private translate: TranslateService,
    private locationStrategy: LocationStrategy,
    private stripe: StripeScriptTag,
    private apiService: ApiService,
    private router: Router
  ) {}

  public init(data: InitInfo): void {
    this.queryParams = data;
    this.isWidget = this.router.url.includes('widgets/');
    this.companyService
      .getById(data.companyId)
      .pipe(
        switchMap((res) => {
          if (res.status === 'success') {
            this.companyInfo = res.data;
            this.companyInfo.industryTitle = this.companyInfo.industries
              .slice(0, 3)
              .map((title) => this.translate.instant(title))
              .join(', ');
            this.companyInfo.currentBranch = this.companyInfo.branches.find((x) => x.id === Number(data.branchId));

            // Отпарсим занятия для группировки связанных с ними абонементов
            if (this.companyInfo.currentBranch['groups'].length) {
              let _sub = {};
              this.companyInfo.currentBranch['groups'].forEach((row) => {
                row.subscriptions.forEach((sub) => {
                  if (Object.keys(_sub).indexOf(sub['id'].toString()) == -1) {
                    _sub[sub.id] = sub;
                  }
                  if (Object.keys(_sub[sub.id]).indexOf('classes') == -1) {
                    _sub[sub.id]['classes'] = {};
                  }
                  let obj = {};
                  obj[row.id] = row;
                  Object.assign(_sub[sub.id]['classes'], obj);
                });
              });
              this.companyInfo.currentBranch['subscriptions'] = _sub;
            }
            switch (this.companyInfo.session) {
              case companyType.SERVICE:
                this.services = this.parseByCategory(this.companyInfo.currentBranch.services, this.companyInfo);

                // Сортируем services по category_title
                this.services.sort((a, b) => a.category_title.localeCompare(b.category_title));

                // Сортируем items в каждой категории по title
                this.services.forEach(category => {
                  category.items.sort((a, b) => a.title.localeCompare(b.title));
                });
                break;
              case companyType.GROUP:
                this.services = this.parseByCategory(this.companyInfo.currentBranch.groups, this.companyInfo);
                this.personalGroups = this.parseByCategory(this.companyInfo.currentBranch.groups_personal, this.companyInfo);
                break;
              default:
                break;
            }

            return this.getEmployees(data).pipe(
              tap((res) => {
                if (this.companyInfo['type'] === 1) {
                  const owner = res['data'].find((employee) => employee.role === 1);
                  this.companyInfo['employee_id'] = owner ? owner.id : null;
                }

                if (!isNaN(data.productId)) {
                  this.setProduct(data.productId);
                }
              })
            );
          }
        }),
        take(1)
      )
      .subscribe(() => {
        this.isLoaded.next(true);
      });
  }

  public getEmployees({
    companyId = this.queryParams.companyId,
    branchId = this.queryParams.branchId,
    productId = this.queryParams.productId,
    employeeId = this.queryParams.employeeId,
  }): Observable<any> {
    this.currentUrl = new URL(`${window.location.origin}${this.locationStrategy.path()}`).pathname;
    let showEmployeeNextBooking = (productId) ? 1 : 0;

    const pathsToCheck = ['/booking/employee', '/booking/finish', '/booking/service'];
    if (!pathsToCheck.some(path => this.currentUrl.includes(path))) {
      showEmployeeNextBooking = 0;
    }

    return this.employeeService
      .getAll({
        companyId: companyId,
        branchId: branchId,
        showNextBooking: showEmployeeNextBooking,
        productId: productId || null,
      })
      .pipe(
        tap((res) => {
          if (res['status'] === 'success') {
            this.employees = res.data.filter((employee) => employee.is_online_booking);

            if (employeeId) {
              this.setEmployee(this.employees.find((employee) => employee['id'] == employeeId));
            }
          }
        })
      );
  }

  public setQueryParams(params): void {
    if (params.company_id) {
      this.queryParams.companyId = Number(params.company_id);
    }
    if (params.branch_id) {
      this.queryParams.branchId = Number(params.branch_id);
    }
    if (params.product_id) {
      this.queryParams.productId = Number(params.product_id);
    }
    if (params.employee_id) {
      this.queryParams.employeeId = Number(params.employee_id);
    }
    if (params.book_date) {
      this.queryParams.bookDate = params.book_date;
    }
    if (params.session_id) {
      this.queryParams.paymentSessionId = params.session_id;
    }
    if (params.finish_type) {
      this.queryParams.finishType = params.finish_type;
    }
  }

  public setProduct(productId): void {
    this.queryParams['productId'] = productId;

    if (this.companyInfo['session'] == companyType.SERVICE) {
      let product;

      for (const service of this.services) {
        product = service['items'].find((x) => x.product_id == productId);
        if (product !== undefined) {
          break;
        }
      }

      this.selectedOptions.product = product;
    } else if (this.companyInfo['session'] == companyType.GROUP) {
      this.selectedOptions.product = this.companyInfo.currentBranch.groups.find((x) => x['id'] == productId);

      if (!this.selectedOptions.product && this.companyInfo.currentBranch.groups_personal) {
        this.selectedOptions.product = this.companyInfo.currentBranch.groups_personal.find((x) => x['id'] == productId);
      }
    }

    if (this.queryParams.employeeId && this.queryParams.bookDate) {
      const timeAndEmployee = {
        time: {},
        employee: {},
      };

      timeAndEmployee.employee = this.employees.find((x) => x['id'] == this.queryParams.employeeId);
      timeAndEmployee.time = {
        start_time: moment(this.queryParams.bookDate).format('HH:mm'),
        from_time: this.queryParams.bookDate,
      };

      this.setTime({ ...timeAndEmployee, product: this.queryParams.productId });
    }
  }

  public setEmployee(employee = null): void {
    this.selectedOptions.employee = employee || null;
    this.queryParams.employeeId = employee ? employee.id : null;
  }

  public setClassType(type): void {
    this.queryParams.type = type;
  }

  public setTime({ time, employee, product = null }): Observable<any> {

    let _time = {},
      isService = this.companyInfo['session'] === 0,
      isGroup = this.companyInfo['session'] === 1,
      isPersonalGroup = this.queryParams.productId && this.queryParams.type === 1;

    if (isService || isPersonalGroup) {
      _time = time;

      // if (!this.selectedEmployee) {
      //   const randomEmployeeIdx = Math.floor(Math.random() * time['employees'].length);

      //   employee = time['employees'][randomEmployeeIdx];
      // } else {
      //   employee = this.selectedEmployee;
      // }
    } else if (isGroup) {
      _time = { start_time: moment(time['fromDate']).format('HH:mm'), from_time: time['fromDate'] };
    }

    this.selectedOptions.date = time;
    this.selectedOptions.employee = employee;
    this.queryParams['bookDate'] = moment(time.from_time).format('YYYY-MM-DD HH:mm:ss');
    this.queryParams['employeeId'] = employee['id'];
    this.queryParams['productId'] = product;

    return new Observable(function subscribe(subscriber) {
      subscriber.next();
    });
  }

  public clearAll(): void {
    Object.keys(this.queryParams).forEach((key) => (this.queryParams[key] = null));
    Object.keys(this.selectedOptions).forEach((key) => (this.selectedOptions[key] = null));
  }

  public parseByCategory(data, company_info = null): void {
    return data.reduce((acc, product) => {
      const hours = moment.duration(product['duration'], 'minutes').hours();
      const minutes = moment.duration(product['duration'] - hours * 60, 'minutes').minutes();
      const _product = {
        product_id: product['id'],
        id: product['id'],
        title: product['title'],
        category: product['category'],
        duration: product['duration'],
        description: product['description'],
        relations: [],
        prepayment_value: ('prepayment_value' in product) ? product['prepayment_value'] : 'default',
        h: hours,
        m: minutes,
      };

      if (_product['prepayment_value'] == 'default' && company_info != null) {
        if ('prepayment_value' in company_info) {
          _product['prepayment_value'] = company_info['prepayment_value'];
        }
      }

      if (company_info != null && !company_info['online_payment']) {
        _product['prepayment_value'] = 0;
      }

      if ('subscriptions' in product) {
        product['subscriptions'].forEach((item) => {
          _product['relations'].push(item['id']);
        });
      }

      if ('service_packages' in product) {
        product['service_packages'].forEach((item) => {
          _product['relations'].push(item['id']);
        });
      }

      if ('price' in product) {
        _product['price'] = product['price'];
      }

      if ('price_max' in product) {
        _product['price_max'] = product['price_max'];
      }

      let desc;

      try {
        desc = _product.description
            .split('\n')
            .map((str) => {
              // @ts-ignore
              return utf8.decode(str);
            })
            .join('\n');
      } catch (error) {}

      if (desc) {
        _product.description = desc;
      }

      const categoryIndex = acc.findIndex((i) => i.category_title === product['category']);
      if (categoryIndex === -1) {
        acc.push({
          category_title: product['category'],
          items: [_product],
        });
      } else {
        acc[categoryIndex].items.push(_product);
      }

      return acc;
    }, []);
  }

  public parseGroupsForPrepaymentValue(data, subscriptions, default_value) {
    return data.map(group => ({
      ...group,
      bookings: group.bookings.reduce((acc, booking) => {
        const sub = subscriptions.find(x => booking['relations'].toString().split(',').includes(x.id.toString()));
        let prepayment_value = 0;
        if (sub) {
          prepayment_value = this.getDefaultPrepaymentValue(booking['prepayment_value'], default_value);
        }
        acc.push({ ...booking, prepayment_value: prepayment_value });
        return acc;
      }, [])
    }));
  }

  public getDefaultPrepaymentValue(item_value, company_value) {
    return (item_value == 'default') ? company_value : item_value;
  }

  public doFilterBookings(filterSettings, bookingsArray, isGroup = false) {
    if (filterSettings &&
        filterSettings.hasOwnProperty('title') &&
        filterSettings.hasOwnProperty('employee') &&
        filterSettings.hasOwnProperty('product')) {

      const filteredData = [];

      if (isGroup) {
        bookingsArray.forEach((category) => {
          const filteredBookings = category['bookings'].filter(booking => {

            const titleMatch = () => {
              return (booking['product_title']).toLowerCase().includes((filterSettings['title']).toLowerCase());
            };
            const employeeMatch = () => {
              return (filterSettings['employee'] == 0 || booking['employee_id'] == filterSettings['employee']);
            };
            const productMatch = () => {
              if (filterSettings['product'] == 0)
                return true;

              if ('relations' in booking && booking['relations'] != null) {
                booking['relations'] = booking['relations'].toString();
                return (booking['relations'].split(',').includes(filterSettings['product'].toString()));
              }

              return false;
            };

            return titleMatch() && employeeMatch() && productMatch();
          });

          if (filteredBookings.length) {
            const filteredCategory = {
              ...category,
              bookings: filteredBookings
            };
            filteredData.push(filteredCategory);
          }
        });
      } else {
        bookingsArray.forEach((category) => {
          const filteredBookings = category['items'].filter(booking => {
            const titleMatch = () => {
              return (booking['title']).toLowerCase().includes((filterSettings['title']).toLowerCase());
            };

            const employeeMatch = () => {
              if (filterSettings['employee'] == 0)
                return true;

              let hasProduct: any = false;
              this.employees.forEach((employee) => {
                if (employee.id == filterSettings['employee']) {
                  hasProduct = employee.products.filter(i => i.id == booking.product_id).length > 0;
                }
              });

              return hasProduct;
            };
            return titleMatch() && employeeMatch();
          });

          if (filteredBookings.length) {
            const filteredCategory = {
              ...category,
              items: filteredBookings
            };
            filteredData.push(filteredCategory);
          }
        });
      }

      return filteredData;
    }
  }

  public isFiltered(filterSettings) {
    if (filterSettings) {

      if (filterSettings.hasOwnProperty('title') && filterSettings['title'] != '')
        return true;

      if (filterSettings.hasOwnProperty('employee') && filterSettings['employee'] != 0)
        return true;

      if (filterSettings.hasOwnProperty('product') && filterSettings['product'] != 0)
        return true;

      return false;
    }
  }

  public createInvoiceAndRedirectToCheckout(callback_url, company_id, branch_id, product_id, employee_id, book_date, amount, currency, stripe_account) {
    this.stripe.setPublishableKey(environment.StripePublicKey, {
      stripeAccount: stripe_account,
    });
    this.apiService
        .createInvoice({
          callback_url: callback_url,
          company_id: company_id,
          branch_id: branch_id,
          product_id: product_id,
          employee_id: employee_id,
          book_date: book_date,
          amount: amount,
          currency: currency,
        })
        .pipe(take(1))
        .subscribe((res) => {
          if (res['status'] == 'success' && res['data']['sessionId']) {
            this.stripe.StripeInstance.redirectToCheckout({
              sessionId: res['data']['sessionId'],
            });
          }
        });
  }
}
