import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { IPhoneCodes } from 'src/app/shared/phone-code-select/phone-code-select.component';
import { ApiService } from './api.service';

@Injectable()
export class commonDataService {
  public phoneCodes = [];

  constructor(private apiService: ApiService, private translate: TranslateService) {}

  public getPhoneCodes(): Observable<any> {
    if (!this.phoneCodes.length) {
      return this.apiService.getPhoneCodes().pipe(
        map((res) => {
          if (res['status'] === 'success') {
            this.phoneCodes = res['data'].map(
              (phone): IPhoneCodes => {
                return {
                  label: phone.name,
                  value: {
                    name: this.translate.instant(phone.name),
                    code: phone.code.toLowerCase(),
                    phone_code: phone.phone_code,
                  },
                };
              }
            );
          }

          return this.phoneCodes;
        })
      );
    } else {
      return of(this.phoneCodes);
    }
  }
}
